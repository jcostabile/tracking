**The purpose of this directory is to (1) process images from behavioral sessions, (2) extract
position and angle data of the larva in each of the images, and (3) aggregate extracted data into a 
single file.**

*To use the code, the `conda` environment must be activated. To create the conda environment use
the "environment.yaml" file. Then process .tif stacks and extract data with `python temlr.py <arguments>`.
 After processed, combine data into single file with `python aggregateBoutData.py <arguments>`.*

---
## Create conda environmental with .yml file
Check to see if the 'tracking' virtual environment current exists on the computer with `conda info --envs`
If on a Windows system, create environment with `conda create -f windows_env.yml`, or if on a better
operating system create environment with `conda create -f bash_env.yml`.

---
## Process images
The file you will want to use is 'temlr.py'. For more information on executing it, run 
`python temlr.py -h` and a help dialog will populate. 




