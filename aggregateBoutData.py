# -*- coding: utf-8 -*-
"""
================================================================
Code to combine bout information extract from fish behavior across a collection of files


INPUTS:
  --directory:   path to directory containing either (1) .tif stacks and .info file for a one
                 behavioral session or (2) directories of individual behavioral sessions in
                 subdirectories
  --path_prefix: path with file-name-prefix to save files made while aggregating MPB.h5
                 files. At a minimum two files will be saved (the aggegrate of MPB.h5 
                 files and input data for trajectory-prediction neural network model).
                 More files saved depending on flags below.
  --img_shape:   width x height of image (must be either '80 80' or '240 240')
  --bout_window: length of bout window in milliseconds (min:100)
  --reps:        number of times to replicate data (min:1, max:64)
  --save_class:  flag to save (raw or smoothed, depending on '--raw_angles') data used
                 to manually classify quality of extract tail angles using 'manualBoutClass.py'
  --raw_angles:  flag to use raw tail angles when saving data, w/o flag smoothed tail angles saved
  --shuffle:     flag to shuffle order of swim bouts in aggregate data files set


OUTPUTS (saved files):

  (1) '*_aggregate.h5' (datAgg arrays, see 'datAgg' class below)

  ==> Load '*_aggregate.h5':
    `from aggregateBoutData import loadAggData`
    `idx, head, tail, coord, gcoord, binfo, dfiles = loadAggData(file_path)`
  ===========================================================================
  ** NOTE: tail position 0: tail tip | tail position -1: swim bladder **

   'idx':      <bout start counter, bout end counter, 'dfiles' index>

   'head':    <bout, frame>
           heading angles from each frame in the bout 
           0 radians = positive x axis

   'tail':    <bout, frame, tail position>
           raw tail angles from each tail position within each
           frame in the bout
           0 radians = positive x axis, index 0 = tail tip

   'coord':    <bout, frame, tail position, xy>
           coordinates of tail positions in local image space
           index 0 = tail tip

   'gcoord':    <bout, frame, tail position, xy>
           coordinates of tail positions in global (swim chamber) space
           index 0 = tail tip

   'binfo':    <bout, metric>
          [0]: number of frames in bout where tail was deemed "behaving"
          [1]: swim start index relative to beginning of bout window

   'dfiles':    <file paths sourced to make data, see 'idx'>

  (2) '*_modelData.h5' (numpy arrays)

  ==> Load '*_modelData.h5':
    `from aggregateBoutData import loadNNData`
    `tailComponents, deltaPosition, position0, position1, heading = loadNNData(file_path)`
  ===========================================================================

   'tailComponents':
           <bout, frame, tail, xy>
          Tail angles decomposed into xy components for each frame within 
          a bout, angles are with respect to heading (radians)

   'deltaPosition':
           <bout, (Δx swimbladder, Δy swimbladder, Δx heading, Δy heading)>
          the change in global swimbladder position and the change in
          heading xy components between the start and end of each bout

   'position0':  <bout, start/end bout swimbladder positions, xy>
           global coordinates of swimbladder at the start and end
           of each bout

   'position1':  <bout, start/end bout third-eye positions, xy>
           global coordinates of "third-eye" at the start and end
           of each bout, third-eye = computed center of eyes

   'heading':    <bout, start/end bout heading> 
           heading in radians at the start and end of each bout


  (3) '*_classData.h5' (numpy array)

  ==> Load '*_classData.h5':
    `from aggregateBoutData import loadClassData`
    `diffangles = loadClassData(file_path)`
  ===========================================================================

   'diffangles':  <bout, counter, tail position>
           Tail angle with respect to heading (radians), to be used
           with 'manualBoutClass.py'

================================================================

*Up to date as of 06/02/2022*

"""


import sys
import os
import time
import random
import glob
import h5py
import multiprocessing
import functools
import gc
import argparse
import re

from tqdm import tqdm

import subprocess
import matplotlib.cm as cm
import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

import formatTrackFile as ftf

# Class holding aggregate data
class datAgg:

  def __init__(self):
    self.data = []

  def add(self, addn):
    for a in addn:
      self.data.append(a)

  def npform(self, dt=None):
    if not isinstance(self.data, np.ndarray):
      if dt == None:
        self.data = np.asarray(self.data)
      else:
        self.data = np.asarray(self.data, dtype=dt)

  def insertArray(self, array):
    if not isinstance(array, np.ndarray):
      self.data = np.asarray(array)
    elif isinstance(array, np.ndarray):
      self.data = array

  def applyMask(self, mask):
    self.data = self.data[~mask]

  def saveData(self, field, datafile):
    assert isinstance(field, str), 'Argument \'field\' must be a string'
    with h5py.File(datafile, 'a') as h5f:
      h5f.create_dataset(field, data=self.data)

  def loadData(self, field, datafile):
    assert isinstance(field, str), 'Argument \'field\' must be a string'
    try:
      with h5py.File(datafile, 'r') as h5f:
        self.data = h5f[field][:]
    except IOError:
      print('File not accessible')


class _check_values(argparse.Action):
  def __call__(self, parser, namespace, values, option_string=None):
    if self.dest == 'directory':
      if not os.path.exists(values):
        raise argparse.ArgumentError(self, "Directory non-existent")
      setattr(namespace, self.dest, values)
    elif self.dest == 'path_prefix':
      try:
        with open(values, 'x') as tempfile:
          pass
        os.remove(values)
      except IOError:
        raise argparse.ArgumentError(self, "Unable to write to save file, possible it already exists")
      setattr(namespace, self.dest, values) 
    elif self.dest == 'img_shape':
      if not len(values) == 2:
        raise argparse.ArgumentError(self, "Two values must be entered as image shape")
      setattr(namespace, self.dest, values)
    elif self.dest == 'bout_window':
      if not values > 100:
        raise argparse.ArgumentError(self, "Bout window must be greater than 100ms")
      setattr(namespace, self.dest, values)
    elif self.dest == 'reps':
      if not 0 < values <= 2**6:
        raise argparse.ArgumentError(self, "Replications must be between 1 and 1024")
      setattr(namespace, self.dest, values)


# Handle input arguments
def getOptions(args=sys.argv[1:]):
  parser = argparse.ArgumentParser(description="For details of the inputs and outputs, read " +
                                               "comments at top of python file.")
  parser.add_argument('--directory', type=str, action=_check_values,
            help="Directory containing MPB.h5 and .info files to process and aggregate. " +
               "Directory can be for a single session or directory can contain a collection of " +
               "sessions each in their own subdirectory. Code recursively searches for all " +
               "MPB.h5 files within given directory. Ensure you wish to include them all. NOTE: " +
               "each MPB.h5 file must be in a directory with its associated .info file.")
  parser.add_argument('--path_prefix', type=str, action=_check_values,
            help="Path with file-name-prefix to save files made while aggregating MPB.h5 " +
               "files. At a minimum two files will be saved (the aggegrate of MPB.h5 " +
               "files and input data for trajectory-prediction neural network model). " +
               "More files saved depending on flags below.") 
  parser.add_argument('--img_shape', nargs='+', type=int, action=_check_values,
            help="Include image shape as two integers (i.e., \"80 80\" or \"240,240\").")
  parser.add_argument('--bout_window', type=int, default=280, action=_check_values,
            help="The length of the bout window in milliseconds (min: 100). This argument " +
               "defines the maximum length of tail movement to be considered a useful bout. " +
               "Tail movements spanning longer times will be ignored.")
  parser.add_argument('--reps', type=int, default=1, action=_check_values,
            help="The number of replications of data created for 'path_prefix' file (min: 1, " +
               "max: 64). For each bout, the starting time is randomly shifted in time for all " +
               "bouts in the replicates (including the original).")
  parser.add_argument('--save_class', action='store_true',
            help="Flag to save (raw or smoothed, depending on '--raw_angles') data used " +
               "to manually classify quality of extract tail angles using 'manualBoutClass.py'")
  parser.add_argument('--raw_angles', action='store_true',
            help="Flag to use raw tail angles when saving data, other uses smoothed " +
               "tail angles.")
  parser.add_argument('--shuffle', action='store_true',
            help="Flag to shuffle order of swim bouts in aggregate data files set.")
  options = parser.parse_args(args)
  return options


# Repositions centroid in 'FLHC' with respect to "global" ("camera" really) positions from .'track'
def addGlobalPosition(mdata, flhc, imgShape):
    localCoords = np.asarray(list(map(np.asarray, flhc['center'].values)))
    flhc['gCenter'] = (mdata[['x','y']].values + (localCoords - (np.asarray(imgShape)/2-1))).tolist() 
    return flhc


# Import information from 'MPB.h5' file
def loadMPB(dfile, imgShape):
    # Load information from tail extraction
    with h5py.File(dfile, 'r') as h5f:
        ta = h5f['tailangles'][:]
        sta = h5f['smoothedtailangles'][:]
        tc = h5f['tailcoords'][:]
        shd = h5f['smoothedheading'][:]
        exceedsRadialLimit = h5f['exceedtrials'][:]
        tailtip = h5f['tailtip'][:]
    rawdata = pd.read_hdf(dfile, key='rawdata')
    boutdata = pd.read_hdf(dfile, key='boutdata')
    skipinfo = pd.read_hdf(dfile, key='skipinfo')
    flhc = pd.read_hdf(dfile, key='flhc')
    flhc = addGlobalPosition(rawdata, flhc, imgShape)
    return (flhc, rawdata, boutdata, skipinfo, ta, sta, tc, shd, tailtip, exceedsRadialLimit)


# Loads aggregate information used elsewhere for analyses, "tailVariant" options:
# (1) smoothed tail coordinates = 'stail' or (2) raw tail coordinates = 'tail'
def loadAggData(aggDataFile, loadRaw=False):
  # Create 'agg' variables
  idxAgg, headAgg, tailAgg, coordAgg, gcoordAgg = datAgg(), datAgg(), datAgg(), datAgg(), datAgg()

  # Load raw tail angles or smoothed tail angles
  tailVariant = 'tail' if loadRaw else 'stail'

  # Load data from 'aggFile'
  idxAgg.loadData('idx', aggDataFile)
  headAgg.loadData('head', aggDataFile)
  tailAgg.loadData(tailVariant, aggDataFile)
  coordAgg.loadData('coord', aggDataFile)
  gcoordAgg.loadData('gcoord', aggDataFile)

  # These fields may/may not exist
  binfoAgg, dfileAgg = datAgg(), datAgg()
  try:
    binfoAgg.loadData('binfo', aggDataFile)
    dfileAgg.loadData('dfiles', aggDataFile)
    return (idxAgg, headAgg, tailAgg, coordAgg, gcoordAgg, binfoAgg, dfileAgg)
  except:  ## Fails for some reason
    return (idxAgg, headAgg, tailAgg, coordAgg, gcoordAgg)


# Load data saved by 'createSaveNNData' function, does not load "tail coordinates" from file
def loadNNData(modelDataFilePath):
  try:
    with h5py.File(modelDataFilePath, 'r') as h5f:
      return (h5f['tailComponents'][:], h5f['deltaPosition'][:], h5f['position0'][:],
              h5f['position1'][:], h5f['heading'][:])
  except IOError:
    print('File not accessible')


# Load data for 'manualBoutClass.py'
def loadClassData(classDataFilePath):
  try:
    with h5py.File(classDataFilePath, 'r') as h5f:
      return h5f['diffangles'][:]
  except IOError:
    print('File not accessible')


# Format/calculate/create (and potentially save) data
# for trajectory prediction model (i.e., "swimBoutModel.py")
# (also create classification data for "manualBoutClass.py")
def createSaveNNData(headAgg, tailAgg, gcoordAgg, hyp=12, classDataFilePath=None, modelDataFilePath=None):

  ## Tail angle data
  # Heading -- x,y positions
  xhead = np.cos(headAgg.data[:])
  yhead = np.sin(headAgg.data[:])
  # Tail -- x,y positions
  xtail = np.cos(tailAgg.data[:])
  ytail = np.sin(tailAgg.data[:])

  # Difference between head and tail angles using dot-product rule
  u = np.concatenate((xhead[:,:,np.newaxis], yhead[:,:,np.newaxis]), axis=-1)
  v = np.concatenate((xtail[:,:,:,np.newaxis], ytail[:,:,:,np.newaxis]), axis=-1)
  uv = np.sum(u[:,:,np.newaxis,:]*v, axis=-1)

  # Calculate angle between vectors (with appropriate sign)
  diffAngles = np.arctan2(np.cross(u[:,:,np.newaxis], v), uv)
  # diffAngles = headAgg.data[:,:,np.newaxis] - tailAgg.data

  # Break into components and concatenate
  x = np.cos(diffAngles)
  y = np.sin(diffAngles)
  tailComponents = np.concatenate((x[:,:,:,np.newaxis],y[:,:,:,np.newaxis]), axis=-1)

  ## Positional data - using global coords to calculate distance
  # The last tail coordinate (roughly the swim bladder) in the first and last frames of the bouts ('pos0')
  pos0 = gcoordAgg.data[:,(0,-1),-1]
  # Find the change in its position
  dpos0 = np.diff(pos0, axis=1).squeeze()

  # The position 'hyp' away from 'pos0' in the direction of the heading
  head0 = headAgg.data[:,(0,-1)]
  pos1 = np.array([[hyp*np.cos(head0[:,0]) + pos0[:,0,0], hyp*np.sin(head0[:,0]) + pos0[:,0,1]],
          [hyp*np.cos(head0[:,1]) + pos0[:,1,0], hyp*np.sin(head0[:,1]) + pos0[:,1,1]]])
  pos1 = np.transpose(pos1, (2,0,1))

  # Calculate head angle change ('dtheta') and separate in x,y components ('dtx', 'dty')
  p0, p1 = (pos1[:,0] - pos0[:,0], pos1[:,1] - pos0[:,1])
  dtheta = np.arctan2(np.cross(p0,p1), np.sum(p0*p1, axis=-1))
  dHead = np.concatenate((np.cos(dtheta)[:,np.newaxis], np.sin(dtheta)[:,np.newaxis]), axis=1)
  deltaPosition = np.concatenate((dpos0, dHead), axis=1)

  # Save data to manually classify bout quality (i.e.,  "manualBoutClass.py")
  if classDataFilePath != None:
    with h5py.File(classDataFilePath, 'x') as h5f:
      h5f.create_dataset('diffangles', data=diffAngles)

  # RAM maintenance, delete variables and garbage collect
  del xhead, yhead, xtail, ytail, u, v, uv, diffAngles, x, y
  del dpos0 #, dpos1
  gc.collect()

  # Save data for trajectory-model, if file path given
  if modelDataFilePath != None:
    with h5py.File(modelDataFilePath, 'x') as h5f:
      h5f.create_dataset('tailComponents', data=tailComponents)
      h5f.create_dataset('deltaPosition', data=deltaPosition)
      h5f.create_dataset('position0', data=pos0)
      h5f.create_dataset('position1', data=pos1)
      h5f.create_dataset('heading', data=head0)


# extracts usable (appropriate duration and position) bouts
def analysesPrep(camera, bogusFrames, boutData, maxFrame, windowLength, reps, shuffle=False):

  # Identify bouts where 'bogusFrames' falls within bout bin
  boutsToRemove, boutsToKeep, tmpDropInds, tmpSwimInds = np.array([]), np.array([]), np.array([]), np.array([])
  leftBinLims, rightBinLims = boutData['startCounter'].values, boutData['endCounter'].values
  for c, (l,r) in enumerate(zip(leftBinLims, rightBinLims)):
    if np.intersect1d(bogusFrames, range(l, r)).size != 0:
      boutsToRemove = np.append(boutsToRemove, c)
      tmpDropInds = np.append(tmpDropInds, np.arange(l, r))
    else:
      boutsToKeep = np.append(boutsToKeep, c)
      tmpSwimInds = np.append(tmpSwimInds, np.arange(l, r))

  # Create bout window for use in neural net
  usableBD = boutData.iloc[boutsToKeep].copy()
  fLength = windowLength/(1000/camera['rate'])  # number of frames, window length in msec
  boutDur = usableBD['endCounter'].values - usableBD['startCounter'].values
  boutsToUse = np.nonzero(boutDur < fLength)[0]

  # Only the bout data within the radial boundary and under 'windowLength' in time
  usableBD = usableBD.iloc[boutsToUse]
  framesInBout = np.tile(boutDur[boutsToUse], reps)

  ## Begin padding
  counterPad = fLength - framesInBout

  # Randomized padding before bout
  prePad = np.full(counterPad.shape, np.nan)
  prePad[counterPad > 10] = np.array([np.random.randint(10,c) for c in counterPad[counterPad > 10]])
  prePad[counterPad <= 10] = np.array([np.random.randint(c) if c > 0 else 0
                                                            for c in counterPad[counterPad <= 10]])

  # Difference in padding added after bout
  postPad = counterPad - prePad

  # Add padding to lists
  ubs, ube = [], []  # "usable bout starts/ends (counter)"
  ubs.extend((np.tile(usableBD['startCounter'].values, reps) - prePad).tolist())  # "usable bout start (counter)"
  ube.extend((np.tile(usableBD['endCounter'].values, reps) + postPad).tolist())  # "usable bout end (counter)"

  # Handle ranges exceeding 'maxFrame' by reversing the number of frames over
  exInds = np.nonzero(np.asarray(ube) > maxFrame)[0]
  for ex in exInds:
    exDiff = ube[ex] - maxFrame
    ube[ex] = maxFrame - exDiff
    ubs[ex] = ubs[ex] - 2*exDiff

  # Handle ranges with negative indexs (shouldn't be the case, but just in case)
  negInds = np.nonzero(np.asarray(ubs) < 0)[0]
  for neg in negInds:
    negDiff = -1*ubs[neg]
    ubs[neg] = ubs[neg] + negDiff
    ube[neg] = ube[neg] + negDiff

  # Shuffle counters together
  ubPlus = list(zip(map(int, ubs), map(int, ube),
                    map(int, framesInBout.tolist()), map(int, prePad.tolist())))
  if shuffle:  ## Shuffle for real?
    random.seed(a='funStuff')
    random.shuffle(ubPlus)

  return map(list, zip(*ubPlus))


# collects bout information for neural net model per behavioral session
# --> to break up bouts into windows smaller than the entire bout length
# --> use 'boutWindowFrames' argument, not currently active
def aggregateData(dfile, boutWindowFrames=None):

  # Load needed options
  opts = getOptions()
  imgShape, reps, bout_window, shuffle = (opts.img_shape, opts.reps, opts.bout_window, opts.shuffle)

  # Load information from tail extraction
  flhc, rawdata, boutdata, _, ta, sta, tc, shd, _, exceedsRadialLimit = loadMPB(dfile, imgShape)

  # Camera info
  camera = ftf.camParams(os.path.dirname(dfile))

  # Function to extract (and resample) bout boundaries
  if boutWindowFrames == None:
    boutStarts, boutEnds, framesInBout, swimStarts = analysesPrep(camera, exceedsRadialLimit,
                                            boutdata, rawdata.shape[0], bout_window, reps, shuffle)
  elif boutWindowFrames > 0:
    boutStarts, boutEnds, framesInBout, swimStarts = analysesPrep(camera, exceedsRadialLimit,
                                          boutdata, rawdata.shape[0], bout_window, 1, shuffle)

    # Begin expanding the bout indices to compile into segments of 'boutWindowFrames' length
    subsequentWindows = (np.asarray(boutEnds) - np.asarray(boutStarts)) - boutWindowFrames
    assert len(np.unique(subsequentWindows)) == 1, 'Usable bout window not consistent'
    windows = np.tile(np.arange(np.unique(subsequentWindows)+1), (len(subsequentWindows),1))
    partialWindow_boutStarts = np.ravel(windows + np.asarray(boutStarts, dtype=int)[:,np.newaxis])
    partialWindow_boutEnds = partialWindow_boutStarts + boutWindowFrames
    partialWindow_swimStarts = list(np.asarray(boutStarts) + np.asarray(swimStarts))

    # New bout starts and ends
    boutStarts = partialWindow_boutStarts.tolist()
    boutEnds = partialWindow_boutEnds.tolist()
    # Update 'swimStarts' to reflect where in the sub-bouts the swimming starts
    # Here 'swimStarts' will indicate where (if at all) within the sub-bout a swim started
    swimStarts = np.full(np.shape(boutStarts), -1)
    tmpFIB = np.full(np.shape(boutStarts), 0)  ## necessary to update 'framesInBout' too
    for n,s in enumerate(partialWindow_swimStarts):
      sDiff = s - np.asarray(boutStarts)
      sInds = np.logical_and(sDiff >= 0, sDiff < boutWindowFrames)
      swimStarts[sInds] = sDiff[sInds].astype(int)
      tmpFIB[sInds] = framesInBout[n]
    framesInBout = tmpFIB  ## reset variable to new array

    # Clean up
    del subsequentWindows, windows, partialWindow_boutStarts, partialWindow_boutEnds
    del partialWindow_swimStarts, sDiff, sInds, tmpFIB

  # Create lists to hold bout's tail information
  indices, heading, tail, stail, gcoords, coords, positions, binfo = [], [], [], [], [], [], [], []

  # Loop over bouts, aggregating information into lists
  for n,(i,j) in enumerate(zip(boutStarts, boutEnds)):
    if not np.isnan(ta[i:j,:]).any():

      # "Global" positions, to be used in 'gcoords'
      tmp = rawdata.iloc[i:j][['x','y']].values

      # Aggregate
      indices.append([i,j-1])  ## bout start and stop indices
      heading.append(flhc.iloc[i:j]['heading'].values) ## (shd[i:j])  ## heading values per frame
      tail.append(ta[i:j])  ## tail angle values
      stail.append(sta[i:j]) ## smoothed tail angle values
      gcoords.append(tmp[:,np.newaxis,:] + (tc[i:j]-(np.asarray(imgShape)/2)))  ## global tail coords
      coords.append(tc[i:j])  ## local tail coordinates
      positions.append((np.asarray(flhc.iloc[j-1]['gCenter']) - np.asarray(flhc.iloc[i]['gCenter'])).tolist())
      binfo.append([framesInBout[n], swimStarts[n]])  ## no. frames in bout, relative start indices

  # Delete variable, garbage collect = lower RAM usage
  del ta, sta, shd, tc, exceedsRadialLimit, rawdata, boutdata, flhc, camera
  del boutStarts, boutEnds, swimStarts, framesInBout
  gc.collect()

  return (indices, heading, tail, stail, gcoords, coords, positions, binfo)


def makeAggregateFile(dfiles):

  # Create data objects
  idxAgg, headAgg = datAgg(), datAgg()
  tailAgg, stailAgg = datAgg(), datAgg()
  gcoordAgg, binfoAgg = datAgg(), datAgg()
  coordAgg, posAgg = datAgg(), datAgg()

  for n, dfile in tqdm(enumerate(dfiles), desc='Looping over MPB.h5 files...', ascii=False,
                       ncols=75, position=0):
    i, h, t, s, g, c, p, b  = aggregateData(dfile)
    i = [element + [n] for element in i]  ## add file counter

    idxAgg.add(i); headAgg.add(h); tailAgg.add(t); stailAgg.add(s); gcoordAgg.add(g), binfoAgg.add(b)
    coordAgg.add(c); posAgg.add(p)

    if len(p) != len(i): breakpoint()  ## cannot remember why I included this...

    # Memory clean up
    del i, h, t, s, g, c, p, b
    gc.collect()

  # Convert to numpy arrays
  idxAgg.npform()
  headAgg.npform()
  tailAgg.npform()
  stailAgg.npform()
  gcoordAgg.npform()
  binfoAgg.npform()
  coordAgg.npform()

  # Remove all bouts with nans
  mask = np.all(np.isnan(tailAgg.data), axis=(1,2))
  idxAgg.applyMask(mask)
  headAgg.applyMask(mask)
  tailAgg.applyMask(mask)
  stailAgg.applyMask(mask)
  gcoordAgg.applyMask(mask)
  binfoAgg.applyMask(mask)
  coordAgg.applyMask(mask)

  # Create data object to associate 3rd column of 'idxAgg.data' to its file path
  fileAgg = datAgg()
  fileAgg.add(dfiles)
  fileAgg.npform(h5py.special_dtype(vlen=str))

  # Numpy arrays to HDF5, less easy (but still easy)
  opts = getOptions()
  saveFiles = [None, None]
  if opts.raw_angles:
    # Create save file names
    if opts.save_class:
      saveFiles[0] = os.path.realpath(opts.path_prefix + '_raw_classData.h5')
    saveFiles[1] = os.path.realpath(opts.path_prefix + '_raw_modelData.h5')

    with h5py.File(os.path.realpath(opts.path_prefix + '_raw_aggregate.h5'), 'a') as h5f:
      h5f.create_dataset('idx', data=idxAgg.data)
      h5f.create_dataset('head', data=headAgg.data)
      h5f.create_dataset('tail', data=tailAgg.data)
      h5f.create_dataset('coord', data=coordAgg.data)
      h5f.create_dataset('gcoord', data=gcoordAgg.data)
      h5f.create_dataset('binfo', data=binfoAgg.data)
      h5f.create_dataset('dfiles', data=fileAgg.data)

    createSaveNNData(headAgg, tailAgg, gcoordAgg, classDataFilePath=saveFiles[0],
                                                  modelDataFilePath=saveFiles[1])
    with h5py.File(saveFiles[1], 'a') as h5f:
      h5f.create_dataset('tailCoordinates', data=coordAgg.data)
  else:
    # Create save file names
    if opts.save_class:
      saveFiles[0] = os.path.realpath(opts.path_prefix + '_smooth_classData.h5')
    saveFiles[1] = os.path.realpath(opts.path_prefix + '_smooth_modelData.h5')

    with h5py.File(os.path.realpath(opts.path_prefix + '_smooth_aggregate.h5'), 'a') as h5f:
      h5f.create_dataset('idx', data=idxAgg.data)
      h5f.create_dataset('head', data=headAgg.data)
      h5f.create_dataset('stail', data=stailAgg.data)
      h5f.create_dataset('coord', data=coordAgg.data)
      h5f.create_dataset('gcoord', data=gcoordAgg.data)
      h5f.create_dataset('binfo', data=binfoAgg.data)
      h5f.create_dataset('dfiles', data=fileAgg.data)

    createSaveNNData(headAgg, stailAgg, gcoordAgg, classDataFilePath=saveFiles[0],
                                                   modelDataFilePath=saveFiles[1])
    with h5py.File(saveFiles[1], 'a') as h5f:
      h5f.create_dataset('tailCoordinates', data=coordAgg.data)


if __name__ == "__main__":

  # Load input arguments
  opts = getOptions()

  # Before this version of the code, other file endings were used. Kept this line just in case it is needed again
  fileEnding = '*_MPB.h5'

  # Locate files with 'fileEnding' in 'directory'
  dfiles = glob.glob(os.path.join(os.path.realpath(opts.directory), '**', fileEnding),
                     recursive=True)  ## all files

  # Aggregate data
  if len(dfiles) > 0:
    makeAggregateFile(dfiles)
