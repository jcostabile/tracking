# -*- coding: utf-8 -*-
"""
===========================
===========================

Functions to extract data from '.info' and '.track' files.
Primarily used by 'temlr.py' (one function used in
'aggregateBoutData.py').

"""

import mmap
import os
import re
import json

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from tifffile import TiffFile
from scipy.signal import windows
# import peakfinder


# A class used to save dictionary (with numpy arrays) to .json file
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


# Match '.track' file frame number to '.tif' file (both "camImage" and "bgImage")
def frameNumberFetching(trainDir):
    tifFiles = sorted([f for f in os.listdir(trainDir) if f.endswith('.tif')])
    bgFiles = [tf for tf in tifFiles if 'bgImage' in tf]
    camFiles = [tf for tf in tifFiles if 'camImage' in tf]

    # determine # of "bgImage" frames
    bgPages = []
    for bg in bgFiles:
        print('BG file {}'.format(bg))
        with TiffFile(os.path.join(trainDir, bg)) as tif:
            bgPages.append(len(tif.pages))
        print(bgPages)
    bgFrameIndex = np.vstack((np.insert(np.cumsum(bgPages)[:-1], 0, 0) , 
                              np.insert(np.cumsum(bgPages)[1:], 0, bgPages[0]))).transpose()
    bgFrameDict = {k: v for k, v in zip(bgFiles, bgFrameIndex)}

    # determine # of "camImage" frames
    camPages = []
    for cam in camFiles:
        print('CAM file {}'.format(cam))
        with TiffFile(os.path.join(trainDir, cam)) as tif:
            camPages.append(len(tif.pages))
        print(camPages)
    camFrameIndex = np.vstack((np.insert(np.cumsum(camPages)[:-1], 0, 0) , 
                               np.insert(np.cumsum(camPages)[1:], 0, camPages[0]))).transpose()
    camFrameDict = {k: v for k, v in zip(camFiles, camFrameIndex)}

    # merge dictionaries
    frameDict = {**bgFrameDict, **camFrameDict}

    return frameDict


# Create "frame dictionary": which frames are located in which files (i.e., maps frame number to .tif file)
def createFrameDict(trainDir):
    # if 'frameDict.json' file does not exist create it and save it
    if not os.path.exists(os.path.join(trainDir, 'frameDict.json')):
        frameDict = frameNumberFetching(trainDir)
        dict4json = json.dumps(frameDict, cls=NumpyEncoder) 
        with open(os.path.join(trainDir, 'frameDict.json'), 'w') as fp: 
            json.dump(dict4json, fp, sort_keys=True, indent=4) 
    else:
        with open(os.path.join(trainDir, 'frameDict.json')) as jsonFile:
            jsonFD = json.load(jsonFile)
        frameDict = json.loads(jsonFD)

    return frameDict


# Insert a dataframe in the middle of another one
def insertRow(idx, df, dfInsert):
    if idx == 0:
        dfA = pd.DataFrame()
        dfB = df.loc[idx:, ]
    else:
        dfA = df.loc[:idx, ]
        dfB = df.loc[idx+1:, ]

    df = dfA.append(dfInsert, ignore_index = False).append(dfB, ignore_index = False).reset_index(drop = True)

    return df


# Extract camera parameters from '.info' file
def camParams(trainDir):
    infoFile = [f for f in os.listdir(trainDir) if f.endswith('.info')]
    with open(os.path.join(trainDir, infoFile[0]), 'rb', 0) as file, \
            mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
        frameRate = int(re.findall(br'Frame rate: (\d+)', s)[0].decode('UTF-8'))
        resolution = int(re.findall(br'Resolution: (\d+)', s)[0].decode('UTF-8'))
        centerAttempt = re.findall(br'Dish center \(x, y\): \((\d+), (\d+)\)', s)

    if centerAttempt:  ## Checks if string matched for center of dish
        centerOfDish = tuple(map(int, centerAttempt[0]))
        return {'rate': frameRate, 'res': resolution, 'center': centerOfDish}
    else:  ## otherwise, ignores key:value in output dictionary
        return {'rate': frameRate, 'res': resolution}


# Port '.track' file into pandas dataframe
def moveData(trainDir):
    # Locate '.track' file
    trackFile = [f for f in os.listdir(trainDir) if f.endswith('.track')]
    # Load '.track' file as pandas dataframe
    mdata = pd.read_csv(os.path.join(trainDir, trackFile[0]), sep = '\t', header = None, usecols=[0,1,2,3])
    # Name columns
    mdata.columns = ['counter', 'x', 'y', 'heading']

    return mdata


# Handle dropped frames and add time/distance/speed information
def formatMoveData1(mdata, camera):
    # Row number should correspond to frame number, check if first recorded
    # frame equals the row number
    firstFrameIdx = np.nonzero(~np.isnan(mdata['counter'].values))[0][0]
    rowFrameDiff = int(mdata['counter'].iloc[firstFrameIdx] - firstFrameIdx)
    if rowFrameDiff != 0:
        dfna = pd.DataFrame(np.nan, index=np.arange(rowFrameDiff), 
                            columns = mdata.columns)
        mdata = insertRow(0, mdata, dfna)

    # Removed frames dropped during tracking
    mdata = removeDroppedFrames(mdata)

    # Calculations
    mdata['time'] = mdata['counter'] / camera['rate']  # seconds
    # mdata['time'] = mdata['time'].map('{:10.3f}'.format)
    mdata['dist'] = (((mdata['x'].diff())/camera['res'])**2 + 
                     ((mdata['y'].diff())/camera['res'])**2)**(1/2)  # mm
    mdata['speed'] = mdata['dist'] * camera['rate']  # mm/seconds

    return mdata


# Handle frames with obvious mistracking
def formatMoveData2(mdata, camera):
    # Recalculate speed for errors caused by mistracking
    mdata = removeMistrackedFrames(mdata, camera, 100)
    mdata = removeMistrackedFrames(mdata, camera, 10)
    mdata = removeMistrackedFrames(mdata, camera, 5)
    mdata = removeMistrackedFrames(mdata, camera, 2)
    mdata = removeMistrackedFrames(mdata, camera, 0)

    return mdata


# Fill dropped frames with nans in dataframe
def removeDroppedFrames(mdata):
    # Code to update frame numbers to reflect dropped frames, if difference in'counter' 
    # between two rows in 'mdata' are greater than 1
    dropFrameIndex = mdata.loc[mdata['counter'].diff() > 1].index - 1; counter = 0
    while (dropFrameIndex.shape[0] > 0):
        dfi = dropFrameIndex[0]
        frame1, frame2 = np.int(mdata['counter'].loc[dfi]), np.int(mdata['counter'].loc[dfi+1])
        dfna = pd.DataFrame(np.nan, index=np.arange(frame2-frame1-1), 
                            columns = mdata.columns)
        dfna['counter'] = np.arange(frame1+1, frame2)
        mdata = insertRow(dfi, mdata, dfna)

        # Prepare for next loop
        dropFrameIndex = mdata.loc[mdata['counter'].diff() > 1].index - 1
        counter+=1  
        """
        Protect against infinite loop (or near infinite), may be caused by coding
        not solving the underlying issue for why there are missing frames '.track'
        """
        if counter > 1000:
            print('While loop ended in \'formatTrackfile.py\', see \'removeDroppedFrames\' function.')
            break
    # print(mdata.loc[mdata['counter'].diff() > 1].index - 1)

    return mdata


# Ignore frames with speeds greater than 250mm/s (shown to be shifts in x and
# y positions due to mistracking)
def removeMistrackedFrames(mdata, camera, diffSep):
    # Recalculate speed for errors caused by mistracking
    speedLimitIndex = mdata.loc[mdata['speed'] > 250].index; counter = 0
    speedLimitIndex = speedLimitIndex[np.nonzero(np.diff(speedLimitIndex)>diffSep)[0]]
    while (speedLimitIndex.shape[0] > 0):
        sli = speedLimitIndex[0]
        x0, y0 = mdata.iloc[sli-1]['x'], mdata.iloc[sli-1]['y']
        x2, y2 = mdata.iloc[sli+1]['x'], mdata.iloc[sli+1]['y']
        spd, ang = mdata.iloc[sli-1]['speed'], mdata.iloc[sli-1]['heading']

        # Update position by simple midpoint
        midp = midpoint((x0,y0,x2,y2))
        mdata.iloc[sli]['x'], mdata.iloc[sli]['y'] = midp[0], midp[1]

        # Update position if there is movement
        # if np.linalg.norm(np.array((x0 - x2, y0 - y2))) == 0:
        #     mdata.iloc[sli]['x'], mdata.iloc[sli]['y'] = x0, y0
        # # else:
        # #     mdata.iloc[sli]['x'] = x0 - (spd/camera['rate'])*np.sin(ang)*camera['res']
        # #     mdata.iloc[sli]['y'] = y0 + (spd/camera['rate'])*np.cos(ang)*camera['res'] 

        # Difference in x,y positions
        dx = np.diff(mdata.iloc[sli-1:sli+2]['x'].values)
        dy = np.diff(mdata.iloc[sli-1:sli+2]['y'].values)
        # Update distance and speed for index
        mdata.iloc[sli]['dist'] = ((dx[0]/camera['res'])**2 + (dy[0]/camera['res'])**2)**(1/2)
        mdata.iloc[sli]['speed'] = mdata.iloc[sli]['dist'] * camera['rate']
        # Update distance and speed for index+1
        mdata.iloc[sli+1]['dist'] = ((dx[1]/camera['res'])**2 + (dy[1]/camera['res'])**2)**(1/2)        
        mdata.iloc[sli+1]['speed'] = mdata.iloc[sli+1]['dist'] * camera['rate']  

        # Prepare for next loop
        speedLimitIndex = mdata.loc[mdata['speed'] > 250].index
        speedLimitIndex = speedLimitIndex[np.nonzero(np.diff(speedLimitIndex)>diffSep)[0]]
        counter+=1
        """
        Protect against infinite loop (or near infinite), may be caused by coding
        not solving the underlying issue for why there are missing frames '.track'
        """
        if counter > 10000:
            print('While loop ended in \'formatTrackfile.py\', see \'removeMistrackedFrames\' function.')
            break

    return mdata


def plotDish(xc, yc, r, ax=None, border=0):
    # Plotting functions
    def createCircle(xc, yc, r, colour):
        circle= plt.Circle((xc, yc), radius= r, edgecolor=colour, fill=False)
        return circle


    def showShape(patch):
        ax=plt.gca()
        ax.add_patch(patch)
        plt.axis('scaled')

    # Plot dish
    if ax==None:
        fig, ax = plt.subplots()
    ax.set_title('Swim chamber outline and center in black')
    ax.plot(xc, yc, 'ok')
    c1 = createCircle(xc, yc, r, 'black')  # dish boundary
    c2 = createCircle(xc, yc, r-border, 'magenta')  # tracking boundary
    showShape(c1)
    showShape(c2)


# Find the central point of dish
def dishCenter(dataT, inclusionRadius, lps=20, plotting=False):

    ##--Helper functions--##
    def defineCircle(xArc, yArc):
        x1, x2, x3 = xArc
        y1, y2, y3 = yArc

        A = x1*(y2-y3) - y1*(x2-x3) + x2*y3 - x3*y2
        B = (x1**2+y1**2)*(y3-y2) + (x2**2+y2**2)*(y1-y3) + (x3**2+y3**2)*(y2-y1)
        C = (x1**2+y1**2)*(x2-x3) + (x2**2+y2**2)*(x3-x1) + (x3**2+y3**2)*(x1-x2)
        D = (x1**2+y1**2)*(x3*y2-x2*y3) + (x2**2+y2**2)*(x1*y3-x3*y1) + (x3**2+y3**2)*(x2*y1-x1*y2)

        xc, yc = ((-1*B)/(2*A)), ((-1*C)/(2*A))
        r = np.sqrt((B**2 + C**2 - 4*A*D)/(4*A**2))

        return (xc, yc, r)

    def diff2D(a1, a2):
        a1_rows = a1.view([('', a1.dtype)] * a1.shape[1])
        a2_rows = a2.view([('', a2.dtype)] * a2.shape[1])
        return np.setdiff1d(a1_rows, a2_rows).view(a1.dtype).reshape(-1, a1.shape[1])
    ##--------------------##

    # Create some variables
    inds = np.arange(0, dataT.shape[0]) 
    points = np.empty((lps,4))
    centerXY = np.empty((lps,2))
    D = np.empty((lps,))

    # Display larva positions
    if plotting:
        fig, ax = plt.subplots()
        ax.plot(dataT[:,0], dataT[:,1])

    # Iteratively find points furthest from one another, call "midpoints" circle's center
    for i in range(lps):
        # Starting point
        randPoint = np.random.randint(np.shape(dataT[inds,:])[0], size=1)[0]

        # Further point from previous
        newPoint = np.argmax(np.linalg.norm(dataT[randPoint,:] - dataT[:,None], axis=-1)) 
        for j in range(4):
            newPoint = np.argmax(np.linalg.norm(dataT[newPoint,:] - dataT[:,None], axis=-1)) 
        lastPoint = np.argmax(np.linalg.norm(dataT[newPoint,:] - dataT[:,None], axis=-1)) 

        # Pool points and find midpoint, diameter
        points[i,:] = (dataT[lastPoint][0], dataT[lastPoint][1], dataT[newPoint][0], dataT[newPoint][1])
        centerXY[i,:] = midpoint(points[i,:])
        D[i] = diameter(points[i,:])
        if plotting: ax.plot([points[i,0], points[i,2]], [points[i,1], points[i,3]], 'or')

    # Find points furthest from estimated centers
    uniqueCenters = np.unique(centerXY, axis=0)
    furthestFromCenters = np.empty(uniqueCenters.shape)
    for n, uC in enumerate(uniqueCenters):
        dists = np.linalg.norm(uC - dataT[:,None], axis=-1)
        furthestFromCenters[n,:] = dataT[np.argmax(dists),:]
    furthestFromCenters = np.unique(furthestFromCenters, axis=0)  # Only the unique ones

    # Remove points duplicated between points and furthestFromCenters
    pts = np.unique(points.reshape((-1,2)), axis=0)
    remainingPts = diff2D(pts, furthestFromCenters)
    # Find remaining point furthest from maxCenterXY
    maxCenterXY = centerXY[np.argmax(D),:]
    furthestPoint = remainingPts[np.argmax(np.linalg.norm(maxCenterXY - remainingPts[:,None],
                                                          axis=-1)),:]
    # Combine to form "ptsOnArc"
    ptsOnArc = np.vstack((furthestFromCenters, furthestPoint))
    # If only one "unique center" is found, this will result in only two points along the arc to be
    # located, code below creates areas of exclusion around found points and locates another far one
    # from center
    if len(ptsOnArc) == 2:
        dp0 =  np.linalg.norm(ptsOnArc[0] - dataT[:,None], axis=-1)  ## distances from point 0
        dp1 =  np.linalg.norm(ptsOnArc[1] - dataT[:,None], axis=-1)  ## distances from point 1
        ptsOutside = np.logical_and(dp0[:,0] > 200, dp1[:,0] > 200)  ## '200' here is subjective
        # Assuming only one unique center is found, therefore uses 'dists' determined previously
        ptsOnArc = np.vstack((ptsOnArc, dataT[ptsOutside][np.argmax(dists[ptsOutside]),:]))
        if plotting: ax.plot(*ptsOnArc.T, '.c')
    elif len(ptsOnArc) < 2:   ## or something else happened...
        fig, ax = plt.subplots()
        ax.plot(dataT[:,0], dataT[:,1])
        raise ValueError("Fewer than two points determined while identifying arena boundary. " +
                         "Inspect positions to see if they look bizarre. Otherwise, start coding " +
                         "in 'formatTrackFile.py'!")

    # Define circle center and radius
    xc, yc, r = defineCircle(ptsOnArc[:3,0], ptsOnArc[:3,1])

    if plotting:
        plotDish(xc, yc, r, ax, inclusionRadius)

    return (xc, yc, r)


# Find the frame numbers (indices) next to the swim arena boundary
def framesNearBoundary(mdata, camera, distInMM, plotting=False):
    # Distance from wall in pixels
    distanceFromWall = distInMM*camera['res']
    # Modify `mdata`
    data = np.vstack((mdata['x'].dropna(), mdata['y'].dropna()))
    dataT = data.transpose()

    # Find midpoint and radius
    if 'center' in camera.keys():
        XC, YC = camera['center']
        R = 380  ## estimate (in pixels)

        # Plot swim arena and larva positions
        if plotting:
            fig, ax = plt.subplots()
            ax.plot(dataT[:,0], dataT[:,1])
            plotDish(XC, YC, R, ax, distanceFromWall)
    else:
        XC, YC, R = dishCenter(dataT, distanceFromWall, plotting=plotting)

    # Determine which tracking coordinates exceed radius
    XY = np.vstack((mdata['x'], mdata['y']))
    radius = lambda x, y: np.sqrt((x-XC)**2 + (y-YC)**2)
    radialLimit = R - distanceFromWall  # In pixels, 'distInMM' away from wall
    coordsAsRadii = radius(XY[0,:], XY[1,:])

    # Only inspect the non-NaN locations
    nonanCoords = coordsAsRadii[~np.isnan(coordsAsRadii)]
    coordsExLimit = np.nonzero(nonanCoords > radialLimit)[0]

    return np.nonzero(~np.isnan(coordsAsRadii))[0][coordsExLimit]


# Calulate midpoint (for `dishCenter` function)
def midpoint(points):
    x1, y1, x2, y2 = points
    return np.array(((x2+x1)/2, (y2+y1)/2))


# Calculate diameter by distance between furthest points
def diameter(points):
    x1, y1, x2, y2 = points
    return np.sqrt((x2-x1)**2 + (y2-y1)**2)


# Smooth x and y position using moving boxcar or Hamming window to average
def smoothXYData(mdata, camera, pad=40):
    # Create Hamming window and normalize
    window = windows.hamming(10)
    # window = windows.boxcar(20)
    window /= np.sum(window) 

    # Convolve window with x and y coordinates
    x, y = (mdata['x'].values.copy(), mdata['y'].values.copy())
    xW = np.convolve(np.pad(x, (0, pad), 'reflect'), window, mode='same')[:-pad]
    yW = np.convolve(np.pad(y, (0, pad), 'reflect'), window, mode='same')[:-pad]

    # Recalculate distance and speed
    dist = ((np.diff(xW)/camera['res'])**2 +(np.diff(yW)/camera['res'])**2)**(1/2) 
    spd = dist * camera['rate']  

    # Rewrite dataframe
    mdata['x'] = xW
    mdata['y'] = yW
    mdata['dist'].iloc[1:] = dist
    mdata['speed'].iloc[1:] = spd

    return mdata


# Copied from MH script
def speedDetectBouts(instantSpeed, minFramesPerBout, frameRate, speedThresholdAbsolute=4):
    """
    Detect bouts based on emprically determined characterstics within a trace
    of instant speeds.
    """
    #the speed threshold is determined mostly empirically and probably needs
    #to be changed if our framerate deviates much from 250 - this depends on
    #how aggressively the position traces are smoothed in relation to the
    #framerate

    #bouts should have a clear peak, hence the peakspeed should not be
    #maintained for >~20ms
    maxFramesAtPeak = 5

    #threshold speeds
    spds = instantSpeed.copy()
    spds[np.isnan(spds)] = 0
    spds[spds<speedThresholdAbsolute] = 0

    #master indexer
    i = 0 #python like any real language uses 0-based indexing!
    bouts = np.zeros((1,5))

    l = np.size(spds)
    zeroCounter = 0

    while (i < l):
        if spds[i]==0:
            i = i+1
        else:
            # Reset zero counter
            zeroCounter = 0
            #we found a potential "start of bout" - loop till end collecting
            #all speeds
            bStart = i
            b = spds[i]
            i += 1
            # while(i<l and spds[i]>0):
            while (i<l and zeroCounter <= 5):
                if spds[i] == 0:
                    zeroCounter += 1
                else:
                    zeroCounter = 0

                b = np.append(b,spds[i])
                i += 1
            peak = b[b==b.max()]
            bEnd = i-6#since we loop until speed==0 the last part of the bout is in the frame before this frame
            #look at our criteria to decide whether we take this bout or not
            if (np.size(peak) <= maxFramesAtPeak and np.size(b) >= minFramesPerBout):
                #if fish initiate bouts before coming to rest (as during faster
                #OMR presentation) we may have a "super bout" consisting of
                #multiple concatenated bouts - try using peakfinder to determine
                #if this is the case
                #these criteria alon lead to way too many bout-splits
                #a) only bouts wihich are at least 2*minFramesPerBout should
                #be split and each split should still retain at least minFram...
                #b) the minima that are used to split the bout should be rather
                #small - smaller than 1.5*speedThreshold... and smaller
                #than half of the average speed of its surrounding peaks to
                #make sure that the fish was actually approaching rest rather
                #than just slowing down its swim a bit

                # # peakLoc, _ = find_peaks(b, prominence=1)
                # # peakLoc = detect_peaks(b)
                # pmaxs, pmins = peakfinder.peakdet(b)
                # # from IPython import embed; embed()
                # try:
                #     peakLoc = pmaxs[:,0].astype(np.int)
                #     peakMag = pmaxs[:,1]
                #     # peakMag = b[peakLoc]
                # except IndexError:#rare case: Absolutely no peak found?
                #     peakLoc = np.zeros(1)

                # if (np.size(b)>2*minFramesPerBout and np.size(peakLoc)>1):
                #     #find minima in between peaks and make sure that they are
                #     #small enough
                #     possibleMinima = np.array([],dtype=float)
                #     for p in range(np.size(peakLoc)-1):
                #         minval = b[peakLoc[p]:peakLoc[p+1]].min()
                #         if(minval<speedThresholdAbsolute*1.5 and minval<(peakMag[p]+peakMag[p+1])/4):
                #             mn = np.nonzero(b[peakLoc[p]:peakLoc[p+1]]==minval)+peakLoc[p]
                #             possibleMinima = np.append(possibleMinima,mn)
                #     # breakpoint()
                #     #for p in range...
                #     #each minimum splits the bout - make sure that on both
                #     #sides of each there is still at least minFramesPerBout left
                #     if(np.size(possibleMinima)>0):
                #         lm = 0
                #         for p in range(np.size(possibleMinima)):
                #             if(possibleMinima[p]-lm < minFramesPerBout):
                #                 possibleMinima[p] = float('NaN')
                #             else:
                #                 lm = possibleMinima[p]
                #         #for p in range...
                #         possibleMinima = possibleMinima[np.nonzero(~np.isnan(possibleMinima))]
                #     #if(have possible minima)
                # else:
                #     possibleMinima = np.array([])
                possibleMinima = np.array([])

                if (np.size(possibleMinima)>0):
                    #we should split our bout
                    nBouts = np.size(possibleMinima)+1
                    allSplitBouts = np.zeros((nBouts,5))
                    #update quantities to be relative to experiment start
                    #rather than bout start
                    minima = possibleMinima + bStart
                    allSplitStarts = np.zeros(nBouts,dtype=int)
                    allSplitEnds = np.zeros_like(allSplitStarts)
                    allSplitStarts[0] = bStart
                    allSplitStarts[1::] = minima+1
                    allSplitEnds[0:-1] = minima
                    allSplitEnds[-1] = bEnd;
                    #find the summed displacement and peakframe for each
                    #sub-bout
                    allSplitDisp = np.zeros(nBouts)
                    allPeaks = np.zeros(nBouts)
                    allPeakMags = np.zeros(nBouts)
                    for k in range(nBouts):
                        allSplitDisp[k] = np.sum(spds[allSplitStarts[k]:allSplitEnds[k]],dtype=float)/frameRate
                        mag = np.max(spds[allSplitStarts[k]:allSplitEnds[k]])
                        allPeakMags[k] = mag
                        pk = np.nonzero(spds[allSplitStarts[k]:allSplitEnds[k]]==mag)
                        allPeaks[k] = pk[0][0]
                    allPeaks = allPeaks + bStart
                    #assign all
                    allSplitBouts[:,0] = allSplitStarts
                    allSplitBouts[:,1] = allPeaks
                    allSplitBouts[:,2] = allSplitEnds
                    allSplitBouts[:,3] = allSplitDisp
                    allSplitBouts[:,4] = allPeakMags
                    #add our bouts
                    bouts = np.vstack((bouts,allSplitBouts))
                else:
                    #we have a valid, singular bout
                    peakframe = np.nonzero(b==np.max(b))
                    peakframe = peakframe[0][0]
                    #put peakframe into context
                    peakframe = bStart + peakframe
                    #a bout should not start on the peakframe
                    if(bStart==peakframe):
                        bStart = bStart-1
                    # bt = np.r_[bStart,peakframe,bEnd,np.sum(b,dtype=float)/frameRate,np.max(b)]
                    bt = np.r_[bStart, peakframe, bEnd, np.trapz(b, dx=1/frameRate), np.max(b)]
                    bouts = np.vstack((bouts,bt))
                # breakpoint()
                #if(np.size(possibleMinima)>0)
            ##if(np.size(peak) <= maxFramesAtPeak and numpy.size(b) >= minFramesPerBout)
        #if(np.isnan(spds[i]) or spds[i]==0):
    #outer while loop2
    if(np.size(bouts)>1):
        bouts = bouts[1::,:]
    return bouts
