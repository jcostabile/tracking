# -*- coding: utf-8 -*-

"""

OUTPUTS:

(1) BEHAVIOR (pandas dataframe)

Format:
behavior = pd.DataFrame(columns=['plane', 'ts', 'bout', 'regular_bout',
                                'dposition', 'dheading', 'sum_tail', 'delta_tail',
                                 'ddelta_tail', 'vigor', 'rolling_vigor', 'rolling_sum_tail',
                                 'rolling_delta_tail'])

Variable list:
           'plane' -- plane number ## irrelevant for free swimming data                       [nan]
              'ts' -- timestamps according to 'exp' object                                [seconds]
            'bout' -- location of bout starts                                              [binary]
    'regular_bout' -- flag to indicate bout consists of regular tail movements      [1=Reg, -1=Irr]
       'dposition' -- predicted displacement of bout                                       [pixels]
        'dheading' -- predicted change in heading angle                                   [radians]
        'sum_tail' -- summation of tail angles                                            [radians]
      'delta_tail' -- tail deflection metric                                              [radians]
     'ddelta_tail' -- abs. value of smoothed tail deflection derivative               [radians/sec]
           'vigor' -- absolute sum of energy in tail movement                             [unknown]
   'rolling_vigor' -- rolling sum of vigor over the succeeding frames (number = bout window) [unkn]
'rolling_sum_tail' -- rolling sum of sum_tail over the succeeding frames
'rolling_delta_tail' -- rolling sum of delta_tail over the succeeding frames

(2) Source data file location (string)

(3) Swim trajectory (ANN) model directory location (string) -- UNUSED FOR FREE-SWIMMING BEHAVIOR FILE
Currently unused, but should be updated to include (once new trajectory model is created)

(4) UMAP (numpy array)

Purpose of array is to assess transfer learning of swim trajectory model. The reason it is
called UMAP is because this was the analytical tool applied to the free and head-fixed swimming
behavior data sets for assessment.

NOTE #1: Tail position 0 = tail tip; tail position -1 = swim bladder
NOTE #2: Coordinates rotated so larva faces positive y axis, angles left unchanged because they
         are relative to adjacent tail position

Format:
  [Rows]: Bouts
  [Cols]: <plane,
          ACTUAL bout Δx displacement, ACTUAL bout Δy displacement,
          ACTUAL bout Δx angle component, ACTUAL bout Δy angle component,
          ~frame 0, tail position 0~ (angle, x coordinate, y coordinate),
          ~frame 0, tail position 1~ (angle, x coordinate, y coordinate),
          ~frame 0, tail position 2~ (angle, x coordinate, y coordinate),
          ...,
          ~frame 0, last tail position~ (angle, x coordinate, ycoordinate),
          ~frame 1, tail position 0~ (angle, x coordinate, y coordinate),
          ~frame 1, tail position 1~ angle, x coordinate, y coordinate,
          ...,
          ~last frame, last tail position~ angle, x coordinate, y coordinate>
"""


# Add paths
import os
import gc
import sys
import glob
import h5py
import argparse

import pandas as pd
import numpy as np
import matplotlib.pyplot as pl; pl.ion()
from natsort import natsorted
from datetime import date

import formatTrackFile as ftf
from temlr import anglePrep
from aggregateBoutData import analysesPrep, loadMPB


class _check_values(argparse.Action):
  def __call__(self, parser, namespace, values, option_string=None):
    if self.dest == 'mpb_path':
      if not os.path.isfile(os.path.realpath(values)):
        raise argparse.ArgumentError(self, "MPB file does not exist, check full path")
      setattr(namespace, self.dest, os.path.realpath(values))
    elif self.dest == 'img_shape':
      if not len(values) == 2:
        raise argparse.ArgumentError(self, "Two values must be entered as image shape")
      setattr(namespace, self.dest, values)
    elif self.dest == 'bout_window':
      if not values > 100:
        raise argparse.ArgumentError(self, "Bout window must be greater than 100ms")
      setattr(namespace, self.dest, values)


# Handle input arguments
def getOptions(args=sys.argv[1:]):
  parser = argparse.ArgumentParser(description="Converts MPB.h5 file into bhvr file format " +
               "matching the bhvr files created for two-photon rig behavioral sessions.")
  parser.add_argument('--mpb_path', type=str, action=_check_values,
            help="Path to MPB.h5 to convert to bhvr format. " +
               "Directory for a single session only.  NOTE: The MPB.h5 file must be in the same" +
               "directory as its associated .info file.")
  parser.add_argument('--img_shape', nargs='+', type=int, action=_check_values,
            help="Include image shape as two integers (i.e., \"80 80\" or \"240,240\").")
  parser.add_argument('--bout_window', type=int, default=280, action=_check_values,
            help="The length of the bout window in milliseconds (min: 100). This argument " +
               "defines the maximum length of tail movement to be considered a useful bout. " +
               "Tail movements spanning longer times will be ignored.")
  options = parser.parse_args(args)
  return options


def generateRotationMatrix(_coords, _posShift=1/2, _negShift=3/2, _returnHeadingPoints=False,
                           _flipped=False):
  # Find the angle between swim bladder and the tail-point directly before it
  if _flipped: ## coordinate of tail position preceding swim bladder (positioned at origin)
    hcoord = np.squeeze(np.diff(_coords[:,:,:2][:,:,::-1], axis=2))
  else:
    hcoord = np.squeeze(np.diff(_coords[:,:,-2:], axis=2))
  hcoordAngle = np.arctan2(hcoord[:,:,1].ravel(), hcoord[:,:,0].ravel())  ## .reshape(hcoord.shape[:2])
  posInds, negInds = (np.nonzero(hcoordAngle >= 0)[0], np.nonzero(hcoordAngle < 0)[0])
  hcoordAngle[posInds] = _posShift*np.pi - hcoordAngle[posInds]
  hcoordAngle[negInds] = -1*_negShift*np.pi - hcoordAngle[negInds]
  hcoordAngle = hcoordAngle.reshape(hcoord.shape[:2])
  # Create rotation matrix 'R' from 'hcoordAngle'
  c, s = np.cos(hcoordAngle.ravel()).reshape(hcoordAngle.shape), np.sin(hcoordAngle.ravel()).reshape(hcoordAngle.shape)
  R = np.array(((c, -s), (s, c))).transpose((2,3,0,1))

  # Prepare outputs
  if _returnHeadingPoints:
    return (R, np.einsum('bfij, bfj -> bfi', R, hcoord))
  else:
    return R


def calcDeltaHeading(_headings):

  # Angle components
  x = np.cos(_headings)
  y = np.sin(_headings)

  # Find change in angle using dot-product rule
  u = np.hstack((x[:, 0, np.newaxis], y[:, 0, np.newaxis]))
  v = np.hstack((x[:, 1, np.newaxis], y[:, 1, np.newaxis]))
  uv = np.sum(u*v, axis=1)
  # Calculate magnitudes based on arctan(sin,cos)
  diffAngles = np.stack((uv, np.cross(u,v)), axis=-1)

  return diffAngles


def diffHeadingTail(_headings, _tailangles):
  ## Tail angle data
  # Heading -- x,y positions
  xhead = np.cos(_headings)
  yhead = np.sin(_headings)
  # Tail -- x,y positions
  xtail = np.cos(_tailangles)
  ytail = np.sin(_tailangles)

  # Difference between head and tail angles using dot-product rule
  u = np.concatenate((xhead[:,np.newaxis], yhead[:,np.newaxis]), axis=-1)
  v = np.concatenate((xtail[:,:,np.newaxis], ytail[:,:,np.newaxis]), axis=-1)
  uv = np.sum(u[:,np.newaxis,:]*v, axis=-1)

  # Calculate angle between vectors (with appropriate sign)
  diffAngles = np.arctan2(np.cross(u[:,np.newaxis], v), uv)
  # diffAngles = headAgg.data[:,:,np.newaxis] - tailAgg.data

  # Break into components and concatenate
  x = np.cos(diffAngles)
  y = np.sin(diffAngles)
  tailComponents = np.concatenate((x[:,:,np.newaxis],y[:,:,np.newaxis]), axis=-1)

  return tailComponents


def aggregate_bhvr_files(_bhvr_dir):
  """ Condense bhvr files from free-swimming experimental sessions
  The output will have the same format as that created in 'main' below.

  Keyword arguments:
  _bhvr_dir -- location of directory containing bhvr files
  """
  # Collect paths
  bfiles = []
  for path in glob.glob(os.path.join(_bhvr_dir, '**', '*.bhvr'), recursive=True):
    bfiles.append(path)
  bfiles = natsorted(bfiles)

  # Construct aggregate dataframe
  # Prepare behavior metrics dataframe
  behavior = pd.DataFrame(columns=['plane', 'ts', 'bout', 'regular_bout', 'dposition', 'dheading',
                                   'sum_tail', 'delta_tail', 'ddelta_tail', 'vigor',
                                   'rolling_vigor', 'rolling_sum_tail', 'rolling_delta_tail',
                                   'exp_distance', 'exp_heading'])

  # Fill in 'behavior' dataframe
  for bfile in bfiles:
    if not 'modelPath' in locals():
      B, _, modelPath, _ = load_bhvr(bfile)
    else:
      B = pd.read_hdf(_bhvr_path)

    # Now to build growing dataframe
    behavior = pd.concat([behavior, B], ignore_index=True)

    # RAM housekeeping
    try:
        del B
        gc.collect()
    except:
        pass

  # Convert dtypes to int for some columns
  for col in behavior.columns[[2,3]]:  ## bout, regular_bout
    behavior[col] = behavior[col].astype(int)

  # Convert dtypes to int for other columns
  behavior['plane'] = behavior['plane'].astype(float)
  behavior['ts'] = behavior['ts'].astype(float)
  for col in behavior.columns[4:]:  ## all the others...
    behavior[col] = behavior[col].astype(float)

  ## Time to save
  saveFilePath = os.path.join(os.path.realpath(_bhvr_dir),
                              f'{date.today():%y%m%d}_MPB_aggregate.bhvr')
  try:
    # Dataframe to HDF5
    behavior.to_hdf(saveFilePath, key='B', mode='w')

    # Append paths for HDF5 object
    dt = h5py.special_dtype(vlen=str)
    path_len = sorted([len(_bhvr_dir), len(modelPath)], reverse=True)[0]
    with h5py.File(saveFilePath, 'r+') as h5f:
      dset = h5f.create_dataset('paths', (path_len,), dtype=dt)
      dset[0] = _bhvr_dir
      dset[1] = modelPath

  except:
    print("Unable to create {}".format(saveFilePath))


# Return contents on BHVR file
def load_bhvr(_bhvr_path):

  try:
    B = pd.read_hdf(_bhvr_path)

    with h5py.File(_bhvr_path, 'r') as h5f:
      dataPath = h5f['paths'][0]
      modelPath = h5f['paths'][1]

      if 'umap' in h5f.keys():
        umap = h5f['umap'][:]
        umap_bool = True
      else:
        umap_bool = False

    if umap_bool:
      return B, dataPath, modelPath, umap
    else:
      return B, dataPath, modelPath

  except Exception as e:
    print(e)
    print("Unable to load {}".format(_bhvr_path))


def main(_mpbPath, _imgShape=(80,80), _boutWindow=280):

  ## Load information and data
  # Camera info
  camera = ftf.camParams(os.path.dirname(_mpbPath))
  # MPB data
  FLHC, _, boutdata, _, tailangles, _, tailcoords, _, _, bogusFrames = loadMPB(_mpbPath, _imgShape)

  # Prepare behavior metrics dataframe
  behavior = pd.DataFrame(columns=['plane', 'ts', 'bout', 'regular_bout', 'dposition', 'dheading',
                                   'sum_tail', 'delta_tail', 'ddelta_tail', 'vigor',
                                   'rolling_vigor', 'rolling_sum_tail', 'rolling_delta_tail',
                                   'exp_distance', 'exp_heading'])

  # Expand the bout window to equal '_boutWindow'
  boutStarts, boutEnds, _, _ = analysesPrep(camera, bogusFrames, boutdata, tailangles.shape[0],
                                            _boutWindow, 1, False)
  # Indices of bouts with padding
  indexes = np.array([np.arange(bs,be) for bs,be in zip(boutStarts, boutEnds)])

  # Calculate tail features
  delta_tail, ddelta_tail, _, _ = anglePrep(tailangles.copy(),
                                            FLHC['heading'].values.copy(),
                                            camera['rate'], (0,7), 5)

  # Tailangle difference with heading
  filt_cta = diffHeadingTail(FLHC['heading'].values.copy(), tailangles.copy())

  # Assemble data used in neural network ("mdl_inputs": 'Rds' and 'dstail')
  ## tail angles
  ta = tailangles[indexes]
  dstail = np.stack((np.cos(ta), np.sin(ta)), axis=-1)
  ## tail positions
  origin = tailcoords[indexes,-1]  ## position of "swim bladder", to translate other points around
  dscoords = tailcoords[indexes] - origin[:,:,np.newaxis]

  # Create rotation matrix 'R' by aligning tail-position coordinates ('dscoords') to y-axis
  R = generateRotationMatrix(dscoords, _returnHeadingPoints=False, _flipped=False)
  # Apply rotation matrix 'R' to 'dscoords' (model input variable)
  Rds = np.einsum('bfij, bfkj -> bfki', R, dscoords)

  ## Calculate result of bouts from 'FLHC' dataframe
  # Exclude nans from subsequent calculations
  hd = FLHC['heading'].values  ## frame-by-frame headings
  indexes_update = [np.append(i, i[-1]+1) for i in indexes]  ## include the last index in arrays
  boutTerminals = [iu[np.nonzero(~np.isnan(hd[iu]))[0][[0,-1]]]
                   for iu in indexes_update]  ## non-nan starts/ends
  # Recreate 'resultY' variable (from 'swimBoutModel.py') for actual bout results (i.e., trajectory)
  delta_displacement = np.array([np.diff(FLHC.iloc[bt]['gCenter'].tolist(), axis=0)[0]
                                 for bt in boutTerminals])  ## resultY[0] equivalent
  bout_headings = np.array([FLHC.iloc[bt]['heading'].tolist() for bt in boutTerminals])
  delta_heading = calcDeltaHeading(bout_headings)  ## resultY[1] equivalent

  ## Build arrays to insert into 'behavior' data frame
  frames_total, frames_in_bout = (tailangles.shape[0], boutEnds[0] - boutStarts[0])
  bout_occurances = np.zeros(frames_total)
  exp_distance = np.zeros(frames_total)
  exp_heading = np.zeros(frames_total)
  disp_angle = np.zeros(frames_total)

  # '1' where bouts began
  bout_occurances[boutStarts] = 1

  # Actual change in displacement
  exp_distance[boutStarts] = np.linalg.norm(delta_displacement, axis=1)
  ## Actual change in heading
  exp_heading[boutStarts] = np.arctan2(delta_heading[:,1], delta_heading[:,0])
  ## Direction of displacement, is bout "forwards" or "backwards"
  disp_angle[boutStarts] = np.arctan2(delta_displacement[:,1], delta_displacement[:,0])
  reg_bout = np.sign(disp_angle)  ## bout identified with a backward trajectory

  # Calculate vigor
  dts = 1/camera['rate']  # temporal resolution
  convolve_start, convolve_stop = (int(frames_in_bout/2), -1*(int(frames_in_bout/2)-1))
  vigor = np.convolve(ddelta_tail, dts*np.ones(frames_in_bout),
                      mode='full')[convolve_start:convolve_stop]

  ## Put data into 'behavior' dataframe
  behavior['plane'] = np.full(frames_total, np.nan)
  behavior['ts'] = dts*np.arange(0, frames_total)
  behavior['bout'] = bout_occurances
  behavior['regular_bout'] = reg_bout
  #behavior['dposition'] =  ## need to generate new model, then calculate and insert Δdisplacement
  #behavior['dheading'] = ## need to generate new model, then calculate and insert Δangle
  behavior['sum_tail'] = filt_cta[:,0]  ## angle at end of tail
  behavior['delta_tail'] = delta_tail
  behavior['ddelta_tail'] = ddelta_tail
  behavior['vigor'] = vigor
  behavior['exp_distance'] = exp_distance
  behavior['exp_heading'] = exp_heading

  # Rolling window calcs
  behavior['rolling_vigor'] = behavior.rolling(frames_in_bout)['vigor'].sum().shift(-69)
  behavior['rolling_sum_tail'] = behavior.rolling(frames_in_bout)['sum_tail'].sum().shift(-69)
  behavior['rolling_delta_tail'] = behavior.rolling(frames_in_bout)['delta_tail'].sum().shift(-69)

  # Convert dtypes to int for some columns
  for col in behavior.columns[[2,3]]:  ## bout, regular_bout
    behavior[col] = behavior[col].astype(int)

  # Convert dtypes to int for other columns
  behavior['plane'] = behavior['plane'].astype(float)
  behavior['ts'] = behavior['ts'].astype(float)
  for col in behavior.columns[4:]:  ## all the others...
    behavior[col] = behavior[col].astype(float)

  ## Build UMAP data = combined model inputs & outputs into array
  mdl_inputs = np.concatenate((ta[...,None], Rds[:,:,:-1]), axis=-1).reshape(ta.shape[0],-1)
  plane_array = np.full((ta.shape[0], 1), np.nan)
  umap_array = np.concatenate((plane_array, delta_displacement, delta_heading, mdl_inputs), axis=1)

  return behavior, umap_array

if __name__ == "__main__":

  # Load input arguments
  opts = getOptions()

  # Create and save bhvr file
  B, UMAP = main(opts.mpb_path, opts.img_shape, opts.bout_window)

  ## Save to HDF5
  # Generate save file location
  saveFilePath = opts.mpb_path.split('_MPB')[0] + '.bhvr'
  try:
    if B is not None:
      B.to_hdf(saveFilePath, key='B', mode='w')

      # Append paths for HDF5 object
      model_path = 'ANN model not used, feature yet to be added...'
      dt = h5py.special_dtype(vlen=str)
      path_len = sorted([len(opts.mpb_path), len(model_path)], reverse=True)[0]
      with h5py.File(saveFilePath, 'r+') as h5f:
        dset = h5f.create_dataset('paths', (path_len,), dtype=dt)
        dset[0] = opts.mpb_path
        dset[1] = model_path

      # Add 'UMAP' array to hdf5 object
      with h5py.File(saveFilePath, 'r+') as h5f:
        h5f.create_dataset('umap', data=UMAP)

      print("File created: {}".format(saveFilePath))
  except:
    print("Unable to create {}".format(saveFilePath))
    os.remove(saveFilePath)
