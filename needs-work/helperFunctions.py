# -*- coding: utf-8 -*-
"""
================================================================
Code to inspect, extract, and analyze fish behavior video/images
================================================================

*Functions to help visualize swim images*

** NOTE: Not all functions have been completed **

"""

import os
import glob
import json
import subprocess
import h5py

import cv2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from tifffile import TiffFile, imread
from scipy.signal import windows
from scipy.ndimage import median_filter

import formatTrackFile as ftf


# Show suspicious image frames
def showFrames(trainDir, suspiciousFrame, frameDict):
    # Extract useful data from dictionary
    files = list(frameDict.keys())  
    frames = list(frameDict.values())  
    # Locate files that 'suspiciousFrame' is in
    suspiciousIndices = np.where([np.logical_and(suspiciousFrame >= f1, suspiciousFrame < f2) for f1,f2 in frames])[0]
    frameOfInterest = suspiciousFrame - frames[suspiciousIndices[1]][0]

    # Load files
    bg = imread(os.path.join(trainDir, files[suspiciousIndices[0]]))
    im = imread(os.path.join(trainDir, files[suspiciousIndices[1]]))

    img = np.abs(im[frameOfInterest-7:frameOfInterest+8,:,:].astype(np.float32) - 
                 bg[frameOfInterest-7:frameOfInterest+8,:,:].astype(np.float32)).astype(np.uint8)
    img2D = img.transpose(1,2,0).reshape((240, -1), order='F')
    imgRecast = np.vstack((img2D[:,:240*5], img2D[:,240*5:240*10], img2D[:,240*10:]))
    cv2.imshow('', imgRecast); cv2.waitKeyEx(25000); cv2.destroyAllWindows()

    # img = np.abs(im[frameOfInterest,:,:].astype(np.float32) - 
    #              bg[frameOfInterest,:,:].astype(np.float32)).astype(np.uint8)
    # cv2.imshow('', img); cv2.waitKeyEx(25000); cv2.destroyAllWindows()


# Save suspicious image frames as video
def showVideo(trainDir, startingFrame, endingFrame):
    # Load 'frameDict.json'
    with open(os.path.join(trainDir, 'frameDict.json')) as jsonFile:
        jsonFD = json.load(jsonFile)
    frameDict = json.loads(jsonFD)

    # Extract useful data from dictionary
    files = list(frameDict.keys())  
    frames = list(frameDict.values())  
    # Locate files that 'startingFrame' and 'endingFrame' are in
    startIdx = np.where([np.logical_and(startingFrame >= f1, startingFrame < f2) for f1,f2 in frames])[0]
    endIdx = np.where([np.logical_and(endingFrame >= f1, endingFrame < f2) for f1,f2 in frames])[0]
    # if ~np.all(startIdx == endIdx):
    #     print('Ending *showVideo*: starting and ending frames different.')
    #     return

    # Within file indices of the frames
    sFrame = startingFrame - frames[startIdx[1]][0]
    eFrame = endingFrame - frames[startIdx[1]][0]

    # Video name, before loading
    vidName = input("Video file name: ")

    # Load files
    bg = imread(os.path.join(trainDir, files[startIdx[0]]))
    im = imread(os.path.join(trainDir, files[startIdx[1]]))

    # Create video from images
    if not vidName:
        video = cv2.VideoWriter('../video/showVideo.avi', cv2.VideoWriter_fourcc(*'XVID') , 20.0, im.shape[1:], 0)
    else:
        video = cv2.VideoWriter('../video/'+ vidName +'.avi', cv2.VideoWriter_fourcc(*'XVID') , 20.0, im.shape[1:], 0)
    for frameOfInterest in np.arange(sFrame, eFrame):
        # 'np.flipud' to place the image in "world" orientation
        img = np.abs(im[frameOfInterest,:,:].astype(np.float32) - bg[frameOfInterest,:,:].astype(np.float32)).astype(np.uint8)
        text = 'Frame ' + str(frameOfInterest + frames[startIdx[1]][0])
        cv2.putText(img, text, (1,10), cv2.FONT_HERSHEY_SIMPLEX, 0.3, 255, 1, bottomLeftOrigin=False)
        video.write(img)

        # Show images as video
        # cv2.imshow('', img)
        # # Press Q on keyboard to  exit
        # if cv2.waitKey(100) & 0xFF == ord('q'):
        #     break

    # cv2.destroyAllWindows()
    video.release()


# Save suspicious image frames as video
def showVideoInStack(trainDir, startingCounter, endingCounter, df):

    # Video name, before loading
    vidName = input("Video file name: ")

    # Extract useful bits from 'df'
    stack = int(df['stack'].iloc[startingCounter])
    sFrame = int(df['frame'].iloc[startingCounter])
    eFrame = int(df['frame'].iloc[endingCounter])

    # Separate into image types
    bgFiles = sorted(glob.glob(os.path.join(trainDir,'*bgImage*')))
    camFiles = sorted(glob.glob(os.path.join(trainDir,'*camImage*')))

    # Load files
    bg = imread(os.path.join(trainDir, bgFiles[stack]))
    im = imread(os.path.join(trainDir, camFiles[stack]))

    # Create video from images
    if not vidName:
        video = cv2.VideoWriter('../video/showVideo.avi', cv2.VideoWriter_fourcc(*'XVID') , 10.0, im.shape[1:], 0)
    else:
        video = cv2.VideoWriter('../video/'+ vidName +'.avi', cv2.VideoWriter_fourcc(*'XVID') , 10.0, im.shape[1:], 0)
    for f, frameOfInterest in enumerate(np.arange(sFrame, eFrame)):
        img = np.abs(im[frameOfInterest,:,:].astype(np.float32) - 
                     bg[frameOfInterest,:,:].astype(np.float32)).astype(np.uint8)
        text = 'Frame ' + str(startingCounter + f)
        cv2.putText(img, text, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255, 1)
        video.write(img)

    video.release()


# Create video of bout from '.MPB' file and frame numbers
def boutImages(dfile, startingFrame, endingFrame, raw=False, hyp=12):

    # Locations
    trainDir = os.path.dirname(dfile)

    # Convert frames to counter
    flhc = pd.read_hdf(dfile, key='flhc')
    # startingCounter = flhc[flhc['frameIdx'] == startingFrame].index[0]
    # endingCounter = flhc[flhc['frameIdx'] == endingFrame].index[0]
    startingCounter = startingFrame
    endingCounter = endingFrame

    # Load information from tail extraction
    with h5py.File(dfile, 'r') as h5f:
        ta = h5f['smoothedtailangles'][startingCounter:endingCounter]
        tc = h5f['tailcoords'][startingCounter:endingCounter]
        hd = h5f['smoothedheading'][startingCounter:endingCounter]
    # hd = flhc.iloc[startingCounter:endingCounter]['heading'].values

    # Extract useful bits from 'flhc'
    stack = int(flhc['stack'].iloc[startingCounter])
    sFrame = int(flhc['frame'].iloc[startingCounter])
    eFrame = int(flhc['frame'].iloc[endingCounter])

    # Separate into image types
    bgFiles = sorted(glob.glob(os.path.join(trainDir,'*bgImage*')))
    camFiles = sorted(glob.glob(os.path.join(trainDir,'*camImage*')))

    # Load files
    bg = imread(os.path.join(trainDir, bgFiles[stack]))
    im = imread(os.path.join(trainDir, camFiles[stack]))

    # Background subtract image
    if raw:
        img = im[sFrame:eFrame,:,:].astype(np.uint8)
    else:
        img = np.abs(im[sFrame:eFrame,:,:].astype(np.float32) - bg[sFrame:eFrame,:,:].astype(np.float32)).astype(np.uint8)

    # Plotting variable
    if im.shape[1:] == (80, 80):
        acceptableArea = np.array([[10, 10], [10, 60], [60, 60], [60, 10]], dtype=np.int32) 
        hw, hl, l = (1, 0.5, 5)
    elif im.shape[1:] == (240, 240):
        acceptableArea = np.array([[60, 60], [60, 180], [180, 180], [180, 60]], dtype=np.int32) 
        hw, hl, l = (3, 1.5, 15)

    # Create video
    plt.figure(999)
    for i in range(len(img)):
        # Plotting
        plt.imshow(img[i], origin='upper', cmap=cm.Greys_r); plt.ylim(acceptableArea[-1]); plt.xlim(acceptableArea[1])
        if not raw:
            for c, a in zip(tc[i,:,:], ta[i,:]):
                plt.arrow(*c, (l/10)*np.cos(a), (l/10)*np.sin(a),
                          head_width=hw/3, head_length=hl/3, fc=None, ec='magenta', linewidth=2) 

            # Generate "third eye" from last tail coord (of each frame) and heading (of each frame)
            t, h = tc[i,-1], hd[i]
            p01 = np.array([hyp*np.cos(h)+t[0], hyp*np.sin(h)+t[1]])
            p0 = np.concatenate([t, p01])
            plt.plot(*p0[0:2], marker='x', color='orange', mew=4)
            plt.plot(*p0[2:], marker='x', color='orange', mew=4)
            plt.xticks([], []); plt.yticks([], [])
        plt.text(*acceptableArea[0]+5, 'Frame ' + str(i+startingFrame), fontsize=10, color='red', fontweight='bold')

        # Save plot as .png and clear axis
        plt.savefig(os.path.join(trainDir, 'video', 'file%02d.png' % i))
        plt.cla()
    plt.close()

    # Create video from images
    subprocess.call(['ffmpeg', '-framerate', str(8), '-i', os.path.join(trainDir, 'video', 'file%02d.png'), 
                     '-r', str(30), '-pix_fmt', 'yuv420p', os.path.join(trainDir, 'video',
                     'frames_' + str(startingFrame) + '-' + str(endingFrame) + '.mp4')])
    for file_name in glob.glob(os.path.join(trainDir, 'video', '*.png')):
        os.remove(file_name)


# Let's see how this goes
def boutXY(diffangles, x, y, idxAgg, randomNum=1000, saveDir='/run/media/jamie/LargePass/dataAggregate'):

    assert x.shape == y.shape, 'Shapes of \'x\' and \'y\' are not equal.'

    # Choose a random collection of swim bouts
    randIdx = sorted(np.random.choice(range(len(diffangles)), randomNum, replace=False))

    # Create video
    fig, ax = plt.subplots(3, 1, num=999)
    for n,i in enumerate(randIdx):
        # Plotting
        ax[0].imshow(diffangles[i].T)
        ax[1].imshow(x[i].T)
        ax[2].imshow(y[i].T)

        # Display starting/ending counter, and file index as title
        ax[0].set_title('#{3} | Start {0} | stop {1} | file index {2}'.format(
                        idxAgg.data[i][0], idxAgg.data[i][1], idxAgg.data[i][2], n), fontweight='bold') 

        # Handle y axis labels
        ax[0].set_ylabel('Δ Angles', fontweight='bold')
        ax[1].set_ylabel('X', fontweight='bold')
        ax[2].set_ylabel('Y', fontweight='bold')
        # Handle x axis labels
        ax[0].set_xticks([]); ax[1].set_xticks([])
        ax[2].set_xlabel('Frame', fontweight='bold')

        plt.savefig(os.path.join(saveDir, 'video', 'file%02d.png' % n))

        if n % 50 == 0 and n != 0:
            plt.close()
            fig, ax = plt.subplots(3, 1, num=999)
        else:
            plt.cla()
    plt.close()

    # Create video from images
    subprocess.call(['ffmpeg', '-r', str(2), '-i', os.path.join(saveDir, 'video', 'file%02d.png'), 
                     '-r', str(2), '-pix_fmt', 'yuv420p', os.path.join(saveDir, 'video',
                     'XY_' + str(randomNum) + 'rand_bouts.mp4')])
    for file_name in glob.glob(os.path.join(saveDir, 'video', '*.png')):
        os.remove(file_name)


# Let's see how this goes
def boutImagesAgg(bdirs, idx, idxAgg, headAgg, tailAgg, coordAgg, hyp=12):

    # Convert info
    indices = idxAgg.data
    heading = headAgg.data
    tailAngles = tailAgg.data
    tailCoords = coordAgg.data

    # Locations
    trainDir = bdirs[indices[idx,-1].astype(np.int)]
    ta = tailAngles[idx]
    tc = tailCoords[idx]
    hd = heading[idx]

    # Extract useful bits from 'FLHC'
    startingCounter, endingCounter = indices[idx,:2].astype(np.int)
    FLHC = pd.read_hdf(glob.glob(os.path.join(trainDir, '*_tail_MPB.h5'))[0], 'flhc')  
    stack = int(FLHC['stack'].iloc[startingCounter])
    sFrame = int(FLHC['frame'].iloc[startingCounter])
    eFrame = int(FLHC['frame'].iloc[endingCounter])

    # Separate into image types
    bgFiles = sorted(glob.glob(os.path.join(trainDir,'*bgImage*')))
    camFiles = sorted(glob.glob(os.path.join(trainDir,'*camImage*')))

    # Load files
    bg = imread(os.path.join(trainDir, bgFiles[stack]))
    im = imread(os.path.join(trainDir, camFiles[stack]))

    # Create image
    img = np.abs(im[sFrame:eFrame,:,:].astype(np.float32) - bg[sFrame:eFrame,:,:].astype(np.float32)).astype(np.uint8)

    # Plotting variable
    acceptableArea = np.array([[20, 20], [20, 60], [60, 60], [60, 20]], dtype=np.int32) 
    hw, hl, l = (1, 0.5, 5)
    # acceptableArea = np.array([[60, 60], [60, 180], [180, 180], [180, 60]], dtype=np.int32) 
    # hw, hl, l = (3, 1.5, 15)

    # Create video
    plt.figure(999)
    for i in range(len(img)):
        # Plotting
        plt.imshow(img[i], origin='upper', cmap=cm.Greys_r); plt.ylim(acceptableArea[-1]); plt.xlim(acceptableArea[1])
        for c, a in zip(tc[i,:,:], ta[i,:]):
            plt.arrow(*c, (l/10)*np.cos(a), (l/10)*np.sin(a),
                      head_width=hw/3, head_length=hl/3, fc='magenta', ec='magenta', linewidth=2) 

        # Generate "third eye" from last tail coord (of each frame) and heading (of each frame)
        t, h = tc[i,-1], hd[i]
        p01 = np.array([hyp*np.cos(h)+t[0], hyp*np.sin(h)+t[1]]) 
        p0 = np.concatenate([t, p01])       
        plt.plot(*p0[0:2], marker='x', color='orange', mew=4)
        plt.plot(*p0[2:], marker='x', color='orange', mew=4)

        plt.savefig(os.path.join(trainDir, 'video', 'file%02d.png' % i))
        plt.cla()
    plt.close()

    # Create video from images
    subprocess.call(['ffmpeg', '-framerate', '8', '-i', os.path.join(trainDir, 'video', 'file%02d.png'), 
                     '-r', '30', '-pix_fmt', 'yuv420p', os.path.join(trainDir, 'video',
                     'frames_' + str(startingCounter) + '-' + str(endingCounter) + '.mp4')])
    for file_name in glob.glob(os.path.join(trainDir, 'video', '*.png')):
        os.remove(file_name)


# Speed related information has been...a bit of a problem
def plotSpeeds(df):
    t = df['time'].values.copy()
    spd = df['speed'].values.copy()
    fig, ax = plt.subplots()
    ax.plot(t, spd)


# Show the swim bouts
def plotSwimBouts(frameIdx, swimMetric, boutStarts, boutEnds, yLabel='Swim Metric'):
    # Overlay on to current plot
    fig, ax = plt.subplots(1,1)
    ax.plot(swimMetric, '.', linestyle='-', color='black')

    ## Highlight swim bouts
    for i0, i1 in zip(boutStarts, boutEnds): 
        ax.plot(frameIdx[i0:i1], swimMetric[i0:i1], linestyle='-', color='violet')

    ax.set_xlabel('Frame No.', fontweight='bold')
    ax.set_ylabel(yLabel, fontweight='bold')


# Show the swim bouts
def speedSwimBouts(raw, smoothed, bouts, kept, removed):
    fig, ax = plt.subplots()
    frameIdxs = raw['frameIdx'].values

    ## Plot speed calculated from raw positions
    # ax.plot(raw['speed'].values, '.')
    # for i0, i1 in zip(bouts['startIdx'].values, bouts['endIdx'].values): 
    #     ax.plot(np.arange(i0,i1), raw['speed'].iloc[i0:i1], linestyle='-', color='orange')

    ## Plot speed calculated from smoothed positions
    ax.plot(frameIdxs, smoothed['speed'].values, marker='.', linestyle='-', color='black')

    ## Highlight swim bouts
    for i0, i1 in zip(bouts.iloc[kept]['startIdx'].values, bouts.iloc[kept]['endIdx'].values):
        ax.plot(frameIdxs[i0:i1], smoothed['speed'].iloc[i0:i1], linestyle='-', color='violet')
    for i0, i1 in zip(bouts.iloc[removed]['startIdx'].values, bouts.iloc[removed]['endIdx'].values):
        ax.plot(frameIdxs[i0:i1], smoothed['speed'].iloc[i0:i1], linestyle='-', color='red')

    ax.set_xlabel('Frame No.', fontweight='bold')
    ax.set_ylabel('Speed (mm/s)', fontweight='bold')


# Compare the bout starts and stops from two methods
def compareBoutInds(frameIdx, swimMetric, boutStarts0, boutEnds0, boutStarts1, boutEnds1):
    fig, ax = plt.subplots(2,1) 

    ## Plot speed calculated from smoothed positions
    ax[0].plot(frameIdx, swimMetric, linestyle='-', color='black')
    ax[1].plot(frameIdx, swimMetric, linestyle='-', color='black')

## Highlight swim bouts
    for i0, i1 in zip(boutStarts0, boutEnds0): 
        ax[0].plot(frameIdx[i0:i1], swimMetric[i0:i1], linestyle='-', color='violet')
    for i0, i1 in zip(boutStarts1, boutEnds1): 
        ax[1].plot(frameIdx[i0:i1], swimMetric[i0:i1], linestyle='-', color='red')

    ax[1].set_xlabel('Frame No.', fontweight='bold')
    ax[1].set_ylabel('Swim metric', fontweight='bold')


# Function to look into frames within 'skipInfo' or 'FLHC' dataframes
def investigateFrame(bdir, idx):
    # Load info from h5 file
    h5File =  glob.glob(os.path.join(bdir, '*MPB.h5'))[0]
    with h5py.File(h5File, 'r') as h5f:
        tailAngles = h5f['tailangles'][idx]
        tailCoords = h5f['tailcoords'][idx]

    FLHC = pd.read_hdf(h5File, key='flhc')
    rawData = pd.read_hdf(h5File, key='rawdata')

    # Grab some parameters from 'FLHC'
    cX, cY = FLHC['center'].iloc[idx]
    heading = FLHC['heading'].iloc[idx]

    # Load 'frameDict.json'
    with open(os.path.join(bdir, 'frameDict.json')) as jsonFile:
        jsonFD = json.load(jsonFile)
    frameDict = json.loads(jsonFD)

    # Load info from frameDict
    files = list(frameDict.keys())  
    frames = list(frameDict.values())

    # Sort frame number to order files
    bgIdx = [k for k, f in enumerate(files) if 'bgImage' in f]
    camIdx = [k for k, f in enumerate(files) if 'camImage' in f]
    assert len(bgIdx) == len(camIdx), 'The number of ''bg'' and ''cam'' images files are not equal.'

    # Index of associated 'bg' and 'cam' files, and total file number
    frameIdx = np.array((bgIdx, camIdx)).T
 
    # Expand info from FDHC dataframe
    stackOfInterest = int(FLHC['stack'].iloc[idx])
    frameNumber = int(FLHC['frame'].iloc[idx])
    counter = int(FLHC['counter'].iloc[idx])

    # Load file corresponding to 'stackOfInterest'
    bg = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,0]]))
    im = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,1]]))

    # Background subtract to form image
    img = np.abs(im[frameNumber,:,:].astype(np.float32) - 
                 bg[frameNumber,:,:].astype(np.float32)).astype(np.uint8)

    # Find centroids of largest contours (within acceptable area)
    if img.size == 80**2:
        plotRange = np.array([[20, 60], [60, 20]], dtype=np.int32) 
        hw, hl, l = (1, 0.5, 5)
    elif img.size == 240**2:
        plotRange = np.array([[60, 180], [180, 60]], dtype=np.int32) 
        hw, hl, l = (3, 1.5, 15)

    if True:
        # Plotting
        plt.ion()
        plt.imshow(img, origin='upper', cmap=cm.Greys_r)
        plt.xlim(plotRange[0]); plt.ylim(plotRange[1])

        plt.plot(cX, cY, 'ok')
        # plt.plot(*tailCoords.T, '.m', markersize=8)

        ax = plt.gca()
        for p, ta in enumerate(tailAngles):
            ax.arrow(*tailCoords[p,:], 1.5*np.cos(ta), 1.5*np.sin(ta),
                    head_width=hw/3, head_length=hw/4, fc='magenta', ec='magenta', linewidth=2)  
        ax.arrow(cX, cY, 12*np.cos(heading), 12*np.sin(heading),
                 head_width=hw, head_length=hl, fc='red', ec='red')     


# Computes the minimum angle of rotation going from angleFirst to angleSecond
def computeMinorDeltaAngle(angleFirst, angleSecond):

    if(np.size(angleFirst)>1 or np.size(angleSecond)>1):
        #we have vector data - make sure it conforms
        m1 = np.shape(angleFirst)
        m2 = np.shape(angleSecond)

        if(np.size(m1)>1 or np.size(m2)>1):
            raise ValueError('Function only accepts vector or scalar data')

        if(m1!=m2):
            raise ValueError('Inputs to the function must have the same dimensions')

    if(np.size(angleFirst)==1):
        #scalar data
        dangle = angleSecond - angleFirst
        if(dangle>np.pi):
            delta = dangle - 2*np.pi
        else:
            if(dangle<-np.pi):
                delta = dangle + 2*np.pi
            else:
                delta = dangle
    else:
        #vector data
        dangle = angleSecond - angleFirst
        delta = dangle
        delta[dangle>np.pi] = dangle[dangle>np.pi] - 2*np.pi
        delta[dangle<-np.pi] = dangle[dangle<-np.pi] + 2*np.pi

    return delta


# Defines bouts using threshold of windowed standard deviation (in radians!)
def headingBoutDetection(heading, camera):
    # Camera image frequency
    frame_rate = camera['rate']

    # Difference in heading angles (radians)
    all_d_angles = np.r_[np.nan, computeMinorDeltaAngle(heading[:-1], heading[1:])]

    # Convert radians to degrees 
    all_d_angles = all_d_angles * (180/np.pi)

    # empirically determined cutoff
    # all_d_angles[np.abs(all_d_angles) > 50] = 0

    ang_std = np.full(all_d_angles.size, np.nan)
    for i in range(9, ang_std.size):
        ang_std[i] = np.std(all_d_angles[i - 9:i + 1])

    # call bouts based on heading angle standard deviations
    std_cut = 0.5
    abv_t = (ang_std > std_cut).astype(float)
    abv_t[np.isnan(abv_t)] = 0
    starts = np.where(np.r_[0, np.diff(abv_t)] > 0)[0]
    ends = np.where(np.r_[0, np.diff(abv_t)] < 0)[0]
    if ends[0] < starts[0]:
        ends = ends[1:]
    if starts.size > ends.size:
        starts = starts[:ends.size]
    blen = ends - starts
    # filter by bout length
    take = blen >= 80 / (1000 / frame_rate)
    starts = starts[take]
    ends = ends[take]
    bouts = pd.DataFrame(np.c_[starts[:, None], ends[:, None]], columns=['startIdx', 'endIdx'])

    return bouts

