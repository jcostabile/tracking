# -*- coding: utf-8 -*-
"""
====================================
    Train swim bout classifier
====================================
"""
import os
import sys
import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt


# Handle input arguments
def get_options(args=sys.argv[1:]):
	parser = argparse.ArgumentParser(
		description="Classify swim bouts based on tail angles as good (1), weird (2), or bad (3)")
	parser.add_argument(dest='dataFilePath', type=str,
						help="location of the \'classifier_data.h5\' file")
	parser.add_argument(dest='saveFileName', type=str,
						help="name of csv file to be saved to \'dataFilePath\' directory "
						   + "printing classifications in [bout index, classification] form; " 
						   + "appends if file exists, creates otherwise")
	parser.add_argument(dest='numToClassify', type=str, nargs='?', default=50,
						help="the number of bouts to be classified (default=50)")	
	options = parser.parse_args(args)

	return options


# Defines what happens on a 'key_press_event'
def press(event, i, arr):
	if (event.key == '1' or event.key == '2' or \
		event.key == '3' or event.key == '4'):
		print('Tail bout {0} classified as {1}'.format(i, event.key))
		arr.append((i, int(event.key)))
		sys.stdout.flush()
	elif (event.key == 'b' and arr != []):
		print('Tail bout {0} classified inappropriately, removed'.format(arr[-1][0]))
		arr.append((0, event.key))
		sys.stdout.flush()


# A random collection of swim bouts to classify
def main(diffangles, x, y, randnum):
	imgClassifier = []  ## list containing bout index and classification
	randIdx = np.random.choice(range(len(diffangles)), randnum, replace=False)

	# Loop over tail angle heat plots
	fig, ax = plt.subplots(3,1,num=999)
	fig.canvas.mpl_connect('key_press_event', lambda event: press(event, i, imgClassifier))
	# Disable 'q' keypress
	plt.rcParams['keymap.quit'].remove('q')

	n = 0  ## counter
	try:
		while n < randnum:
			# Particular index to present
			i = randIdx[n]

			# Plot tail angles along with x and y components
			ax[0].imshow(diffangles[i].T, vmin=-np.pi, vmax=np.pi)
			ax[1].imshow(x[i].T, vmin=-1, vmax=1)
			ax[2].imshow(y[i].T, vmin=-1, vmax=1)

			# Display starting/ending counter, and file index as title
			ax[0].set_title('Enter image category:\n1 (good), 2 (weird good), 3 (weird bad), 4 (bad)', fontweight='bold') 

			# Handle y axis labels
			ax[0].set_ylabel('Δ Angles', fontweight='bold')                                                            
			ax[1].set_ylabel('X', fontweight='bold')                                                                   
			ax[2].set_ylabel('Y', fontweight='bold')           
			# Handle x axis labels
			ax[0].set_xticks([]); ax[1].set_xticks([])
			ax[0].set_yticks([]); ax[1].set_yticks([]); ax[2].set_yticks([])
			ax[2].set_xlabel('Frame', fontweight='bold')

			# Take only key press, not mouse click
			while not plt.waitforbuttonpress(timeout=-1): pass

			try:  ## avoid problems with empty 'imgClassifier' list
				if (imgClassifier[n][-1] == 1 or imgClassifier[n][-1] == 2 or \
				    imgClassifier[n][-1] == 3 or imgClassifier[n][-1] == 4):
					n += 1  ## update counter classified as 1,2,3 (or 4)
				elif (imgClassifier[n][-1] == 'b' and n > 0):
					imgClassifier = imgClassifier[:-2]  ## remove last element AND go back one
					n -= 1  ## step back due to inappropriate classification
			except:
				pass

			# Clear axes and begin again
			plt.cla()
	except:
		pass
	finally:
		plt.close()
		return imgClassifier


if __name__ == "__main__":

	# Retreive input arguments	
	opts = get_options()
	datafile, savefilename, numToClassify = (opts.dataFilePath, opts.saveFileName, int(opts.numToClassify))

	# Load data from 'datafile'
	with h5py.File(datafile, 'r') as h5f:
		diffangles = h5f['diffangles'][:]
	x = np.cos(diffangles)
	y = np.sin(diffangles)

	ic = main(diffangles, x, y, numToClassify)

	if savefilename.lower().endswith('.csv'):
		with open(os.path.join(os.path.dirname(datafile), savefilename), 'a') as savefile:
			np.savetxt(savefile, np.asarray(ic), delimiter=',', fmt='%d')
	else:
		with open(os.path.join(os.path.dirname(datafile), os.path.splitext(savefilename)[0] + '.csv'), 'a') as savefile:
			np.savetxt(savefile, np.asarray(ic), delimiter=',', fmt='%d')
