# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as pl

from sklearn.metrics import r2_score
from mpb2bhvr import load_bhvr

mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

if __name__ == "__main__":
  # Load free swimming behavior file
  bpath = "F:\\free_swimming_master\\220606_MPB_0_166_aggregate.bhvr"
  B, _, _ = load_bhvr(bpath)

  # Find indices of bouts
  inds = B[B['bout'] == 1].index

  # Recalculate rolling sums
  B['rolling_vigor'] = B.rolling(frames_in_bout)['vigor'].sum().shift(-69)
  B['rolling_sum_tail'] = B.rolling(frames_in_bout)['sum_tail'].sum().shift(-69)
  B['rolling_delta_tail'] = B.rolling(frames_in_bout)['delta_tail'].sum().shift(-69)

  # Convert units pixels-->mm, radians-->degrees
  exp_distance = B['exp_distance'].iloc[inds].values / 8 ## free swimming behavior rig resolution: 8 px/mm
  exp_heading = np.rad2deg(B['exp_heading'].iloc[inds].values)
  rolling_sum_tail = np.rad2deg(B['rolling_sum_tail'].iloc[inds].values)
  rolling_vigor = B['rolling_vigor'].iloc[inds].values * (180/np.pi)

  # Create trendlines
  tl_disp = np.poly1d(np.polyfit(rolling_vigor, exp_distance, 1))
  tl_angle = np.poly1d(np.polyfit(rolling_sum_tail, exp_heading, 1))

  # Plot rolling_vigor vs displacement
  fig, ax = pl.subplots(1,2)
  ax[0].plot(rolling_vigor[::10], exp_distance[::10], '.', color='gray')
  ax[0].plot(rolling_vigor[::10], tl_disp(rolling_vigor[::10]), '-k', linewidth=2)
  ax[0].text(0.05, 0.95, f"$\\rho = {np.corrcoef(rolling_vigor, exp_distance)[0,1]:0.3f}$",
             transform=ax[0].transAxes, fontsize=10, verticalalignment='top')

  ax[0].set_xlabel(r'Vigor ($\degree$/s)', fontweight='bold')
  ax[0].set_ylabel(r'$\Delta$displacement (mm)', fontweight='bold')
  #ax[0].set_xticks([0,500,1000,1500,2000]); ax[0].set_yticks([0,5,10,15])

  # Plot rolling sum tail vs heading change
  ax[1].plot(rolling_sum_tail[::10], exp_heading[::10], '.', color='gray')
  ax[1].plot(rolling_sum_tail[::10], tl_angle(rolling_sum_tail[::10]), '-k', linewidth=2)
  ax[1].text(0.05, 0.95, f"$\\rho = {np.corrcoef(rolling_sum_tail, exp_heading)[0,1]:0.3f}$",
             transform=ax[1].transAxes, fontsize=10, verticalalignment='top')

  ax[1].set_xlabel(r'$\Sigma$ (cumulative tail angle) ($\degree$)', fontweight='bold')
  ax[1].set_ylabel(r'$\Delta$heading ($\degree$)', fontweight='bold')
  #ax[1].set_xticks([-6000,-3000,0,3000,6000]); ax[1].set_yticks([-180,-90,0,90,180])

