# -*- coding: utf-8 -*-
# A quick file generated to batch download entire Box directories

# Import modules
import os, sys
import numpy as np
from boxsdk import Client, OAuth2

def initiateClient():
    # Authenticate connection info and execution
    auth = OAuth2(
        client_id='yi48pgqfp6rrsmvnbn5tj9og9pueoa0b',
        client_secret='08BNO7bV13BN4aXSphOp5JKjAS6ZqE8R',
        # access_token='XzBFUlcDLWqyhIOsiz2Kt1uzbRXrRUjK',
    )

    # auth_url, csrf_token = auth.get_authorization_url('[]')
    # // redirect(auth_url, code=302)
    client = Client(auth)

    # Success?
    user = client.user().get()
    print('The current user ID is {0}'.format(user.id))
    return client

# Box purely uses these hard to find IDs (I found the one below (foldID) in the Box URL, BOX!! *wrings hands*)
def boxFolders(client, foldID='107934386892'):
    # Store box folder names
    folderIDs = np.array([])
    folderNames = np.array([])
    # Get sub-folder information from Box
    items = client.folder(folder_id=foldID).get_items()
    print('Choose folder to download:')
    # list information
    for (n, item) in enumerate(items):
        print('[{0}] {1}'.format(n, item.name))
        folderIDs = np.append(folderIDs, item.id)
        folderNames = np.append(folderNames, item.name)
    # Choose folder and return information
    foldChoice = input('?? >> ')[0]
    print('\n')
    return folderNames[int(foldChoice)], folderIDs[int(foldChoice)]


def boxDL(client, dlDir, foldName, foldID):
    items = client.folder(folder_id=foldID).get_items()
    if not os.path.isdir(os.path.join(dlDir, foldName)):
        os.mkdir(os.path.join(dlDir, foldName))

    for item in items:
        if item.name not in os.listdir(dlDir):
            print(f'Downloading {item.name} to {os.path.join(dlDir, foldName)}')
            outputFile = open(os.path.join(dlDir, foldName, item.name), 'wb')
            client.file(item.id).download_to(outputFile)
            outputFile.close()
            # print('{0} {1} is named "{2}"'.format(item.type.capitalize(), item.id, item.name))
        else:
            print('Skipping {0}...'.format(item.name))


def main():
    c = initiateClient()  # Connect to Box
    folderName, folderID = boxFolders(client=c)  # Due to Box's nuances, find folder IDs
    boxDL(client=c, dlDir='/mnt/data-drive/tracking/', foldName=folderName, foldID=folderID)  # Download files to local host


if __name__ == "__main__":
    main()
