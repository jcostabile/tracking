# -*- coding: utf-8 -*-	
import numpy as np
from tifffile import imread
im = imread('/mnt/data-drive/tracking/032420/CloseUp_05_ABLF_camImage.tif_0010.tif') 
bg = imread('/mnt/data-drive/tracking/032420/CloseUp_05_ABLF_bgImage.tif_0010.tif')

import cv2
cv2.imshow('', im[0,:,:]); cv2.waitKeyEx(0); cv2.destroyAllWindows()

img = np.abs(im[0,:,:].astype(np.float32) - bg[0,:,:].astype(np.float32)).astype(np.uint8)
cv2.imshow('', np.concatenate((im[0,:,:], bg[0,:,:], img), axis=1)); cv2.waitKeyEx(0); cv2.destroyAllWindows()

 # Threshold image
__, imgT = cv2.threshold(img.copy(), 10, 255, 0)
cv2.imshow('', np.concatenate((img, imgT), axis=1)); cv2.waitKeyEx(25000); cv2.destroyAllWindows()

# Try again after opening, closing
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3)) 
imgO = cv2.morphologyEx(img.copy(), cv2.MORPH_OPEN, kernel)  # Morphological erOsion then dialation  
__, imgT = cv2.threshold(imgO.copy(), 5, 255, 0)
# cv2.imshow('', np.concatenate((img, imgO, imgT), axis=1)); cv2.waitKeyEx(25000); cv2.destroyAllWindows()

# Look into other thresholding limtis
__, imgINV5 = cv2.threshold(img.copy(), 5, 255, cv2.THRESH_BINARY_INV)
__, imgINV10 = cv2.threshold(img.copy(), 10, 255, cv2.THRESH_BINARY_INV)
__, imgINV20 = cv2.threshold(img.copy(), 20, 255, cv2.THRESH_BINARY_INV)
cv2.imshow('', np.concatenate((img, imgINV5, imgINV10, imgINV20, imgT), axis=1)); cv2.waitKeyEx(25000); cv2.destroyAllWindows()

# Good enough, find largest contours
import imutils
contours = cv2.findContours(imgT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
cnts = imutils.grab_contours(contours)
c = sorted(cnts, key = cv2.contourArea, reverse = True)[0]

# Rectangular bounding box
x,y,w,h = cv2.boundingRect(c)
maskR = np.zeros(img.shape, dtype=np.uint8) 
cv2.rectangle(maskR, (x, y), (x+w, y+h), 255, -1)

# Poly bounding box
accuracy = 0.005 * cv2.arcLength(c, True)
approx = cv2.approxPolyDP(c, accuracy, True)
maskP = np.zeros(img.shape, dtype=np.uint8) 
cv2.fillConvexPoly(maskP, approx.reshape((-1,2)), 255) 
cv2.imshow('', np.concatenate((imgT, maskR, maskP), axis=0)); cv2.waitKeyEx(25000); cv2.destroyAllWindows()

# Mask image from bounding box
imgMask = cv2.bitwise_and(imgT.copy(), imgT.copy(), mask=maskR)
# Thinning operation
imgThin = cv2.ximgproc.thinning(imgMask.copy(), thinningType=cv2.ximgproc.THINNING_GUOHALL)
# Create thinning mask
imgThinColor = cv2.cvtColor(imgThin.copy(), cv2.COLOR_GRAY2RGB) 
imgThinColor[:,:,1] = 0;
imgThinMask = cv2.bitwise_not(imgThin.copy())

# Plotting scratch code (at the moment)
imgColor = cv2.cvtColor(img.copy(), cv2.COLOR_GRAY2RGB)
img1BG = cv2.bitwise_and(imgColor.copy(), imgColor.copy(), mask = maskR)
dst = cv2.add(img1BG, imgThinColor)

cv2.imshow('Tail tracking', dst); cv2.waitKeyEx(25000); cv2.destroyAllWindows()
