# -*- coding: utf-8 -*-
# Detect blobs
detector = createDetector()
keypoints = detector.detect(imgMask.copy())
im_with_keypoints = cv2.drawKeypoints(imgMask.copy(), keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# plt.imshow(im_with_keypoints); plt.show() 

def createDetector():
	# Setup SimpleBlobDetector parameters.
	params = cv2.SimpleBlobDetector_Params()

	# Change thresholds
	params.minThreshold = 100
	params.maxThreshold = 255

	# Filter by Area.
	params.filterByArea = True
	params.minArea = 2

	# Filter by Circularity
	params.filterByCircularity = False
	params.minCircularity = 0.1

	# Filter by Convexity
	params.filterByConvexity = False
	params.minConvexity = 0.87

	# Filter by Inertia
	params.filterByInertia = True
	params.minInertiaRatio = 0.01

	# Create a detector with the parameters
	detector = cv2.SimpleBlobDetector_create(params)

	return detector


"""
#find all your connected components (white blobs in your image)
nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img, connectivity=8)
#connectedComponentswithStats yields every seperated component with information on each of them, such as size
#the following part is just taking out the background which is also considered a component, but most of the time we don't want that.
sizes = stats[1:, -1]; nb_components = nb_components - 1

# minimum size of particles we want to keep (number of pixels)
#here, it's a fixed value, but you can set it as you want, eg the mean of the sizes or whatever
min_size = 1000  

#your answer image
img2 = np.zeros((output.shape))
#for every component in the image, you keep it only if it's above min_size
for i in range(0, nb_components):
    if sizes[i] >= min_size:
        print('once')
        img2[output == i + 1] = 255
"""


import cv2
import numpy as np
from sklearn.mixture import GaussianMixture

def preprocess(x): 
   return (x - x.mean(axis=(0,1), keepdims=True)) / x.std(axis=(0,1), keepdims=True)

# EM hyper parameters
epsilon = 1e-4 # stopping criterion
R = 10 # number of re-runs
N = 2 # number of components
max_iter = 300 # stopping criterion max iterations

# read in the example image
img = cv2.imread('./data/images/test/253027.jpg')
orig = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img = preprocess(np.copy(orig))
x = img.reshape(img.shape[0]*img.shape[1],-1)

# use EM to fit N Gaussians
MoG = GaussianMixture(n_init=R,init_params='random',max_iter=max_iter,n_components=N,tol=epsilon,verbose=1)
MoG.fit(x)

# create segmentation map
clustering = MoG.predict(x).reshape(img.shape[0],img.shape[1])

# show results
plt.imshow(clustering)
plt.show()

"""
MULTIPROCESSING CODE
"""

def f(i, n):
    return (i, n, i * i + 2*n)

def main():
    import multiprocessing
    pool = multiprocessing.Pool(2)
    ans = pool.map(functools.partial(f, n=20), range(20))

    return ans


"""
Taken from 'swimBoutModel.py'
"""

	idxAgg, headAgg, tailAgg, coordAgg, posAgg = datAgg((1,3), 0), datAgg((1,75), 0), datAgg((75,20,1), 0),\
												 datAgg((75,21,2,1), 0), datAgg((1,3), 0)


class datAgg:

	def __init__(self, shape, fill):
		self.data = [np.full(shape, fill)]

	def add(self, addn, ax):
		self.data = np.concatenate((self.data, addn), axis=ax)

	def removeFirst(self, ax):
		if ax == 0:
			self.data = self.data[1:]
		elif ax == 2:
			self.data = self.data[:,:,1:]
		elif ax ==3:
			self.data = self.data[:,:,:,1:]
		else:
			raise ValueError('Only supports axis 0 and 2 currently')

	def applyMask(self, mask, ax):
		if ax == 0:
			self.data = self.data[~mask,:]
		elif ax == 2:
			self.data = self.data[:,:,~mask]
		elif ax == 3:
			self.data = self.data[:,:,:,~mask]
		else:
			raise ValueError('Only supports axis 0 and 2 currently')