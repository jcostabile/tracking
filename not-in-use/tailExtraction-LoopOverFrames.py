# -*- coding: utf-8 -*-

# Load libraries
import time
import os
import json
import numpy as np
import matplotlib.pyplot as plt
import cv2
import imutils
from tifffile import imread
from scipy.interpolate import UnivariateSpline
# import formatBehaviorImages as fbi
from sklearn.neighbors import NearestNeighbors
import networkx as nx


# Calulate midpoint
def midpoint(points):
    x1, y1, x2, y2 = points
    return (((x2+x1)/2, (y2+y1)/2))


# Determine nearest point in an array
def findNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


# Eye and swim bladder position
def bodyPositions(imgOM, centroid, maskRadius=4):
	# Find location of larvae eyes	
	eyeMask = np.zeros(imgOM.shape, np.uint8)

	# Find first most intense pixel
	(minVal0, maxVal0, minLoc0, maxLoc0) = cv2.minMaxLoc(imgOM.copy())
	cv2.circle(eyeMask, maxLoc0, maskRadius, 255, -1)
	
	# Find second most intense pixel
	(minVal1, maxVal1, minLoc1, maxLoc1) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), cv2.bitwise_not(eyeMask)))
	cv2.circle(eyeMask, maxLoc1, maskRadius, 255, -1)

	# Find third most intense pixel 
	(minVal2, maxVal2, minLoc2, maxLoc2) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), cv2.bitwise_not(eyeMask)))
	cv2.circle(eyeMask, maxLoc2, maskRadius, 255, -1)

	# Determine which pair are the eyes by finding the shortest distance
	maxLocArr = np.array((maxLoc0, centroid, maxLoc1, centroid, maxLoc2, centroid)) 
	shortestPair = np.argmin(np.sqrt(np.sum(np.diff(maxLocArr, axis=0)[::2]**2, axis=1)))

	# Return (eye1, eye2, swimbladder)
	if shortestPair == 0:
		return (maxLoc2, maxLoc1, maxLoc0)
	elif shortestPair == 1:
		return (maxLoc0, maxLoc2, maxLoc1)
	elif shortestPair == 2:
		return (maxLoc0, maxLoc1, maxLoc2)


# Determine the heading angle by thresholding (to find location of eyes and
# swim bladder) and then find contours, the centroids of those contours, and
# lastly angles between vectors created using the centroids
def determineHeading(imgOM, bodyParts, filterRadius=5, plotting=False):

	# Get eye and bladder mask
	eye1, eye2, sb = bodyParts
	thirdEye = midpoint(eye1 + eye2)

	# "Vector Between Eyes" ('vbe'), then calculate the "Orthogonal Angles Between Eyes" ('oabe')
	vbe = np.subtract(eye1, eye2)
	oabe = np.array([np.arctan2(vbe[1], vbe[0])+np.pi/2, np.arctan2(vbe[1], vbe[0])-np.pi/2])
	oabe[oabe < 0] += 2*np.pi 

	# Calculate midpoint between eyes ("third eye") and then the angle between the
	# angle between the swim bladder and third-eye ('asb')
	vbsb = np.subtract(thirdEye, sb)
	asb = np.arctan2(vbsb[1], vbsb[0])
	if asb < 0: asb += 2*np.pi

	# Determine heading angle, in 'oabe', by whichever direction is closest to the angle of the vector
	# between the swim bladder and the third-eye (asb)
	# heading = findNearest(np.array((oabe, oabe-2*np.pi)).flatten(), asb)
	# if heading < 0: heading += 2*np.pi
	heading = asb

	# Plotting code
	if plotting:
		cntsCentroids = np.asarray((eye1, eye2, sb))
		plt.imshow(imgOM, origin='lower')
		plt.plot(cntsCentroids[:2,0], cntsCentroids[:2,1], '.r')
		plt.plot(cntsCentroids[:2,0], cntsCentroids[:2,1], '-r', linewidth=3)
		plt.plot(cntsCentroids[-1,0], cntsCentroids[-1,1], '.k')
		ax = plt.gca()
		ax.arrow(thirdEye[0], thirdEye[1], 15*np.cos(heading), 15*np.sin(heading),
				 head_width=3, head_length=1.5, fc='red', ec='black') 
		ax.arrow(cntsCentroids[-1,0], cntsCentroids[-1,1], vbsb[0], vbsb[1],
				 head_width=3, head_length=1.5, fc='black', ec='black') 
		plt.show()

	return heading


# Apply Huang, et al. (2013) method to smooth tail location
def decimalPrecision(img, tailPositions, i=1, j=1):

	decimalTailPositions = np.zeros(np.shape(tailPositions))

	for n,(x,y) in enumerate(zip(tailPositions[0], tailPositions[1])):
		# Create i-by-j coordinates around one tailPosition coordinate (x,y)
		iRange, jRange = np.arange(-1*i, i+1), np.arange(-1*j, j+1)
		xi = np.tile(iRange, 2*i+1) + x
		yj = np.repeat(jRange, 2*j+1) + y

		# Find sum of i-by-j pixels around one tailPosition coordinate (x,y)
		imgIdx = np.array([xi, yj])
		xySum = np.take(img, np.ravel_multi_index(imgIdx, img.shape)).astype(np.float).sum()

		# Find x decimal precision
		xSum = 0.0
		for ii in iRange:
			xSumIdx = np.array([np.repeat(ii+x, 2*j+1), jRange+y])
			xSum += np.float(ii)*np.take(img, np.ravel_multi_index(xSumIdx, img.shape)).astype(np.float).sum()
		xPrime = np.float(x) + xSum/xySum

		# Find y decimal precision
		ySum = 0.0
		for jj in jRange:
			ySumIdx = np.array([iRange+x, np.repeat(jj+y, 2*i+1)])
			ySum += np.float(jj)*np.take(img, np.ravel_multi_index(ySumIdx, img.shape)).astype(np.float).sum()
		yPrime = np.float(y) + ySum/xySum

		# Insert into output vector
		decimalTailPositions[:,n] = [xPrime, yPrime]

	return decimalTailPositions


# Determine tail positions by skeletonizing the outline of the larva
def extractTailPosition(imgOT, imgOMN, thirdEye, frameNumber, plotting=False):

	# Find largest contours of thresheld image
	contours = cv2.findContours(imgOT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
	cnts = imutils.grab_contours(contours)
	c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

	# Rectangular bounding box (may be used)
	rectbb = np.zeros(imgOT.shape, dtype=np.uint8)
	x,y,w,h = cv2.boundingRect(c)
	cv2.rectangle(rectbb, (x, y), (x+w, y+h), 255, -1)
	# cv2.imshow('Bounding Rectangle', rectbb); cv2.waitKeyEx(0); cv2.destroyAllWindows()

	# Poly bounding box
	accuracy = 0.015 * cv2.arcLength(c, True)
	approx = cv2.approxPolyDP(c, accuracy, True)
	# cv2.drawContours(img, [approx], 0, (255, 255, 0), 2)

	# Mask image from bounding polybox
	mask = np.zeros(imgOT.shape, dtype=np.uint8) 
	cv2.fillConvexPoly(mask, approx.reshape((-1,2)), 255)   
	imgMask = cv2.bitwise_and(imgOT.copy(), imgOT.copy(), mask=mask)
	imgMaskClose = cv2.morphologyEx(imgMask.copy(), cv2.MORPH_CLOSE, (5,5))

	# Create mask to separate tail into two components
	headMask = np.zeros(imgOT.shape, dtype=np.uint8)
	cv2.circle(headMask, tuple(np.array(thirdEye, dtype=int)), 10, 255, -1)

	# Thinning operation
	wholeSkeleton = cv2.ximgproc.thinning(imgMaskClose.copy(), thinningType=cv2.ximgproc.THINNING_GUOHALL)
	# Apply mask 'headMask' to 'wholeSkeleton'
	twoSkeletonSegs = cv2.bitwise_and(wholeSkeleton.copy(), wholeSkeleton.copy(), mask=cv2.bitwise_not(headMask))
	
	# Find largest segment of 'twoSkeletonSegs'
	throwAwayMask = np.zeros(imgOT.shape, dtype=np.uint8)
	throwAwayContours = cv2.findContours(twoSkeletonSegs.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  
	throwAwayCnts = imutils.grab_contours(throwAwayContours)
	throwAwayC = sorted(throwAwayCnts, key=np.shape, reverse=True)[0]
	cv2.drawContours(throwAwayMask, [throwAwayC], -1, 255, -1) 
	imgThin = cv2.bitwise_and(twoSkeletonSegs.copy(), throwAwayMask)

	# Plotting
	if plotting:
		# Create bladder mask
		bodyMaskColor = cv2.cvtColor(headMask.copy(), cv2.COLOR_GRAY2RGB)
		bodyMaskColor[:,:,2] = 0
		
		# Create thinning mask
		imgThinColor = cv2.cvtColor(wholeSkeleton.copy(), cv2.COLOR_GRAY2RGB) 
		imgThinColor[:,:,1] = 0;
		imgThinMask = cv2.bitwise_not(wholeSkeleton.copy())

		# Plotting scratch code (at the moment)
		imgColor = cv2.cvtColor(imgOT.copy(), cv2.COLOR_GRAY2RGB)
		img1BG = cv2.bitwise_and(imgColor.copy(), imgColor.copy(), mask = imgOT)

		dst = imgColor.copy()
		dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = cv2.bitwise_not(headMask.copy()))
		dst = cv2.add(dst.copy(), bodyMaskColor)
		dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = imgThinMask)
		dst = cv2.add(dst, imgThinColor)

		# Add some text
		text = 'Frame ' + str(frameNumber)
		cv2.putText(dst, text, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

		cv2.imshow('Tail tracking', cv2.resize(dst, (480,480))); cv2.waitKeyEx(500); cv2.destroyAllWindows()


	return imgThin


# Compute the tail angles from the sorted tail positions ("sortedPoints")
def computeTailAngles(sortedPoints):
	# Linear length along the line:
	distance = np.cumsum(np.sqrt(np.sum( np.diff(sortedPoints, axis=0)**2, axis=1)))
	print('Total distance: {0:.3f}'.format(distance[-1]))
	distance = np.insert(distance, 0, 0)/distance[-1]

	# Build a list of the spline function, one for each dimension:
	splines = [UnivariateSpline(distance, coords, k=3, s=.5) for coords in sortedPoints.T]

	# Computed the spline for the asked distances:
	alpha = np.linspace(0, 1, 21)
	sortedPointsFitted = np.vstack([spl(alpha) for spl in splines]).T

	# Compute tail angles
	tailAngles = np.arctan2(*np.diff(np.fliplr(sortedPointsFitted).T))

	return (sortedPointsFitted, tailAngles)

# Set up the function
# def main():



if __name__ == "__main__":
	# main()
	# Behavioral directory of interest
	bdir = '/mnt/data-drive/tracking/062320-10'

	# if 'frameDict.json' file does not exist (maps frame number to .tif file)
	# create it and save it
	if not os.path.exists(os.path.join(bdir, 'frameDict.json')):
		frameDict = frameNumberFetching(bdir)
		dict4json = json.dumps(frameDict, cls=NumpyEncoder) 
		with open(os.path.join(bdir, 'frameDict.json'), 'w') as fp: 
			json.dump(dict4json, fp, sort_keys=True, indent=4) 
	else:
		with open(os.path.join(bdir, 'frameDict.json')) as jsonFile:
			jsonFD = json.load(jsonFile)
		frameDict = json.loads(jsonFD)

	# Extract useful data from frameDict
	files = list(frameDict.keys())  
	frames = list(frameDict.values())  
	tmpHolder = -1

	# Find location of frame in file
	# frameNumber = 341375 ## Straight
	# frameNumber = 61333  ## J-bend
	# frameNumber = 227167  ## O-bend
	# frameNumber = 227153  ## U-like
	# frameNumber = 227072  ## Hook-like
	for frameNumber in range(227360, 227361):
		# Locate file that 'frameNumber' resides
		frameIdx = np.where([np.logical_and(frameNumber >= f1, frameNumber < f2) for f1,f2 in frames])[0]
		# Within file indices of the frames
		frameOfInterest = frameNumber - frames[frameIdx[1]][0]

		# Load files
		if frameIdx[0] != tmpHolder:
			bg = imread(os.path.join(bdir, files[frameIdx[0]]))
			im = imread(os.path.join(bdir, files[frameIdx[1]]))
			tmpHolder = frameIdx[0]

		# Background subtract to form image
		img = np.abs(im[frameOfInterest,:,:].astype(np.float32) - 
					 bg[frameOfInterest,:,:].astype(np.float32)).astype(np.uint8)

		# Image processing
		# Morphologically open image using an ellipse (removes speckle around object of interest)
		kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3)) 
		imgO = cv2.morphologyEx(img.copy(), cv2.MORPH_OPEN, kernel)  # Morphological erOsion then dialation 

		# Apply median filter
		imgOM = cv2.medianBlur(imgO.copy(), 5)  # Low pass filter image

		# Threshold image
		imgOT = cv2.threshold(imgO.copy(), 5, 255, 0)[1] # Threshold image
		
		# Compute the centroid of thresheld image
		M = cv2.moments(cv2.bitwise_and(imgO.copy(), imgO.copy(), mask = imgOT))
		# M = cv2.moments(imgO.copy())
		centroid = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

		# Normalize median filtered image
		imgOMN = np.zeros(imgOM.shape, dtype=np.uint8)
		imgOMN = cv2.normalize(imgOM.copy(), imgOMN, 0, 255, cv2.NORM_MINMAX, -1)  

		# Body label positions ('chakra is the midpoint between thirdEye and swim bladder')
		eye1, eye2, sb = bodyPositions(imgO.copy(), centroid)
		thirdEye = midpoint(eye1 + eye2)
		chakra = midpoint(thirdEye + sb)

		# Tail position extraction
		tailMask = extractTailPosition(imgOT, imgOMN, thirdEye, frameNumber, False) 
		tailPositions = np.nonzero(tailMask)  
		decimalTailPositions = decimalPrecision(img, tailPositions, 2, 2)

		# Determine heading
		heading = determineHeading(imgOM, (eye1, eye2, sb), 5, False)

		# Tail position sorting
		y, x = tailPositions
		x = np.insert(x, 0, thirdEye[0])
		y = np.insert(y, 0, thirdEye[1])
		points = np.c_[x, y]
		clf = NearestNeighbors(n_neighbors=2).fit(points)
		G = clf.kneighbors_graph(mode='distance')
		T = nx.from_scipy_sparse_matrix(G)
		order = list(nx.dfs_preorder_nodes(T, source=0))[1:]
		xx = x[order]
		yy = y[order]
		points = np.c_[xx,yy]

		# Get tail angles
		points_fitted, tailAngles = computeTailAngles(points)

		if False:
			# Plotting
			plt.ion()
			plt.imshow(img, origin='lower')
			# plt.plot(decimalTailPositions[1], decimalTailPositions[0], '.r', markersize=8)
			# plt.plot(tailPositions[1], tailPositions[0], '.b', markersize=6)
			plt.xlim([80, 160]); plt.ylim([80, 160])
			# plt.plot(xx, yy, '-w')
			# plt.legend()
			ax = plt.gca()
			for p, ta in enumerate(tailAngles):
				ax.arrow(*points_fitted[p,:], 1.5*np.cos(ta), 1.5*np.sin(ta),
						 head_width=1, head_length=1, fc='black', ec='black', linewidth=2) 
			plt.plot(*points_fitted.T, '.m', markersize=8)
			plt.plot(*centroid, 'ob')
			# plt.show()
			# time.sleep(2.0)