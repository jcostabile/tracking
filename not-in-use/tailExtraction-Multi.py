# -*- coding: utf-8 -*-
"""
================================================================
Code to inspect, extract, and analyze fish behavior video/images
================================================================

*Make some comments about code function*

"""

# Load libraries
import time
import os
import re
import json
import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2
import imutils
import networkx as nx

from tifffile import imread
from scipy.interpolate import UnivariateSpline
from sklearn.neighbors import NearestNeighbors
from tqdm import tqdm
from scipy.signal import convolve2d as conv2 

import functools
import multiprocessing
import formatTrackFile as ftf

# Calulate midpoint
def midpoint(points):
    x1, y1, x2, y2 = points
    return (((x2+x1)/2, (y2+y1)/2))


# Determine nearest point in an array
def findNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


# Eye and swim bladder position
def bodyPositions(imgOM, centroid, maskRadius=4):
	# Create empty arrays for masks	
	eyeMask = np.zeros(imgOM.shape, np.uint8)
	# centroidMask = np.zeros(imgOM.shape, np.uint8)
	# # Fill 'centroidMask' with circle
	# cv2.circle(centroidMask, centroid, 2+maskRadius, 255, -1)

	# Find most intense pixel
	(__, __, __, maxLoc0) = cv2.minMaxLoc(imgOM.copy())
	cv2.circle(eyeMask, maxLoc0, maskRadius, 255, -1)
	
	# Find second most intense pixel (outside of 'eyeMask')
	(__, __, __, maxLoc1) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), cv2.bitwise_not(eyeMask)))
	cv2.circle(eyeMask, maxLoc1, maskRadius, 255, -1)

	# Find third most intense pixel (outside of 'eyeMask')
	(__, __, __, maxLoc2) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), cv2.bitwise_not(eyeMask)))
	cv2.circle(eyeMask, maxLoc2, maskRadius, 255, -1)

	# Determine which location is closest to centroid
	closeToCentroid = np.array((maxLoc0, centroid, maxLoc1, centroid, maxLoc2, centroid)) 
	nearCentroid = np.argmin(np.sqrt(np.sum(np.diff(closeToCentroid, axis=0)[::2]**2, axis=1)))

	# Determine which pair is closest
	pairs = np.array((maxLoc0, maxLoc1, maxLoc2, maxLoc0)) 
	shortestPair = np.argmin(np.sqrt(np.sum(np.diff(pairs, axis=0)**2, axis=1)))
	# Determine which pair is furthest
	# furthestPair = np.argmax(np.sqrt(np.sum(np.diff(pairs, axis=0)**2, axis=1)))

	# Return (eye1, eye2, swimbladder)
	if shortestPair == 0 and nearCentroid == 2:
		return (maxLoc0, maxLoc1, maxLoc2)				
	elif shortestPair == 1 and nearCentroid == 0:
		return (maxLoc2, maxLoc1, maxLoc0)
	elif shortestPair == 2 and nearCentroid == 1:
		return (maxLoc0, maxLoc2, maxLoc1)	
	else:
		# Create empty arrays for masks	
		eyeMask = np.zeros(imgOM.shape, np.uint8)
		centroidMask = np.zeros(imgOM.shape, np.uint8)
		# Fill 'centroidMask' with circle
		cv2.circle(centroidMask, centroid, 2+maskRadius, 255, -1)

		# Find most intense pixel within 'centroidMask'
		(__, __, __, maxLoc2) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), centroidMask))
		cv2.circle(eyeMask, maxLoc2, 2+maskRadius, 255, -1)

		# Find most intense pixel
		(__, __, __, maxLoc0) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), cv2.bitwise_not(eyeMask)))
		cv2.circle(eyeMask, maxLoc0, maskRadius, 255, -1)
		
		# Find second most intense pixel (outside of 'eyeMask')
		(__, __, __, maxLoc1) = cv2.minMaxLoc(cv2.bitwise_and(imgOM.copy(), cv2.bitwise_not(eyeMask)))
		cv2.circle(eyeMask, maxLoc1, maskRadius, 255, -1)

		# Determine which location is closest to centroid
		closeToCentroid = np.array((maxLoc0, centroid, maxLoc1, centroid, maxLoc2, centroid)) 
		nearCentroid = np.argmin(np.sqrt(np.sum(np.diff(closeToCentroid, axis=0)[::2]**2, axis=1)))

		# Determine which pair is closest
		pairs = np.array((maxLoc0, maxLoc1, maxLoc2, maxLoc0)) 
		shortestPair = np.argmin(np.sqrt(np.sum(np.diff(pairs, axis=0)**2, axis=1)))

		if shortestPair == 0 and nearCentroid == 2:
			return (maxLoc0, maxLoc1, maxLoc2)				
		elif shortestPair == 1 and nearCentroid == 0:
			return (maxLoc2, maxLoc1, maxLoc0)
		elif shortestPair == 2 and nearCentroid == 1:
			return (maxLoc0, maxLoc2, maxLoc1)				
		else:
			# Equalize over histogram using CLAHE method
			clahe = cv2.createCLAHE(clipLimit = 5, tileGridSize=(5,5))
			imgCL = clahe.apply(imgOM.copy())
			histCL = np.histogram(imgCL.flatten(), bins=50, range=(0,255))
			# Threshold based off of eyes and swim bladder area (100px) estimate
			bodyThresh = histCL[1][np.nonzero(np.cumsum(histCL[0]) > 57500.0)[0][0]+1]  # Formerly, '-1'/'57500.0' not '-0'/'57490.0'
			thrCL = cv2.threshold(imgCL.copy(), bodyThresh, 255, cv2.THRESH_BINARY)[1]

			# Find largest contours of thresheld image
			contours = cv2.findContours(thrCL.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
			cnts = imutils.grab_contours(contours)
			cntSorted = sorted(cnts, key=cv2.contourArea, reverse=True)
			cXY = []

			if len(cntSorted) >= 3:
				# Loop over contours, determine centroid
				for n in range(3):
					# compute the center of the contour
					c = cntSorted[n]
					M = cv2.moments(c)
					if M["m00"] != 0:
						cXY.append((int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"])))	
					else:
						return (None, None, None)
				return (cXY[2], cXY[1], cXY[0])
			else:
				return (None, None, None)


# Determine the heading angle by thresholding (to find location of eyes and
# swim bladder) and then find contours, the centroids of those contours, and
# lastly angles between vectors created using the centroids
def determineHeading(imgOM, bodyParts, filterRadius=5, plotting=False):

	# Get eye and bladder mask
	eye1, eye2, sb = bodyParts
	thirdEye = midpoint(eye1 + eye2)

	# "Vector Between Eyes" ('vbe'), then calculate the "Orthogonal Angles Between Eyes" ('oabe')
	vbe = np.subtract(eye1, eye2)
	oabe = np.array([np.arctan2(vbe[1], vbe[0])+np.pi/2, np.arctan2(vbe[1], vbe[0])-np.pi/2])
	oabe[oabe < 0] += 2*np.pi 

	# Calculate midpoint between eyes ("third eye") and then the angle between the
	# angle between the swim bladder and third-eye ('asb')
	vbsb = np.subtract(thirdEye, sb)
	asb = np.arctan2(vbsb[1], vbsb[0])
	if asb < 0: asb += 2*np.pi

	# Determine heading angle, in 'oabe', by whichever direction is closest to the angle of the vector
	# between the swim bladder and the third-eye (asb)
	# heading = findNearest(np.array((oabe, oabe-2*np.pi)).flatten(), asb)
	# if heading < 0: heading += 2*np.pi
	heading = asb

	# Plotting code
	if plotting:
		cntsCentroids = np.asarray((eye1, eye2, sb))
		plt.imshow(imgOM, origin='lower')
		plt.plot(cntsCentroids[:2,0], cntsCentroids[:2,1], '.r')
		plt.plot(cntsCentroids[:2,0], cntsCentroids[:2,1], '-r', linewidth=3)
		plt.plot(cntsCentroids[-1,0], cntsCentroids[-1,1], '.k')
		ax = plt.gca()
		ax.arrow(thirdEye[0], thirdEye[1], 15*np.cos(heading), 15*np.sin(heading),
				 head_width=3, head_length=1.5, fc='red', ec='black') 
		ax.arrow(cntsCentroids[-1,0], cntsCentroids[-1,1], vbsb[0], vbsb[1],
				 head_width=3, head_length=1.5, fc='black', ec='black') 
		plt.show()

	return heading


# Apply Huang, et al. (2013) method to smooth tail location
def decimalPrecision(img, tailPositions, i=1, j=1):

	decimalTailPositions = np.zeros(np.shape(tailPositions))

	for n,(x,y) in enumerate(zip(tailPositions[0], tailPositions[1])):
		# Create i-by-j coordinates around one tailPosition coordinate (x,y)
		iRange, jRange = np.arange(-1*i, i+1), np.arange(-1*j, j+1)
		xi = np.tile(iRange, 2*i+1) + x
		yj = np.repeat(jRange, 2*j+1) + y

		# Find sum of i-by-j pixels around one tailPosition coordinate (x,y)
		imgIdx = np.array([xi, yj])
		xySum = np.take(img, np.ravel_multi_index(imgIdx, img.shape)).astype(np.float).sum()

		# Find x decimal precision
		xSum = 0.0
		for ii in iRange:
			xSumIdx = np.array([np.repeat(ii+x, 2*j+1), jRange+y])
			xSum += np.float(ii)*np.take(img, np.ravel_multi_index(xSumIdx, img.shape)).astype(np.float).sum()
		xPrime = np.float(x) + xSum/xySum

		# Find y decimal precision
		ySum = 0.0
		for jj in jRange:
			ySumIdx = np.array([iRange+x, np.repeat(jj+y, 2*i+1)])
			ySum += np.float(jj)*np.take(img, np.ravel_multi_index(ySumIdx, img.shape)).astype(np.float).sum()
		yPrime = np.float(y) + ySum/xySum

		# Insert into output vector
		decimalTailPositions[:,n] = [xPrime, yPrime]

	return decimalTailPositions


# Compute the tail angles from the sorted tail positions ("sortedPoints")
def computeTailAngles(sortedPoints, distance, fittedSize=21):

	# Build a list of the spline function, one for each dimension:
	splines = [UnivariateSpline(distance, coords, k=3, s=.5) for coords in sortedPoints.T]

	# Computed the spline for the asked distances:
	alpha = np.linspace(0, 1, fittedSize)
	sortedPointsFitted = np.vstack([spl(alpha) for spl in splines]).T

	# Compute tail angles
	tailAngles = np.arctan2(*np.diff(np.flip(sortedPointsFitted,1).T))
	tailAngles[tailAngles < 0] += 2*np.pi 

	return (sortedPointsFitted, tailAngles)


# Tail sorting function
def pointSorting(tailPositions, thirdEye):
	# Separate into 'x' and 'y' vectors
	y, x = tailPositions

	# Add 'thirdEye' as first point, to sort other based off known head position
	x = np.insert(x, 0, thirdEye[0])
	y = np.insert(y, 0, thirdEye[1])
	points = np.c_[x, y]

	# Use some graph theory (credit StackOverflow) to sort coordinates
	clf = NearestNeighbors(n_neighbors=2).fit(points)
	G = clf.kneighbors_graph(mode='distance')
	T = nx.from_scipy_sparse_matrix(G)
	order = list(nx.dfs_preorder_nodes(T, source=0))[1:]
	xx = x[order]
	yy = y[order]
	pointSorted = np.flip(np.c_[xx,yy],0)  # furthest length is "0" distance

	return pointSorted


# Determine tail positions by skeletonizing the outline of the larva
def extractTailPosition(imgOT, imgOMN, thirdEye, frameNumber, thinningMethod, plotting=False):

	# Find largest contours of thresheld image
	contours = cv2.findContours(imgOT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
	cnts = imutils.grab_contours(contours)
	c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

	# Rectangular bounding box (may be used)
	rectbb = np.zeros(imgOT.shape, dtype=np.uint8)
	x,y,w,h = cv2.boundingRect(c)
	cv2.rectangle(rectbb, (x, y), (x+w, y+h), 255, -1)
	# cv2.imshow('Bounding Rectangle', rectbb); cv2.waitKeyEx(0); cv2.destroyAllWindows()

	# Poly bounding box
	accuracy = 0.015 * cv2.arcLength(c, True)
	approx = cv2.approxPolyDP(c, accuracy, True)
	# cv2.drawContours(img, [approx], 0, (255, 255, 0), 2)
	mask = np.zeros(imgOT.shape, dtype=np.uint8) 
	cv2.fillConvexPoly(mask, approx.reshape((-1,2)), 255)   

	# Apply bounding polybox as mask
	imgMask = cv2.bitwise_and(imgOT.copy(), imgOT.copy(), mask=rectbb)
	imgMaskClose = cv2.morphologyEx(imgMask.copy(), cv2.MORPH_CLOSE, (5,5))

	# Create mask to separate tail into two components
	headMask = np.zeros(imgOT.shape, dtype=np.uint8)
	cv2.circle(headMask, tuple(np.array(thirdEye, dtype=int)), 10, 255, -1)

	# Thinning operation
	if thinningMethod == 'GUOHALL':
		wholeSkeleton = cv2.ximgproc.thinning(imgMaskClose.copy(), thinningType=cv2.ximgproc.THINNING_GUOHALL)
	elif thinningMethod == 'ZHANGSUEN':
		wholeSkeleton = cv2.ximgproc.thinning(imgMaskClose.copy(), thinningType=cv2.ximgproc.THINNING_ZHANGSUEN)
	# Apply mask 'headMask' to 'wholeSkeleton'
	twoSkeletonSegs = cv2.bitwise_and(wholeSkeleton.copy(), wholeSkeleton.copy(), mask=cv2.bitwise_not(headMask))
	
	# Find largest segment of 'twoSkeletonSegs'
	throwAwayMask = np.zeros(imgOT.shape, dtype=np.uint8)
	throwAwayContours = cv2.findContours(twoSkeletonSegs.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  
	throwAwayCnts = imutils.grab_contours(throwAwayContours)
	throwAwayC = sorted(throwAwayCnts, key=np.shape, reverse=True)[0]
	cv2.drawContours(throwAwayMask, [throwAwayC], -1, 255, -1) 
	imgThin = cv2.bitwise_and(twoSkeletonSegs.copy(), throwAwayMask)

	# Plotting
	if plotting:
		# Create bladder mask
		bodyMaskColor = cv2.cvtColor(headMask.copy(), cv2.COLOR_GRAY2RGB)
		bodyMaskColor[:,:,2] = 0
		
		# Create thinning mask
		imgThinColor = cv2.cvtColor(wholeSkeleton.copy(), cv2.COLOR_GRAY2RGB) 
		imgThinColor[:,:,1] = 0;
		imgThinMask = cv2.bitwise_not(wholeSkeleton.copy())

		# Plotting scratch code (at the moment)
		imgColor = cv2.cvtColor(imgOT.copy(), cv2.COLOR_GRAY2RGB)
		img1BG = cv2.bitwise_and(imgColor.copy(), imgColor.copy(), mask = imgOT)

		dst = imgColor.copy()
		dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = cv2.bitwise_not(headMask.copy()))
		dst = cv2.add(dst.copy(), bodyMaskColor)
		dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = imgThinMask)
		dst = cv2.add(dst, imgThinColor)

		# Add some text
		text = 'Frame ' + str(frameNumber)
		cv2.putText(dst, text, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

		cv2.imshow('Tail tracking', cv2.resize(dst, (480,480))); cv2.waitKeyEx(500); cv2.destroyAllWindows()


	return imgThin


# Determine tail length and positions along it
def tailMeasures(pointSorted):
	cumDistance = np.cumsum(np.sqrt(np.sum(np.diff(pointSorted, axis=0)**2, axis=1)))
	tailLength = cumDistance[-1]
	# print('Total distance: {0:.3f}'.format(tailLength))
	distance = np.insert(cumDistance, 0, 0)/cumDistance[-1]

	return (tailLength, distance)


# Remove branching from tail mask
def removeBranches(tailMask):
	# Set range = (0,1)
	tmN = np.zeros(tailMask.shape)   
	tmN = cv2.normalize(tailMask, tmN, 0, 1, cv2.NORM_MINMAX)

	# Look for position connected by more than two
	dot = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
	dot[1,1] = 0
	filtered = conv2(tmN.astype(np.float), dot, mode='same')
	filtered[filtered <= 2] = 0

	# Separated branches ('D' = "disconnected")
	tmND = cv2.bitwise_and(tmN, cv2.bitwise_not(cv2.bitwise_and(tmN, filtered.astype(np.uint8)))) 
	__, labels = cv2.connectedComponents(tmND)
	labelUnique, labelCounts = np.unique(labels.flatten(), return_counts=True)
	longestBranchLabel = labelUnique[np.argsort(labelCounts)[-2]]
	labelMask = labels.copy()
	labelMask[labelMask != longestBranchLabel] = 0

	return cv2.bitwise_and(tailMask, tailMask, mask=labelMask.astype(np.uint8))


# Loop over all frames in give .tif stack
def func(frameNumber, bg, im, framesWithinBoundary):

	if frameNumber in framesWithinBoundary:
		# Background subtract to form image
		img = np.abs(im[frameNumber,:,:].astype(np.float32) - 
					 bg[frameNumber,:,:].astype(np.float32)).astype(np.uint8)

		### Image processing ###
		# Morphologically open image using an ellipse (removes speckle around object of interest)
		kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3)) 
		imgO = cv2.morphologyEx(img.copy(), cv2.MORPH_OPEN, kernel)  # Morphological erOsion then dialation 

		# Apply median filter
		imgOM = cv2.medianBlur(imgO.copy(), 5)  # Low pass filter image

		# Normalize median filtered image
		imgOMN = np.zeros(imgOM.shape, dtype=np.uint8)
		imgOMN = cv2.normalize(imgOM.copy(), imgOMN, 0, 255, cv2.NORM_MINMAX, -1)  

		# Threshold image
		imgOT = cv2.threshold(imgO.copy(), 5, 255, 0)[1] # Threshold image
		### END ###
		

		# There must be some nonzero pixels to continue / extract larval body information
		if np.sum(imgOT != 0) > 100:
			# Compute the centroid of thresheld image
			M = cv2.moments(cv2.bitwise_and(imgO.copy(), imgO.copy(), mask = imgOT))
			# M = cv2.moments(imgO.copy())
			centroid = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

			# Body-label positions (chakra is the midpoint between thirdEye and swim bladder)
			eye1, eye2, sb = bodyPositions(imgOM.copy(), centroid)
			if eye1 == None or eye2 == None or sb == None:
				# Eyes and/or swim bladder not found :/
				return ((frameNumber, 2, np.nan, np.nan, np.nan), np.full((20,), np.nan) , np.full((21,2), np.nan))

			thirdEye = midpoint(eye1 + eye2)
			chakra = midpoint(thirdEye + sb)

			# Determine heading
			heading = determineHeading(imgOM, (eye1, eye2, sb), 5, False)

			# Tail position extraction
			tailMask = extractTailPosition(imgOT, imgOMN, thirdEye, frameNumber, 'GUOHALL', False) 
			tailPositions = np.nonzero(tailMask)  
			decimalTailPositions = decimalPrecision(img, tailPositions, 2, 2)

			# Tail position sorting
			points = pointSorting(decimalTailPositions, thirdEye)

			# Linear length along the line
			tailLength, distance = tailMeasures(points)

			# Tail length considered too long, possibly branches
			# Scrub branches from tailMask (after GUOHALL thinning)
			if tailLength > 90:
				# The NEW tail position extraction
				tailMask = removeBranches(tailMask)
				tailPositions = np.nonzero(tailMask)  
				decimalTailPositions = decimalPrecision(img, tailPositions, 2, 2)

				# Tail position sorting
				points = pointSorting(decimalTailPositions, thirdEye)

				# Linear length along the line
				tailLength, distance = tailMeasures(points)

			# Tail length considered too long, possibly due too abberation
			# in mask due to thinning, try different thinng (ZHANGSUEN)
			if tailLength > 90:
				# Tail position extraction
				tailMask = extractTailPosition(imgOT, imgOMN, thirdEye, frameNumber, 'ZHANGSUEN', False) 
				tailMask = removeBranches(tailMask)

				tailPositions = np.nonzero(tailMask)  
				decimalTailPositions = decimalPrecision(img, tailPositions, 2, 2)

				# Tail position sorting
				points = pointSorting(decimalTailPositions, thirdEye)

				# Linear length along the line
				tailLength, distance = tailMeasures(points)									

			# Get tail angles and associated information
			pointsFitted, tailAngles = computeTailAngles(points, distance)

			# Insert information into output variables, and increase counter
			return ((frameNumber, tailLength, heading, centroid[0], centroid[1]), tailAngles, pointsFitted)

		else:
			# No pixels found in image
			return ((frameNumber, 0, np.nan, np.nan, np.nan), np.full((20,), np.nan), np.full((21,2), np.nan))
	else:
		# Larva position exceeds limits
		return ((frameNumber, 1, np.nan, np.nan, np.nan), np.full((20,), np.nan), np.full((21,2), np.nan))			


# Set up the function
def main(bdir):
	# Load 'frameDict.json'
	with open(os.path.join(bdir, 'frameDict.json')) as jsonFile:
		jsonFD = json.load(jsonFile)
	frameDict = json.loads(jsonFD)

	# Load info from frameDict
	files = list(frameDict.keys())  
	frames = list(frameDict.values())

	# Output file name creation ??
	textFileName = re.search('^(.*)(?=_(bg)|(cam)Image)', files[0]).groups()[0] + '_tail_MP.h5'

	# Sort frame number to order files
	bgIdx = [k for k, f in enumerate(files) if 'bgImage' in f]
	camIdx = [k for k, f in enumerate(files) if 'camImage' in f]
	assert len(bgIdx) == len(camIdx), 'The number of ''bg'' and ''cam'' images files are not equal.'

	# Index of associated 'bg' and 'cam' files, and total file number
	frameIdx = np.array((bgIdx, camIdx)).T
	numStacks = frameIdx.shape[0]
	stackBins = np.insert(np.asarray(frames[0:numStacks])[:,1], 0, 0)
	frame2stackBins = np.digitize(np.arange(np.min(frames), np.max(frames)), stackBins) - 1

	# Aggregate '.track' data
	camera = ftf.camParams(bdir)
	rawData = ftf.moveData(bdir)
	# Add column for the "ID" number within the tiff stackes
	rawData.insert(1, 'frame', np.arange(np.min(frames), np.max(frames)) - stackBins[frame2stackBins])
	rawData = ftf.formatMoveData(rawData, camera)
	# Reflect 'counter' in 'rawData' dataframe appropriately
	rawData['counter'] = np.arange(rawData.shape[0]) 

	# F - frame number, L - length, H - heading angle, C - centroid
	tmpMat = np.zeros((rawData.shape[0], 2)); tmpMat.fill(np.nan)
	tmpMat[:,0] = rawData['counter'].values.astype(np.int)
	FLHC = pd.DataFrame(tmpMat, columns=['counter', 'stack'])

	# One more matrix to log skipped information
	# DF columnns = ['type', 'stack', 'frame', 'counter']
	skipinfo = pd.DataFrame(columns=['type', 'stack', 'frame', 'counter'])

	## Add "dropped frames" to their appropriate tiff stack
	droppedFrames = np.nonzero(np.isnan(rawData['frame'].values))[0] 

	counterBins = np.nonzero(np.diff(rawData['frame'].values)<0)[0] + 1
	counterBins = np.insert(np.append(counterBins, rawData.shape[0]), 0, 0)

	droppedStacks = np.digitize(droppedFrames, counterBins)-1

	 # Insert stack numbers into 'FLHC'
	FLHC['stack'] = np.digitize(FLHC['counter'].values, counterBins)-1 

	 # Insert frame numbers into 'FLHC'
	# tmpFrames = np.concatenate([np.arange(x) for x in np.diff(stackBins)])
	# pixelFrames = np.delete(np.arange(rawData.shape[0]), droppedFrames)
	# FLHC.loc[pixelFrames, ('frame')] = tmpFrames.astype(np.int)

	 # Insert "dropped" information to 'skipinfo'
	droppedDat = list(zip(np.repeat('no frame', droppedFrames.shape), droppedStacks, 
					np.full(droppedFrames.shape, np.nan), droppedFrames))  
	skipinfo = skipinfo.append(pd.DataFrame(droppedDat, columns=skipinfo.columns), ignore_index=True) 

	## Include frames exceeding radial boundary to 'skipinfo'
	 # Then, determine which frames fall outside radial boundary (x=3 mm from swim chamber wall)
	exceedsRadialLimit = ftf.framesNearBoundary(rawData, camera, 3)
	exceedStacks = np.digitize(exceedsRadialLimit, counterBins)-1
	exceedFrames = rawData['frame'].iloc[exceedsRadialLimit].values

	 # Insert "exceeding" information to 'skipinfo'
	exceedDat = list(zip(np.repeat('exceeds limit', exceedFrames.shape), exceedStacks, 
						 exceedFrames, exceedsRadialLimit)) 
	skipinfo = skipinfo.append(pd.DataFrame(exceedDat, columns=skipinfo.columns), ignore_index=True) 

	# Create temporary lists for concatenating multiprocess output
	fi, ta, tc = [], [], []

	# Loop over all files behavioral session
	for stackOfInterest in tqdm(range(numStacks), desc='Looping over .tif stacks', # range(numStacks)
							   ascii=False, ncols=75, position=0):

		# Load file corresponding to 'stackOfInterest'
		bg = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,0]]))
		im = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,1]]))

		# Determine total frames in file
		assert im.shape[0] == bg.shape[0], \
				'Different number of frames between ''bg'' and ''cam'' files.'
		
		# Frames to loop ("multiprocess") over
		framesInStack = im.shape[0]
		framesWithinBoundary = np.delete(np.arange(framesInStack), exceedFrames[exceedStacks==stackOfInterest]) 

		##### Multiprocess!! #####
		pool = multiprocessing.Pool(processes=multiprocessing.cpu_count()-2)
		out = pool.map(functools.partial(func, bg=bg, im=im, framesWithinBoundary=framesWithinBoundary), 
										 range(framesInStack))
		pool.close()
		pool.join()
		#####----------------#####

		# Convert output to more easily manageable numpy arrays
		fiList, taList, tcList = map(list, zip(*out))
		fi.extend(fiList)
		ta.extend(taList)
		tc.extend(tcList)

	# Insert dropped frames into 'xxList'
	for drop in droppedFrames:
		fi.insert(drop, np.full((5,), np.nan)) 
		ta.insert(drop, np.full((20,), np.nan))
		tc.insert(drop, np.full((21,2), np.nan))

	# Convert to numpy arrays
	frameinfo = np.asarray(fi)
	tailangles = np.asarray(ta) 
	tailcoords = np.asarray(tc) 

	# Insert info into 'FLHC'
	FLHC['frame'] = frameinfo[:,0]
	FLHC['length'] = frameinfo[:,1]
	FLHC['heading'] = frameinfo[:,2]
	FLHC['cX'] = frameinfo[:,3]
	FLHC['cY'] = frameinfo[:,4]

	# Add "no body" to skip info (label = 2) 
	# DF columnns = ['type', 'stack', 'frame', 'counter']
	numNoBody = np.nonzero(frameinfo[:,1] == 2)[0].shape[0]
	addskipNB = list(zip(np.repeat('no body', numNoBody), 
						 np.digitize(np.nonzero(frameinfo[:,1] == 2)[0], counterBins)-1, 
						 frameinfo[np.nonzero(frameinfo[:,1] == 2)[0],0].astype(np.int),  
						 np.nonzero(frameinfo[:,1] == 2)[0]))
	skipinfo = skipinfo.append(pd.DataFrame(addskipNB, columns=skipinfo.columns), ignore_index=True) 

	# Add "no pixels" to skip info (label = 0)
	# DF columnns = ['type', 'stack', 'frame', 'counter']
	numNoPixels = np.nonzero(frameinfo[:,1] == 0)[0].shape[0]
	addskipNP = list(zip(np.repeat('no pixels', numNoPixels), 
						 np.digitize(np.nonzero(frameinfo[:,1] == 0)[0], counterBins)-1,
						 frameinfo[np.nonzero(frameinfo[:,1] == 0)[0],0].astype(np.int),
						 np.nonzero(frameinfo[:,1] == 0)[0]))
	skipinfo = skipinfo.append(pd.DataFrame(addskipNP, columns=skipinfo.columns), ignore_index=True) 

	## Save the sheeit to HDF5 file
	# DataFrames to HDF5, easy
	FLHC.to_hdf(os.path.join(bdir, textFileName), key='flhc', mode='w')
	skipinfo.to_hdf(os.path.join(bdir, textFileName), key='skipinfo', mode='r+')
	# Numpy arrays to HDF5, less easy (but still easy)
	with h5py.File(os.path.join(bdir, textFileName), 'r+') as h5f:
		h5f.create_dataset('tailangles', data=tailangles)
		h5f.create_dataset('tailcoords', data=tailcoords)


if __name__ == "__main__":
	multiprocessing.freeze_support()
	
	# Behavioral directory of interest
	bdir = '/mnt/data-drive/tracking/081420-09'

	# Create 'frameDict' variable is non-existent
	ftf.createFrameDict(bdir)

	# # Process tail movement
	# startTime = time.time()
	# main(bdir)
	# print('Runtime: {0}'.format(time.time() - startTime))