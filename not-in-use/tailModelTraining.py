# -*- coding: utf-8 -*-
# Accepts a training set and create a straight-forward NN model

import mmap
import os
from subprocess import run, PIPE
import re
import pandas as pd

# Not sure if definition has utility
def rcloneCheck():
    boxpath = '/mnt/data-drive/osu-box'
    chkOut = run(['findmnt', boxpath], stdout=PIPE)
    if not chkOut.stdout:
        print('Box directory not mounted to specified location! Mounting \'box\' with rclone...')
        mntCmd = 'rclone mount box: ' + boxpath + ' --daemon --cache-dir ' + boxpath + \
                 '/.osu-box-cache --vfs-cache-mode full'
        mntOut = run(mntCmd.split(), shell=True, stderr=PIPE, stdout=PIPE)
        if not mntOut:
            print('All good buddy!')
    else:
        print('All good buddy!')


# Extract camera parameters from '.info' file
def camParams(trainDir):
    infoFile = [f for f in os.listdir(trainDir) if f.endswith('.info')]
    with open(os.path.join(trainDir, infoFile[0]), 'rb', 0) as file, \
            mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
        frameRate = int(re.findall(br'Frame rate: (\d+)', s)[0].decode('UTF-8'))
        resolution = int(re.findall(br'Resolution: (\d+)', s)[0].decode('UTF-8'))

    return {'rate': frameRate, 'res': resolution}


def moveData(trainDir, camera):
    # Locate '.track' file
    trackFile = [f for f in os.listdir(trainDir) if f.endswith('.track')]
    # Load '.track' file as pandas dataframe
    mdata = pd.read_csv(os.path.join(trainDir, trackFile[0]), sep = '\t', header = None)
    # Name columns
    mdata.columns = ['frame', 'x', 'y', 'headAngle']

    # Code to update frame numbers to reflect dropped frames
    # dropFrameIndex = mdata.loc[mdata['frame'].diff() > 1].index - 1

    # Calculations
    mdata['time'] = mdata['frame'] / camera['rate']
    mdata['time'] = mdata['time'].map('{:10.3f}'.format)
    mdata['dist'] = (((mdata['x'].diff())/camera['res'])**2 + ((mdata['y'].diff())/camera['res'])**2)**(1/2)
    mdata['speed'] = mdata['dist'] / camera['rate']
    
    return mdata


def main():
    tempDir = '/mnt/data-drive/osu-box/Behavior/TrackAndWrite/032420'
    rcloneCheck()
    camera = camParams(tempDir)
    moveData(tempDir, camera)

if __name__ == "__main__":
    main()
