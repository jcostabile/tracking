# -*- coding: utf-8 -*-
"""
================================================================
Code to inspect, extract, and analyze fish behavior video/images

Orignally for 8px/mm resolution images
================================================================

*Update to date as of 12/2/20*

"""

# Load libraries
import time
import os
import psutil
import glob
import re
import json
import h5py
import warnings
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2
import imutils
import networkx as nx

from tifffile import imread
from scipy.interpolate import UnivariateSpline
from sklearn.neighbors import NearestNeighbors
from tqdm import tqdm
from scipy.signal import convolve2d as conv2 
from scipy.ndimage import median_filter

import functools
import multiprocessing
import formatTrackFile as ftf

def limitCpu():
    ## is called at every process start
    p = psutil.Process(os.getpid())
    # set to lowest priority, this is windows only, on Unix use ps.nice(19)
    p.nice(-15)


# Gamma correction
def adjustGamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")
    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)


# Calulate midpoint
def midpoint(points):
    x1, y1, x2, y2 = points
    return (((x2+x1)/2, (y2+y1)/2))


# Determine nearest point in an array
def findNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


# Find body positions from image via "numPixel" number of highest pixel intensities
def bodyPositions(pxMult, histMN, imgSize, imgX):
	tmp = None
	for numPixel in np.array([10, 9 ,8])*pxMult:
		# Threshold based off of eyes and swim bladder area (numPixel) estimate
		bodyThresh = histMN[1][np.nonzero(np.cumsum(histMN[0]) > (imgSize - numPixel))[0][0]]  

		# Threshold normalized image
		imgHT = cv2.threshold(imgX.copy(), bodyThresh, 255, cv2.THRESH_BINARY)[1]

		# Extract contours of eyes and swim bladder
		contoursNorm = cv2.findContours(imgHT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
		cntsNorm = imutils.grab_contours(contoursNorm)
		cnorm = sorted(cntsNorm, key=cv2.contourArea, reverse=True)

		if len(cnorm) == 3:  ## hope for 3 contours
			break
		elif len(cnorm) == 2:  ## will accept two (temporarily)
			tmp = cnorm

	if tmp != None and len(cnorm) != 3:
		return tmp
	else:
		return cnorm


# Determine the heading angle by thresholding (to find location of eyes and
# swim bladder) and then find contours, the centroids of those contours, and
# lastly angles between vectors created using the centroids
def determineHeading(imgM, thirdEye, sb, chakra, lastTailCoord, plotting=False):
	# Ol' Jamie is tired of dealing with radians. Ol' Jamie needs three angles...

	# Calculate the angle between the swim bladder and third-eye ('asb')
	vbsb = np.subtract(thirdEye, sb)
	asb = np.arctan2(vbsb[1], vbsb[0])  ## angle 1
	if asb < 0: asb += 2*np.pi

	if asb >= np.pi: 
		asbPrime = asb - np.pi  ## angle 2 = angle 1 rotated by 180 deg
	elif asb < np.pi:
		asbPrime = asb + np.pi  ## angle 2 = angle 1 rotated by 180 deg

	# Calculate the angle between the last tail coordinate and chakra
	vbltc = np.subtract(chakra, lastTailCoord)
	altc = np.arctan2(vbltc[1], vbltc[0])
	if altc < 0: altc += 2*np.pi  ## angle 3

	# Now find the differences between angles 1&3 and 2&3
	xa1, ya1 = (np.cos(asb), np.sin(asb))
	xa2, ya2 = (np.cos(asbPrime), np.sin(asbPrime))
	xa3, ya3 = (np.cos(altc), np.sin(altc))

	ab31 = np.abs(np.arctan2(np.cross([xa3, ya3], [xa1, ya1]), np.dot([xa3, ya3], [xa1, ya1])))
	ab32 = np.abs(np.arctan2(np.cross([xa3, ya3], [xa2, ya2]), np.dot([xa3, ya3], [xa2, ya2])))

	# Whichever "angle between" (e.g., 'ab') is smaller is the correct one!
	if ab31 <= ab32:
		heading = asb
	elif ab31 > ab32:
		heading = asbPrime

	# Plotting code
	if plotting:
		plt.imshow(imgM, origin='upper')
		plt.plot(*thirdEye, '.r')
		plt.plot(*sb, '.k')
		ax = plt.gca()
		ax.arrow(thirdEye[0], thirdEye[1], 15*np.cos(heading), 15*np.sin(heading),
				 head_width=3, head_length=1.5, fc='red', ec='black') 
		ax.arrow(sb[0], sb[1], vbsb[0], vbsb[1],
				 head_width=3, head_length=1.5, fc='black', ec='black') 
		plt.show()

	return heading


# Apply Huang, et al. (2013) method to smooth tail location
def decimalPrecision(img, tailPositions, i=1, j=1):

	decimalTailPositions = np.zeros(np.shape(tailPositions))

	for n,(x,y) in enumerate(zip(tailPositions[0], tailPositions[1])):
		# Create i-by-j coordinates around one tailPosition coordinate (x,y)
		iRange, jRange = np.arange(-1*i, i+1), np.arange(-1*j, j+1)
		xi = np.tile(iRange, 2*i+1) + x
		yj = np.repeat(jRange, 2*j+1) + y

		# Find sum of i-by-j pixels around one tailPosition coordinate (x,y)
		imgIdx = np.array([xi, yj])
		xySum = np.take(img, np.ravel_multi_index(imgIdx, img.shape)).astype(np.float).sum()

		# Find x decimal precision
		xSum = 0.0
		for ii in iRange:
			xSumIdx = np.array([np.repeat(ii+x, 2*j+1), jRange+y])
			xSum += np.float(ii)*np.take(img, np.ravel_multi_index(xSumIdx, img.shape)).astype(np.float).sum()
		xPrime = np.float(x) + xSum/xySum

		# Find y decimal precision
		ySum = 0.0
		for jj in jRange:
			ySumIdx = np.array([iRange+x, np.repeat(jj+y, 2*i+1)])
			ySum += np.float(jj)*np.take(img, np.ravel_multi_index(ySumIdx, img.shape)).astype(np.float).sum()
		yPrime = np.float(y) + ySum/xySum

		# Insert into output vector
		decimalTailPositions[:,n] = [xPrime, yPrime]

	return decimalTailPositions


# Compute the tail angles from the sorted tail positions ("sortedPoints")
def computeTailAngles(sortedPoints, distance, fittedSize=21):

	# Build a list of the spline function, one for each dimension:
	splines = [UnivariateSpline(distance, coords, k=3, s=.5) for coords in sortedPoints.T]

	# Computed the spline for the asked distances:
	alpha = np.linspace(0, 1, fittedSize)
	# alpha = np.delete(alpha, 1)  ## remove the second element, first and second points don't deviate much
	sortedPointsFitted = np.vstack([spl(alpha) for spl in splines]).T

	# Compute tail angles
	tailAngles = np.arctan2(*np.diff(np.flip(sortedPointsFitted,1).T))
	tailAngles[tailAngles < 0] += 2*np.pi 

	return (sortedPointsFitted, tailAngles)


# Tail sorting function
def pointSorting(coords, bodyPart):

	if coords.shape[-1] > 3:
		# Append 'bodyPart' to closest end of 'coords'
		distFromBP = np.linalg.norm(bodyPart-np.fliplr(coords.T), axis=-1) 

		# Separate into 'x' and 'y' vectors
		if distFromBP[0] < distFromBP[-1]:
			y, x = coords
		else:
			y, x = np.fliplr(coords) 

		# Add 'bodyPart' as first point, to sort other based off known head position
		x = np.insert(x, 0, bodyPart[0])
		y = np.insert(y, 0, bodyPart[1])
		points = np.c_[x, y]

		# Use some graph theory (credit StackOverflow) to sort coordinates
		clf = NearestNeighbors(n_neighbors=3).fit(points)
		G = clf.kneighbors_graph(mode='distance')
		# T = nx.from_scipy_sparse_matrix(G)
		# order = list(nx.dfs_preorder_nodes(T, source=0))[1:]

		# Loop to order points by shortest distance
		cols = np.arange(1, G.toarray().shape[1])
		order = np.full(cols.shape, np.nan)
		i,rc = (0,0)
		while np.isnan(order).any():
			row = G.toarray()[i]  ## Distances from one position
			d = np.ma.masked_where(row==0, row)[cols]  ## Masked to ignore zeros
			order[rc] = cols[np.argmin(d)]  ## Minimum distance
			cols = np.setdiff1d(cols, order[rc])  ## Remove position from search
			i = int(order[rc])  ## Inspect the next position
			rc += 1  ## Update "rowcounter"

		# Sort points based off 'order'
		xx = x[order.astype(int)]
		yy = y[order.astype(int)]
		pointSorted = np.flip(np.c_[xx,yy],0)  # furthest length is "0" distance

		return pointSorted
	else:  ## Not enough coords to sort
		return np.array([[1,2], [3,4]])


# Determine tail positions by skeletonizing the outline of the larva
def extractTailPosition(threshImg, bodyPart, resolutionAdjustment, frameNumber, thinningMethod, plotting=False):

	# Create mask to separate tail into two components
	headMask = np.zeros(threshImg.shape, dtype=np.uint8)
	cv2.circle(headMask, tuple(np.array(bodyPart, dtype=int)), int(3*resolutionAdjustment), 255, -1)

	# Thinning operation
	if thinningMethod == 'GUOHALL':
		wholeSkeleton = cv2.ximgproc.thinning(threshImg.copy(), thinningType=cv2.ximgproc.THINNING_GUOHALL)
	elif thinningMethod == 'ZHANGSUEN':
		wholeSkeleton = cv2.ximgproc.thinning(threshImg.copy(), thinningType=cv2.ximgproc.THINNING_ZHANGSUEN)

	if np.sum(wholeSkeleton != 0) > 10:
		# Apply mask 'headMask' to 'wholeSkeleton'
		twoSkeletonSegs = cv2.bitwise_and(wholeSkeleton.copy(), wholeSkeleton.copy(), mask=cv2.bitwise_not(headMask))	
		# Find largest segment of 'twoSkeletonSegs'
		throwAwayMask = np.zeros(threshImg.shape, dtype=np.uint8)
		throwAwayContours = cv2.findContours(twoSkeletonSegs.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  
		throwAwayCnts = imutils.grab_contours(throwAwayContours)
		throwAwayC = sorted(throwAwayCnts, key=np.shape, reverse=True)[0]
		cv2.drawContours(throwAwayMask, [throwAwayC], -1, 255, -1) 
		imgThin = cv2.bitwise_and(twoSkeletonSegs.copy(), throwAwayMask)

		# Plotting
		if plotting:
			# Create bladder mask
			bodyMaskColor = cv2.cvtColor(headMask.copy(), cv2.COLOR_GRAY2RGB)
			bodyMaskColor[:,:,2] = 0
			
			# Create thinning mask
			imgThinColor = cv2.cvtColor(wholeSkeleton.copy(), cv2.COLOR_GRAY2RGB) 
			imgThinColor[:,:,1] = 0;
			imgThinMask = cv2.bitwise_not(wholeSkeleton.copy())

			# Plotting scratch code (at the moment)
			imgColor = cv2.cvtColor(threshImg.copy(), cv2.COLOR_GRAY2RGB)
			img1BG = cv2.bitwise_and(imgColor.copy(), imgColor.copy(), mask = threshImg)

			dst = imgColor.copy()
			dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = cv2.bitwise_not(headMask.copy()))
			dst = cv2.add(dst.copy(), bodyMaskColor)
			dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = imgThinMask)
			dst = cv2.add(dst, imgThinColor)

			# Add some text
			text = 'Frame ' + str(frameNumber)
			cv2.putText(dst, text, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

			cv2.imshow('Tail tracking', cv2.resize(dst, (480,480))); cv2.waitKeyEx(2500); cv2.destroyAllWindows()

		return imgThin
	else:
		return np.zeros(threshImg.shape, dtype=np.uint8)


# Determine tail length and positions along it
def tailMeasures(pointSorted):
	cumDistance = np.cumsum(np.sqrt(np.sum(np.diff(pointSorted, axis=0)**2, axis=1)))
	tailLength = cumDistance[-1]
	# print('Total distance: {0:.3f}'.format(tailLength))
	distance = np.insert(cumDistance, 0, 0)/cumDistance[-1]

	return (tailLength, distance)


# Remove branching from tail mask
def removeBranches(tailMask):
	# Set intensity range = (0,1)
	tmN = np.zeros(tailMask.shape)   
	tmN = cv2.normalize(tailMask, tmN, 0, 1, cv2.NORM_MINMAX)

	# Look for position connected by more than two
	dot = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
	dot[1,1] = 0
	filtered = conv2(tmN.astype(np.float), dot, mode='same')
	filtered[filtered <= 2] = 0

	# Separated branches ('D' = "disconnected")
	tmND = cv2.bitwise_and(tmN, cv2.bitwise_not(cv2.bitwise_and(tmN, filtered.astype(np.uint8)))) 
	__, labels = cv2.connectedComponents(tmND)
	labelUnique, labelCounts = np.unique(labels.flatten(), return_counts=True)
	longestBranchLabel = labelUnique[np.argsort(labelCounts)[-2]]
	labelMask = labels.copy()
	labelMask[labelMask != longestBranchLabel] = 0

	return cv2.bitwise_and(tailMask, tailMask, mask=labelMask.astype(np.uint8))


# Find pixel in thresheld image that are connected only by one corner
def removeSingleCornerPixels(threshImg):
	# Create hit-and-miss kernels
	kul = np.array(([1, -1, 0], 
					[-1, 1, 0], 
					[0, 0, 0]), dtype="int") 
	kll = np.array(([0, 0, 0], 
					[-1, 1, 0], 
					[1, -1, 0]), dtype="int") 
	kur = np.array(([0, -1, 1], 
					[0, 1, -1], 
					[0, 0, 0]), dtype="int") 
	klr = np.array(([0, 0, 0], 
					[0, 1, -1], 
					[0, -1, 1]), dtype="int")

	# Apply kernels to incoming thresheld image
	imgul = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kul) 
	imgll = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kll) 
	imgur = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kur) 
	imglr = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, klr) 

	# Union of the kernel images
	imgKU = cv2.bitwise_or(imgul, cv2.bitwise_or(imgll, cv2.bitwise_or(imgur, imglr)))

	# Subtracted the kernel images from thresheld
	threshSub = cv2.bitwise_and(threshImg.copy(), cv2.bitwise_not(imgKU))

	# Remove spurious pixels and return
	return removeSpuriousPixels(threshSub.copy())


# Remove spurious pixels from thresheld image
def removeSpuriousPixels(threshImg):
	# Create hit-and-miss kernels
	kspur = np.array(([-1, -1, -1], 
					  [-1, 1, -1], 
				      [-1, -1, -1]), dtype="int")
	# Remove spurious pixels and return
	imgSpur = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kspur) 
	return cv2.bitwise_and(threshImg.copy(), cv2.bitwise_not(imgSpur))


# Applies median filter, averaging, and smoothing in calculation of tail tip
def anglePrep(ta, hd, frameRate, tipLength=7, filtWindow=5):

	# Apply median filter to tail angles
	nanTA = np.any(np.isnan(ta), axis=1)
	ta[nanTA] = np.ones(np.shape(ta)[1]) - np.inf
	filtA = np.transpose(median_filter(ta.T, size=(1, 3), mode='nearest'))
	infTA = np.any(np.isinf(filtA), axis=1)
	filtA[infTA] = np.ones(np.shape(filtA)[1]) + np.nan

	# Same approach for heading
	nanHD = np.isnan(hd)
	hd[nanHD] = -np.inf
	filtH = median_filter(hd[:,np.newaxis], size=(3,1), mode='nearest')
	infHD = np.isinf(filtH)
	filtH[infHD] = np.nan

	# Find tail angle relative to heading
	angles = filtH - filtA

	# Since this is in radians, to average correctly must separate into x,y coordinates
	yavg = np.mean(np.sin(angles[:,:tipLength]), axis=1)
	xavg = np.mean(np.cos(angles[:,:tipLength]), axis=1)

	# Find change in angle using dot-product rule
	u = np.hstack((xavg[:-1, np.newaxis], yavg[:-1, np.newaxis]))
	v = np.hstack((xavg[1:, np.newaxis], yavg[1:, np.newaxis]))
	uv = np.sum(u*v, axis=1)
	# Calculate magnitudes based on arctan(sin,cos)
	diffAngles = np.arctan2(np.cross(u,v), uv)
	diffAngles[np.isnan(diffAngles)] = 0

	# Calculate derivative of angular difference, and filter
	dervDiffAngles = np.abs(diffAngles / frameRate**-1)
	smoothedMeanAngles = filtfilt(dervDiffAngles, filtWindow)
	smoothedMeanAngles = np.insert(smoothedMeanAngles, 0, 0)

	# sam = filtfilt(np.abs(meanAngles), filtWindow)
	# # Plot a little
	# plt.plot(frameIdx, smoothedMeanAngles, '.-')
	# plt.plot(frameIdx, sam)

	return smoothedMeanAngles, filtA, filtH.flatten()


# Bout detection modified from MH script
def detectBouts(swimMetric, minFramesPerBout, frameRate, thresholdValue=2, zeroLimit=2):
	"""
	Detect bouts based on emprically determined characterstics within a trace
	of a swim metric maintaining a certain value.
	"""
	#the threshold is determined mostly empirically and probably needs
	#to be changed if our framerate deviates much from 250 - this depends on
	#how aggressively the position traces are smoothed in relation to the
	#framerate

	#bouts should have a clear peak, hence the peak metric should not be
	#maintained for > ~20ms (0.02s)
	maxFramesAtPeak = 0.02/frameRate**-1

	#threshold swim metric
	smetric = swimMetric.copy()
	smetric[np.isnan(smetric)] = 0
	smetric[smetric<thresholdValue] = 0

	#master indexer
	i = 0  ## python like any real language uses 0-based indexing!
	bouts = np.zeros((1,5))  ## '5' because currently tracking five features

	l = np.size(smetric)
	zeroCounter = 0

	while (i < l):
		if smetric[i]==0:
			i += 1
		else:
			# Reset zero counter
			zeroCounter = 0
			#we found a potential "start of bout" - loop till end collecting
			#all speeds
			bStart = i
			b = smetric[i]
			i += 1
			# while(i<l and smetric[i]>0):
			while (i<l and zeroCounter <= zeroLimit):
				if smetric[i] == 0:
					zeroCounter += 1
				else:
					zeroCounter = 0

				b = np.append(b,smetric[i])             
				i += 1
			peak = b[b==b.max()]
			#since we loop until smetric==0 the last part 
			#of the bout is in the frame before this frame
			bEnd = i-(zeroLimit+1)

			if (np.size(peak) <= maxFramesAtPeak and np.size(b) >= minFramesPerBout):
				#we have a valid, singular bout
				peakframe = np.nonzero(b==np.max(b))
				peakframe = peakframe[0][0]
				#put peakframe into context
				peakframe = bStart + peakframe
				#a bout should not start on the peakframe
				if(bStart==peakframe):
				    bStart = bStart-1
				# bt = np.r_[bStart,peakframe,bEnd,np.sum(b,dtype=float)/frameRate,np.max(b)]
				bt = np.r_[bStart, peakframe, bEnd, np.trapz(b, dx=1/frameRate), np.max(b)]
				bouts = np.vstack((bouts,bt))

	if(np.size(bouts)>1):
		bouts = bouts[1::,:]

	return bouts


# Boxcar filter of size 'window_len', forward and backwards (zero-phase)
def filtfilt(x, window_len):
	"""
	Performs zero-phase digital filtering with a boxcar filter. Data is first
	passed through the filter in the forward and then backward direction. This
	approach preserves peak location.
	"""
	if x.ndim != 1:
		raise ValueError("filtfilt only accepts 1 dimension arrays.")

	if x.size < window_len:
		raise ValueError("Input vector needs to be bigger than window size.")


	if window_len<3:
		return x

	#pad signals with reflected versions on both ends
	s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]

	#create our smoothing window
	w=np.ones(window_len,'d')

	#convolve forwards
	y=np.convolve(w/w.sum(),s,mode='valid')

	#convolve backwards
	yret=np.convolve(w/w.sum(),y[-1::-1],mode='valid')

	return yret[-1::-1]


# Loop over all frames in give .tif stack
def func(frameNumber, bg, im, framesWithinBoundary, camera, rawData, radialLimit, plotting=False):

	# Properties based off camera resolution
	if camera['res'] == 8:
		pxArea = (1,1)
		pxMult = 1
		pxThreshLo, pxThreshHi = (50, 170)
		hw, hl, l = (1, 0.5, 5)
		acceptableArea = np.array([[20, 20], [20, 60], [60, 60], [60, 20]], dtype=np.int32) 
	elif camera['res'] == 24:
		pxArea = (2,2)
		pxMult = 5
		pxThreshLo, pxThreshHi = (90, 270)
		acceptableArea = np.array([[60, 60], [60, 180], [180, 180], [180, 60]], dtype=np.int32) 
		hw, hl, l = (3, 1.5, 15)

	try:
		if frameNumber in framesWithinBoundary:
			# Background subtract to form image
			img = np.abs(im[frameNumber,:,:].astype(np.float32) - 
						 bg[frameNumber,:,:].astype(np.float32)).astype(np.uint8)

			# Threshold image
			imgOT = cv2.threshold(img.copy(), 5, 255, 0)[1] # Threshold image
			imgOT = removeSpuriousPixels(imgOT)
			# Morphologically open image using an ellipse (removes speckle around object of interest)
			# kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2,2)) 
			# imgOT = cv2.morphologyEx(imgOT.copy(), cv2.MORPH_OPEN, kernel)  # Morphological erOsion then dialation 
			# cv2.fastNlMeansDenoising(imgOT, imgOT, 3, 3)
			# imgOT = remove_small_objects(imgOT, min_size=16, connectivity=1)

			# There must be some nonzero pixels to continue / extract larval body information
			if np.sum(imgOT != 0) > pxThreshLo and np.sum(imgOT != 0) < pxThreshHi:
				# Find largest contours of thresheld image
				contours = cv2.findContours(imgOT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
				cnts = imutils.grab_contours(contours)
				c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

				# Exclude all pixels not contained within the largest contour
				mask = np.zeros(imgOT.shape, dtype=np.uint8) 
				cv2.drawContours(mask, [c], -1, 255, -1)
				imgM = cv2.bitwise_and(img, img, mask=mask)

				# Histogram normalization
				imgMN = np.zeros(img.shape, dtype=np.uint8)
				cv2.normalize(imgM, imgMN, 255.0, 0.0, cv2.NORM_INF)

				# Gamma correction
				imgG = adjustGamma(imgMN.copy(), gamma=0.8)

				# Plot images
				if plotting:
					img4plt = np.concatenate((img, imgOT, imgMN, imgG), axis=1)
					plt.figure(); plt.imshow(img4plt)
				# breakpoint()
				# Adaptive thresholding via threshold of histogram
				histMN = np.histogram(imgMN.flatten(), bins=50, range=(0,255))
				# Identify 3 contours (hopefully)
				cnorm = bodyPositions(pxMult, histMN, img.size, imgMN)
				# Doesn't always work so try gamma corrected image
				if not len(cnorm) == 3:
					_cnorm = bodyPositions(pxMult, histMN, img.size, imgG)
					if len(_cnorm) == 2 or len(_cnorm) == 3:
						cnorm = _cnorm

				# Find center of contour by taking average of largest contours (within acceptable area)
				centroids = []
				for cn in cnorm: 
					cnAvg = np.average(cn, axis=0)[0]

					# Check that all centroids are within main portion of image
					if cv2.pointPolygonTest(acceptableArea, tuple(cnAvg), False) != -1.0:
						centroids.append(tuple(np.round(cnAvg, decimals=3)))
					else:
						print('Variable *cnorm*: {}'.format(cn))
						# Mistracked larval position
						print('Mistracked larval position (#4), Frame {}'.format(frameNumber)); return

				if len(centroids) == 3:  ## Identify body parts (i.e., "eyes" and "swimbladder")
					# Identify the shortest pair
					pairs = centroids.copy()
					pairs.append(pairs[0])
					shortestPair = np.argmin(np.sqrt(np.sum(np.diff(pairs, axis=0)**2, axis=1)))

					# Point eyes and swimbladder ('sb')
					eyes = [pairs[shortestPair], pairs[shortestPair+1]]
					sb = list(set(centroids) - set(eyes))[0]
					thirdEye = midpoint(eyes[0] + eyes[1])
				elif len(centroids) == 2:  ## Identify body parts (i.e., "third eye" and "swimbladder")
					# Threshold gamma corrected image using Otsu method
					bodyMask = cv2.threshold(imgG, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
					M = cv2.moments(bodyMask)
					bodyCenter = (M["m10"]/M["m00"], M["m01"]/M["m00"])

					dists = np.linalg.norm(np.asarray(bodyCenter) - np.asarray(centroids), axis=1)
					thirdEye = centroids[np.argmax(dists)]
					sb = centroids[np.argmin(dists)]
				else:
					plt.figure(); plt.imshow(imgMN, origin='upper')
					plt.xlim(acceptableArea[1]); plt.ylim(acceptableArea[-1])
					print(cnorm)
					# Eyes and/or swim bladder not found :/
					print('Eyes and/or swim bladder not found (#2), Frame {}'.format(frameNumber)); return

				# Midpoint between eyes and swimbladder
				chakra = midpoint(thirdEye + sb)

				# Threshold once more
				imgMNT = cv2.threshold(imgMN, 5, 255, cv2.THRESH_BINARY)[1]
				imgMNT = removeSingleCornerPixels(imgMNT)
				# breakpoint()
				if np.sum(imgMNT != 0) < pxThreshLo or np.sum(imgMNT != 0) > pxThreshHi:
					print('Body subthreshold (#5), Frame {}'.format(frameNumber)); return

				# Tail position extraction
				tailMask = extractTailPosition(imgMNT, chakra, camera['res']/8, frameNumber, 'ZHANGSUEN', False) 
				tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
				decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
				points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
				tailLength, distance = tailMeasures(points)  ## Linear length along the line

				# Tail length considered too long, possibly branches
				# Scrub branches from tailMask (after ZHANGSUEN thinning)
				if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:
					if camera['res'] == 24:
						# Re-attempt after a branch removal process
						tailMask = removeBranches(tailMask)
						tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
						decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
						points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
						tailLength, distance = tailMeasures(points)  ## Linear length along the line

						# Tail length still considered too long, possibly due too abberation
						# in mask due to thinning, try different thinng (GUOHALL)
						if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:
							# Tail position extraction
							tailMask = extractTailPosition(imgOT, chakra, camera['res']/8, frameNumber, 'GUOHALL', False) 
							tailMask = removeBranches(tailMask)
							tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
							decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
							points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
							tailLength, distance = tailMeasures(points)  ## Linear length along the line

							# Difficulty in tail length persists
							if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:

								if plotting:
									plt.figure(); plt.imshow(imgMNT, origin='upper')
									plt.plot(*points.T, '.m', markersize=8)
									plt.plot(*thirdEye, 'or')
									plt.plot(*sb, 'ob')
									plt.xlim(acceptableArea[1]); plt.ylim(acceptableArea[-1])

								print('Difficulty in tail length persists (#3), Frame {}'.format(frameNumber)); return

					elif camera['res'] == 8:
							# Re-run thinning process using "Zhang Suen" method
							tailMask = extractTailPosition(imgMNT, chakra, camera['res']/8, frameNumber, 'GUOHALL', False) 
							tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
							decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
							points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
							tailLength, distance = tailMeasures(points)  ## Linear length along the line

							# Difficulty in tail length persists
							if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:

								if plotting:
									plt.figure(); plt.imshow(imgMNT, origin='upper')
									plt.plot(*points.T, '.m', markersize=8)
									plt.plot(*thirdEye, 'or')
									plt.plot(*sb, 'ob')
									plt.xlim(acceptableArea[1]); plt.ylim(acceptableArea[-1])

								print('Difficulty in tail length persists (#3), Frame {}'.format(frameNumber)); return
				print('Tail length: {}'.format(tailLength))
				# Get tail angles and associated information
				pointsFitted, tailAngles = computeTailAngles(points, distance)

				# Determine heading
				heading = determineHeading(imgM, thirdEye, sb, chakra, pointsFitted[-1], False)

				# Successfully finished! Now for plotting...
				if plotting:
					plt.figure(); plt.imshow(imgMN, origin='upper')
					plt.xlim(acceptableArea[1]); plt.ylim(acceptableArea[-1])
					ax = plt.gca()
					for p, ta in enumerate(tailAngles):
						ax.arrow(*pointsFitted[p,:], (l/10)*np.cos(ta), (l/10)*np.sin(ta),
								  head_width=hw/3, head_length=hw/3, fc='black', ec='black', linewidth=2) 
					ax.arrow(thirdEye[0], thirdEye[1], l*np.cos(heading), l*np.sin(heading),
							 head_width=hw, head_length=hl, fc='red', ec='black') 
					plt.plot(*pointsFitted.T, '.m', markersize=8)
					plt.plot(*thirdEye, 'or')
					plt.plot(*sb, 'ob')

				print('Correctly exited, Frame {}'.format(frameNumber)); return
			else:
				plt.figure(); plt.imshow(img, origin='upper')
				if camera['res'] == 24:
					plt.xlim([60,180]); plt.ylim([180,60])
				elif camera['res'] == 8:
					plt.xlim([20,60]); plt.ylim([60,20])
				# No pixels found in image
				print('Atypical thresheld image (#0), Frame {}'.format(frameNumber)); return
		else:
			ftf.framesNearBoundary(rawData, camera, radialLimit, True)
			# Larva position exceeds limits
			print('Frame not within boundary (#1), Frame {}'.format(frameNumber)); return
	except:
		print('Fails for some unknown reason')


# Creates necessary variables, calls 'func'
def prep_call(bdir, stackOfInterest, frameNumber, radialLimit):
	# Create 'frameDict' variable is non-existent
	ftf.createFrameDict(bdir)

	# Load 'frameDict.json'
	with open(os.path.join(bdir, 'frameDict.json')) as jsonFile:
		jsonFD = json.load(jsonFile)
	frameDict = json.loads(jsonFD)

	# Load info from frameDict
	files = list(frameDict.keys())  
	frames = list(frameDict.values())

	# Output file name creation ??
	textFileName = re.search('^(.*)(?=_(bg)|(cam)Image)', files[0]).groups()[0] + '_tail_MP.h5'

	# Sort frame number to order files
	bgIdx = [k for k, f in enumerate(files) if 'bgImage' in f]
	camIdx = [k for k, f in enumerate(files) if 'camImage' in f]
	assert len(bgIdx) == len(camIdx), 'The number of ''bg'' and ''cam'' images files are not equal.'

	# Index of associated 'bg' and 'cam' files, and total file number
	frameIdx = np.array((bgIdx, camIdx)).T
	numStacks = frameIdx.shape[0]
	stackBins = np.insert(np.asarray(frames[0:numStacks])[:,1], 0, 0)
	frame2stackBins = np.digitize(np.arange(np.min(frames), np.max(frames)), stackBins) - 1

	# Aggregate '.track' data
	camera = ftf.camParams(bdir)
	rawData = ftf.moveData(bdir)
	# Add column for the "ID" number within the tiff stackes
	rawData.insert(1, 'frame', np.arange(np.min(frames), np.max(frames)) - stackBins[frame2stackBins])

	# Had to split "ftf.formatMoveData" into two parts
	rawData = ftf.formatMoveData1(rawData, camera)

	# Because...code doesn't handle poor tracking that well, and the poorest of tracking occurs near the wall
	# Thus, we remove those prior to formatting
	exceedsRadialLimit = ftf.framesNearBoundary(rawData, camera, radialLimit, False)
	# rawData.iloc[exceedsRadialLimit, np.r_[2,3,4]] = np.nan

	# Second half of "ftf.formatMoveData"
	rawData = ftf.formatMoveData2(rawData, camera)

	# Reflect 'counter' in 'rawData' dataframe appropriately
	rawData['counter'] = np.arange(rawData.shape[0]) 

	## Add "dropped frames" to their appropriate tiff stack
	droppedFrames = np.nonzero(np.isnan(rawData['frame'].values))[0] 
	counterBins = np.nonzero(np.diff(rawData['frame'].values)<0)[0] + 1
	counterBins = np.insert(np.append(counterBins, rawData.shape[0]), 0, 0)
	droppedStacks = np.digitize(droppedFrames, counterBins)-1

	## Include frames exceeding radial boundary to 'skipinfo'
	# Then, determine which frames fall outside radial boundary (x mm from swim chamber wall)
	exceedStacks = np.digitize(exceedsRadialLimit, counterBins)-1
	exceedFrames = rawData['frame'].iloc[exceedsRadialLimit].values

	# Load file corresponding to 'stackOfInterest'
	bg = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,0]]))
	im = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,1]]))
	
	# Frames to loop ("multiprocess") over
	framesInStack = im.shape[0]
	framesWithinBoundary = np.delete(np.arange(framesInStack), exceedFrames[exceedStacks==stackOfInterest].astype(int)) 

	if frameNumber == 0:
		for f in range(np.diff(stackBins)[stackOfInterest]):
			func(f, bg, im, framesWithinBoundary, rawData, radialLimit, camera)
	else:
		func(frameNumber, bg, im, framesWithinBoundary, camera, rawData, radialLimit, True)


if __name__ == "__main__":
	# Behavioral directory of interest
	bdir = '/run/media/jamie/LargePass/092220/1100-31'
	stackOfInterest, frameNumber = (3, 43208)
	radialLimit = 3

	prep_call(bdir, stackOfInterest, frameNumber, radialLimit)