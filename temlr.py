# -*- coding: utf-8 -*-
"""
================================================================
Code to extract and analyze fish behavior video/images

INPUTS:
  '--directory':  path to directory containing (1) .tif stacks for
          a single behavioral session or (2) directories 
          of individual behavioral sessions
  '--radius':    distance in millimeter away from swim chamber
          barrier for frames to be excluded for being
          too close (to avoid reflection or disturbances
          in behavior to do the barrier), values must be
          between (0,10)

OUTPUTS '*_MPB.h5' saved file):
  ==> Load '*_MPB.h5':
    `from aggregateBoutData import loadMPB`
    `flhc, rawdata, boutdata, skipinfo, tailangles, smoothedtailangles,
      tailcoords, smoothedheading, tailtip, exceedtrials = loadMPB(file_path, image_dimension_tuple)`
  ===========================================================================

  MPB file contains (more details below):
  (pandas dataframes)
  'flhc':      general information about larva position
  'rawdata':    modified '.track' including counter, frame, stack, frameIdx
  'boutdata':    bout start/stop counters and other info
  'skipinfo':    the collection of frames that were not processed

  (numpy arrays)
  'tailangles':
          <counter, tail position>
          frame-by-frame raw tail angle, 0 index = end of tail
  'smoothedtailangles':
          <counter, tail position>
          median filtered 'tailangles' over three frames
  'smoothedheading':
          <counter>
          median filtered 'heading' over three frames, raw headings
          can be found in 'flhc' (jamie calcs) or 'rawdata' (martin calcs)
  'tailcoords':
          <counter, tail position, xy coordinate>
          same as 'tailangles' but for coordinates, contains 21 points
          so that there will be 20 tail angles
  'tailtip':
          <counter>
          tail deflection metric, see NOTE in bout data
  'exceedtrials':
          <index>
          frames falling  within '--radius' distance away from wall indices,
          indexed in 'counter'

  **Data object details**

  'flhc' columns:
   counter   - directly associated with time, includes position of dropped frames
   stack     - the .tif stack the image originates (related to 'frame')
   frame     - the frame number in the .tif stack (related to 'stack')
   frameIdx   - similar to 'counter' but only counts captured frames
   length   - length of tail in pixels, helps to diagnose poorly processed images
   heading   - alternative method of determining larvae heading compared to '.track' file
   mideye   - putative xy coordinates of midpoint between eyes
   bladder   - putative xy coordinates of swim bladder
   center   - xy coordinates midway between 'mideye' and 'bladder', sometimes the 
         code would confuse which was which - midpoint assuages confusion
   gCenter   - global xy coordinates of 'center'

  NOTE: While both 'counter' and 'frameIdx' are running indices of total frames, 'frameIdx' does
      not include the frames dropped during recording. 'counter' accounts for these because
      the total session time and frame rate are known. This code accounts for both.

  NOTE: Using 'loadMPB' adds the 'gCenter' field, not inherent in the dataframe object

  NOTE: The 'length' column contains zero and negative integers. These are flags indicating the
      frame did not complete processing for some reason. {0: No pixels found in image,
      -1: Larva position exceeds radial limits, -2: Eyes and/or swim bladder not found,
      -3: Unusual tail length, -4: Mistracked larval position (near outside of captured frame),
      -5: Weird pixelated body (usually happens when larvae is over text in the dish),
      -6: Fails for some unknown reason}

  'rawdata' columns:
   counter   - see 'flhc'
   stack     - see 'flhc'
   frame     - see 'flhc'
   frameIdx   - see 'flhc'
   x       - global x coordinate of image center
   y       - global y coordinate of image center
   heading   - larva heading
   time     - time from beginning of video recording in seconds 
   dist     - displacement from previous frame in millimeters 
   speed     - speed between frames in millimeters/second

  NOTE: This dataframe is the '.track' file with additional frame indices and calculations.

  'boutdata' columns:
   startCounter  - beginning of bout
   peakCounter  - counter index of peak tail deflection ('tailtip')
   endCounter   - end of bout
   auc       - area under tail deflection ('tailtip') curve
   max       - maximum tail deflection ('tailtip')

  NOTE: Bouts are identified from thresholding 'tailtip'. That's simple enough. If you wish
      to know how 'tailtip' is calculated I recommend reviewing the 'anglePrep' function
      below. Briefly, 'tailtip' is the filtered derivative of the angular difference in
      average tail angle between two frames.

  'skipinfo' columns:
   type     - vague description of processing failure, see 'length' in 'flhc'
   stack     - see 'flhc'
   frame     - see 'flhc'
   counter   - see 'flhc'

The code uses python multi-processing and is set to use two less
than the two number of cores on the processor (e.g., if computer
has 8 core cpu, code uses 6).

Currently handles two pixel resolution for images (8px, 24px), but 
it is possible code will function properly with other resolutions.
Wait...the code must be updated in a few places to work with other
resolutions. Currently, the resolution is pulled from '.info'.

================================================================

*Up to date as of 06/02/2022*

"""

# Load libraries
import sys
import argparse
import time
import os
import re
import gc
import psutil
import glob
import json
import h5py
import warnings
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2
import imutils
import networkx as nx

from tifffile import imread
from scipy.interpolate import UnivariateSpline
from sklearn.neighbors import NearestNeighbors
from tqdm import tqdm
from scipy.signal import convolve2d as conv2 
from scipy.ndimage import median_filter

import functools
import multiprocessing
import formatTrackFile as ftf


class _check_values(argparse.Action):
  def __call__(self, parser, namespace, values, option_string=None):
    if self.dest == 'directory':
      if not os.path.exists(values):
        raise argparse.ArgumentError(self, "Directory non-existent")
      setattr(namespace, self.dest, values)
    elif self.dest == 'radius':
      if not 0 <= values <= 10:
        raise argparse.ArgumentError(self, "Radius must be between 0 and 10 mm")
      setattr(namespace, self.dest, values)


# Handle input arguments
def getOptions(args=sys.argv[1:]):
  # Handle inputs first -- create parser and add the arguments
  parser = argparse.ArgumentParser(description="The code processes .tif stacks of larval zebrafish " + 
          "and saves into '*_MPB.h5' files containing heading, body positions, tail coordinates, " + 
          "and tail angles for each frame in session. The '*_MPB.h5' files are located in the " +
          "directory where its .tif stacks originated. The .tif stacks are from one behavioral " +
          "session. The input directory can be one session or a directory containing many sessions. " +
          "For details of the inputs and outputs, read comments at top of python file.")
  parser.add_argument('--directory', type=str, action=_check_values,
            help="Directory with .tif stacks for extraction, it can be an single session directory or "+
            "directory containing a collection of sessions each in their own subdirectory.")
  parser.add_argument('--radius', type=int, action=_check_values,
            help="Distance (mm) from swim chamber boundary that frames will be excluded " +
            "to avoid both behavior affected by the chamber wall and image processing " +
            "corruption to do reflection.")
  options = parser.parse_args(args)
  return options


# To set the priority of executing this python function
def limitCpu():
  ## is called at every process start
  p = psutil.Process(os.getpid())
  # set to lowest priority, this is windows only, on Unix use ps.nice(19)
  p.nice(-19)


# Gamma correction
def adjustGamma(image, gamma=1.0):
  # build a lookup table mapping the pixel values [0, 255] to
  # their adjusted gamma values
  invGamma = 1.0 / gamma
  table = np.array([((i / 255.0) ** invGamma) * 255
    for i in np.arange(0, 256)]).astype("uint8")
  # apply gamma correction using the lookup table
  return cv2.LUT(image, table)


# Calulate midpoint
def midpoint(points):
  x1, y1, x2, y2 = points
  return (((x2+x1)/2, (y2+y1)/2))


# Determine nearest point in an array
def findNearest(array, value):
  array = np.asarray(array)
  idx = (np.abs(array - value)).argmin()
  return array[idx]


# Find body positions from image by thresholding the "numPixel" number of highest pixel intensities
def bodyPositions(pxMult, histMN, imgSize, imgX):
  tmp = None
  for numPixel in np.array([10, 9 ,8])*pxMult:
    # Determine threshold by the pixel intensity of the number of pixels ('numPixel')
    # below greatest intensity
    bodyThresh = histMN[1][np.nonzero(np.cumsum(histMN[0]) > (imgSize - numPixel))[0][0]]  

    # Threshold normalized image based off this pixel intensity
    imgHT = cv2.threshold(imgX.copy(), bodyThresh, 255, cv2.THRESH_BINARY)[1]

    # Extract contours (putatively, of eyes and swim bladder)
    contoursNorm = cv2.findContours(imgHT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    cntsNorm = imutils.grab_contours(contoursNorm)
    cnorm = sorted(cntsNorm, key=cv2.contourArea, reverse=True)

    if len(cnorm) == 3:  ## hope for 3 contours
      break
    elif len(cnorm) == 2:  ## will accept two (temporarily, called twice in 'imgProcess' definition)
      tmp = cnorm

  if tmp != None and len(cnorm) != 3:
    return tmp
  else:
    return cnorm


# Determine the heading angle by thresholding (to find location of eyes and
# swim bladder) and then find contours, the centroids of those contours, and
# lastly angles between vectors created using the centroids
def determineHeading(imgM, thirdEye, sb, chakra, lastTailCoord, plotting=False):
  # Ol' Jamie is tired of dealing with radians. Ol' Jamie needs three angles...

  # Calculate the angle between the swim bladder ('sb') and third-eye ('asb')
  vbsb = np.subtract(thirdEye, sb)
  asb = np.arctan2(vbsb[1], vbsb[0])  ## angle 1
  if asb < 0: asb += 2*np.pi

  # Find the 180 degree rotation of 'asb', it is possible the swimbladder and 
  # third-eye were confused during image processing as to which was which
  if asb >= np.pi: 
    asbPrime = asb - np.pi  ## angle 2 = angle 1 rotated by 180 deg
  elif asb < np.pi:
    asbPrime = asb + np.pi  ## angle 2 = angle 1 rotated by 180 deg

  # Calculate the angle between the last tail coordinate and chakra
  vbltc = np.subtract(chakra, lastTailCoord)
  altc = np.arctan2(vbltc[1], vbltc[0])
  if altc < 0: altc += 2*np.pi  ## angle 3

  # Now find the differences between angles 1&3 and 2&3
  xa1, ya1 = (np.cos(asb), np.sin(asb))
  xa2, ya2 = (np.cos(asbPrime), np.sin(asbPrime))
  xa3, ya3 = (np.cos(altc), np.sin(altc))
  # I, Jamie, use this approach tan^-1(cross_product, dot_product) to calculate angles to 
  # avoid difficulties with radians being circular
  ab31 = np.abs(np.arctan2(np.cross([xa3, ya3], [xa1, ya1]), np.dot([xa3, ya3], [xa1, ya1])))
  ab32 = np.abs(np.arctan2(np.cross([xa3, ya3], [xa2, ya2]), np.dot([xa3, ya3], [xa2, ya2])))

  # Whichever "angle between" (e.g., 'ab') is smaller is the correct one!
  if ab31 <= ab32:
    heading = asb
  elif ab31 > ab32:
    heading = asbPrime

  # Plotting code
  if plotting:
    plt.imshow(imgM, origin='upper')
    plt.plot(*thirdEye, '.r')
    plt.plot(*sb, '.k')
    ax = plt.gca()
    ax.arrow(thirdEye[0], thirdEye[1], 15*np.cos(heading), 15*np.sin(heading),
         head_width=3, head_length=1.5, fc='red', ec='black') 
    ax.arrow(sb[0], sb[1], vbsb[0], vbsb[1],
         head_width=3, head_length=1.5, fc='black', ec='black') 
    plt.show()

  return heading


# Apply Huang, et al. (2013) method to smooth tail coordinates
def decimalPrecision(img, tailPositions, i=1, j=1):

  decimalTailPositions = np.zeros(np.shape(tailPositions))

  for n,(x,y) in enumerate(zip(tailPositions[0], tailPositions[1])):
    # Create i-by-j coordinates around one tailPosition coordinate (x,y)
    iRange, jRange = np.arange(-1*i, i+1), np.arange(-1*j, j+1)
    xi = np.tile(iRange, 2*i+1) + x
    yj = np.repeat(jRange, 2*j+1) + y

    # Find sum of i-by-j pixels around one tailPosition coordinate (x,y)
    imgIdx = np.array([xi, yj])
    xySum = np.take(img, np.ravel_multi_index(imgIdx, img.shape)).astype(np.float).sum()

    # Find x decimal precision
    xSum = 0.0
    for ii in iRange:
      xSumIdx = np.array([np.repeat(ii+x, 2*j+1), jRange+y])
      xSum += np.float(ii)*np.take(img, np.ravel_multi_index(xSumIdx, img.shape)).astype(np.float).sum()
    xPrime = np.float(x) + xSum/xySum

    # Find y decimal precision
    ySum = 0.0
    for jj in jRange:
      ySumIdx = np.array([iRange+x, np.repeat(jj+y, 2*i+1)])
      ySum += np.float(jj)*np.take(img, np.ravel_multi_index(ySumIdx, img.shape)).astype(np.float).sum()
    yPrime = np.float(y) + ySum/xySum

    # Insert into output vector
    decimalTailPositions[:,n] = [xPrime, yPrime]

  return decimalTailPositions


# Compute the tail angles from the sorted tail positions ("sortedPoints")
def computeTailAngles(sortedPoints, distance, fittedSize=21):

  # Build a list of the spline function, one for each dimension:
  splines = [UnivariateSpline(distance, coords, k=3, s=.5) for coords in sortedPoints.T]

  # Computed the spline for the asked distances:
  alpha = np.linspace(0, 1, fittedSize)
  # alpha = np.delete(alpha, 1)  ## remove the second element, first and second points don't deviate much
  sortedPointsFitted = np.vstack([spl(alpha) for spl in splines]).T

  # Compute tail angles
  tailAngles = np.arctan2(*np.diff(np.flip(sortedPointsFitted,1).T))
  tailAngles[tailAngles < 0] += 2*np.pi

  return (sortedPointsFitted, tailAngles)


# Tail-point sorting function
def pointSorting(coords, bodyPart):

  if coords.shape[-1] > 3:
    # Append 'bodyPart' to closest end of 'coords'
    distFromBP = np.linalg.norm(bodyPart-np.fliplr(coords.T), axis=-1) 

    # Separate into 'x' and 'y' vectors
    if distFromBP[0] < distFromBP[-1]:
      y, x = coords
    else:
      y, x = np.fliplr(coords) 

    # Add 'bodyPart' as first point, to sort other based off known head position
    x = np.insert(x, 0, bodyPart[0])
    y = np.insert(y, 0, bodyPart[1])
    points = np.c_[x, y]

    # Use some graph theory (credit StackOverflow) to sort coordinates
    clf = NearestNeighbors(n_neighbors=3).fit(points)
    G = clf.kneighbors_graph(mode='distance')
    # T = nx.from_scipy_sparse_matrix(G)
    # order = list(nx.dfs_preorder_nodes(T, source=0))[1:]

    # Loop to order points by shortest distance
    cols = np.arange(1, G.toarray().shape[1])
    order = np.full(cols.shape, np.nan)
    i,rc = (0,0)
    while np.isnan(order).any():
      row = G.toarray()[i]  ## Distances from one position
      d = np.ma.masked_where(row==0, row)[cols]  ## Masked to ignore zeros
      order[rc] = cols[np.argmin(d)]  ## Minimum distance
      cols = np.setdiff1d(cols, order[rc])  ## Remove position from search
      i = int(order[rc])  ## Inspect the next position
      rc += 1  ## Update "rowcounter"

    # Sort points based off 'order'
    xx = x[order.astype(int)]
    yy = y[order.astype(int)]
    pointSorted = np.flip(np.c_[xx,yy],0)  # furthest length is "0" distance

    return pointSorted
  else:  ## Not enough coords to sort
    return np.array([[1,2], [3,4]])


# Determine tail positions by skeletonizing the outline of the larva
def extractTailPosition(threshImg, bodyPart, resolutionAdjustment, frameNumber, thinningMethod, plotting=False):

  # Create mask to separate tail into two components
  headMask = np.zeros(threshImg.shape, dtype=np.uint8)
  cv2.circle(headMask, tuple(np.array(bodyPart, dtype=int)), int(3*resolutionAdjustment), 255, -1)

  # Thinning operation
  if thinningMethod == 'GUOHALL':
    wholeSkeleton = cv2.ximgproc.thinning(threshImg.copy(), thinningType=cv2.ximgproc.THINNING_GUOHALL)
  elif thinningMethod == 'ZHANGSUEN':
    wholeSkeleton = cv2.ximgproc.thinning(threshImg.copy(), thinningType=cv2.ximgproc.THINNING_ZHANGSUEN)

  if np.sum(wholeSkeleton != 0) > 10:
    # Apply mask 'headMask' to 'wholeSkeleton'
    twoSkeletonSegs = cv2.bitwise_and(wholeSkeleton.copy(), wholeSkeleton.copy(), mask=cv2.bitwise_not(headMask))  
    # Find largest segment of 'twoSkeletonSegs'
    throwAwayMask = np.zeros(threshImg.shape, dtype=np.uint8)
    throwAwayContours = cv2.findContours(twoSkeletonSegs.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  
    throwAwayCnts = imutils.grab_contours(throwAwayContours)
    throwAwayC = sorted(throwAwayCnts, key=np.shape, reverse=True)[0]
    cv2.drawContours(throwAwayMask, [throwAwayC], -1, 255, -1) 
    imgThin = cv2.bitwise_and(twoSkeletonSegs.copy(), throwAwayMask)

    # Plotting
    if plotting:
      # Create bladder mask
      bodyMaskColor = cv2.cvtColor(headMask.copy(), cv2.COLOR_GRAY2RGB)
      bodyMaskColor[:,:,2] = 0

      # Create thinning mask
      imgThinColor = cv2.cvtColor(wholeSkeleton.copy(), cv2.COLOR_GRAY2RGB) 
      imgThinColor[:,:,1] = 0;
      imgThinMask = cv2.bitwise_not(wholeSkeleton.copy())

      # Plotting scratch code (at the moment)
      imgColor = cv2.cvtColor(threshImg.copy(), cv2.COLOR_GRAY2RGB)
      img1BG = cv2.bitwise_and(imgColor.copy(), imgColor.copy(), mask = threshImg)

      dst = imgColor.copy()
      dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = cv2.bitwise_not(headMask.copy()))
      dst = cv2.add(dst.copy(), bodyMaskColor)
      dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = imgThinMask)
      dst = cv2.add(dst, imgThinColor)

      # Add some text
      text = 'Frame ' + str(frameNumber)
      cv2.putText(dst, text, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

      cv2.imshow('Tail tracking', cv2.resize(dst, (480,480))); cv2.waitKeyEx(2500); cv2.destroyAllWindows()

    return imgThin
  else:
    return np.zeros(threshImg.shape, dtype=np.uint8)


# Determine tail length and positions along it
def tailMeasures(pointSorted):
  cumDistance = np.cumsum(np.sqrt(np.sum(np.diff(pointSorted, axis=0)**2, axis=1)))
  tailLength = cumDistance[-1]
  # print('Total distance: {0:.3f}'.format(tailLength))
  distance = np.insert(cumDistance, 0, 0)/cumDistance[-1]

  return (tailLength, distance)


# Remove branching from tail mask
def removeBranches(tailMask):
  # Set intensity range = (0,1)
  tmN = np.zeros(tailMask.shape)   
  tmN = cv2.normalize(tailMask, tmN, 0, 1, cv2.NORM_MINMAX)

  # Look for position connected by more than two
  dot = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
  dot[1,1] = 0
  filtered = conv2(tmN.astype(np.float), dot, mode='same')
  filtered[filtered <= 2] = 0

  # Separated branches ('D' = "disconnected")
  tmND = cv2.bitwise_and(tmN, cv2.bitwise_not(cv2.bitwise_and(tmN, filtered.astype(np.uint8)))) 
  __, labels = cv2.connectedComponents(tmND)
  labelUnique, labelCounts = np.unique(labels.flatten(), return_counts=True)
  longestBranchLabel = labelUnique[np.argsort(labelCounts)[-2]]
  labelMask = labels.copy()
  labelMask[labelMask != longestBranchLabel] = 0

  return cv2.bitwise_and(tailMask, tailMask, mask=labelMask.astype(np.uint8))


# Find pixel in thresheld image that are connected only by one corner
def removeSingleCornerPixels(threshImg):
  # Create hit-and-miss kernels
  kul = np.array(([1, -1,-1], 
          [-1, 1, 0], 
          [-1, 0, 0]), dtype="int") 
  kll = np.array(([-1, 0, 0], 
          [-1, 1, 0], 
          [1, -1, -1]), dtype="int") 
  kur = np.array(([-1, -1, 1], 
          [0, 1, -1], 
          [0, 0, -1]), dtype="int") 
  klr = np.array(([0, 0, -1], 
          [0, 1, -1], 
          [-1, -1, 1]), dtype="int")

  # Apply kernels to incoming thresheld image
  imgul = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kul) 
  imgll = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kll) 
  imgur = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kur) 
  imglr = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, klr) 

  # Union of the kernel images
  imgKU = cv2.bitwise_or(imgul, cv2.bitwise_or(imgll, cv2.bitwise_or(imgur, imglr)))

  # Subtracted the kernel images from thresheld
  threshSub = cv2.bitwise_and(threshImg.copy(), cv2.bitwise_not(imgKU))

  # Remove spurious pixels and return
  return removeSpuriousPixels(threshSub.copy())


# Remove spurious pixels from thresheld image
def removeSpuriousPixels(threshImg):
  # Create hit-and-miss kernels
  kspur = np.array(([-1, -1, -1], 
            [-1, 1, -1], 
              [-1, -1, -1]), dtype="int")
  # Remove spurious pixels and return
  imgSpur = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kspur) 
  return cv2.bitwise_and(threshImg.copy(), cv2.bitwise_not(imgSpur))


# Applies median filter, averaging, and smoothing in calculation of tail tip
def anglePrep(ta, hd, frameRate, tipIdx, filtWindow=5):

  # Apply median filter to tail angles
  nanTA = np.any(np.isnan(ta), axis=1)
  if nanTA.any():
      ta[nanTA] = -np.inf
  filtA = median_filter(ta, size=(3,1), mode='nearest')
  infTA = np.any(np.isinf(filtA), axis=1)
  filtA[infTA] = np.nan

  # Same approach for heading
  nanHD = np.isnan(hd)
  hd[nanHD] = -np.inf
  filtH = median_filter(hd[:,np.newaxis], size=(3,1), mode='nearest')
  infHD = np.isinf(filtH)
  filtH[infHD] = np.nan

  # Find tail angle relative to heading
  angles = filtH - filtA

  # Since this is in radians, to average correctly must separate into x,y coordinates
  yavg = np.mean(np.sin(angles[:,tipIdx[0]:tipIdx[1]]), axis=1)
  xavg = np.mean(np.cos(angles[:,tipIdx[0]:tipIdx[1]]), axis=1)

  # Find change in angle using dot-product rule
  u = np.hstack((xavg[:-1, np.newaxis], yavg[:-1, np.newaxis]))
  v = np.hstack((xavg[1:, np.newaxis], yavg[1:, np.newaxis]))
  uv = np.sum(u*v, axis=1)
  # Calculate magnitudes based on arctan(sin,cos)
  diffAngles = np.arctan2(np.cross(u,v), uv)
  diffAngles[np.isnan(diffAngles)] = 0

  # Calculate derivative of angular difference, and filter
  dervDiffAngles = np.abs(diffAngles / frameRate**-1)
  smoothedMeanAngles = filtfilt(dervDiffAngles, filtWindow)
  smoothedMeanAngles = np.insert(smoothedMeanAngles, 0, 0)

  return np.insert(diffAngles, 0, 0), smoothedMeanAngles, filtA, filtH.flatten()


# Bout detection modified from MH script
def detectBouts(swimMetric, minFramesPerBout, frameRate, loThresh=2, hiThresh=None, zeroLimit=2):
  """
  Detect bouts based on emprically determined characterstics within a trace
  of a swim metric maintaining a certain value.
  """
  #the threshold is determined mostly empirically and probably needs
  #to be changed if our framerate deviates much from 250 - this depends on
  #how aggressively the position traces are smoothed in relation to the
  #framerate

  #bouts should have a clear peak, hence the peak metric should not be
  #maintained for > ~20ms (0.02s)
  maxFramesAtPeak = 0.02/frameRate**-1

  #threshold swim metric
  smetric = swimMetric.copy()
  smetric[np.isnan(smetric)] = 0
  if hiThresh is not None:
    smetric[smetric<loThresh] = 0
    smetric[smetric>hiThresh] = 0
  else:
    smetric[smetric<loThresh] = 0

  #master indexer
  i = 0  ## python like any real language uses 0-based indexing!
  bouts = np.zeros((1,5))  ## '5' because currently tracking five features

  l = np.size(smetric)
  zeroCounter = 0

  while (i < l):
    if smetric[i]==0:
      i += 1
    else:
      # Reset zero counter
      zeroCounter = 0
      #we found a potential "start of bout" - loop till end collecting
      #all speeds
      bStart = i
      b = smetric[i]
      i += 1
      # while(i<l and smetric[i]>0):
      while (i<l and zeroCounter <= zeroLimit):
        if smetric[i] == 0:
          zeroCounter += 1
        else:
          zeroCounter = 0

        b = np.append(b,smetric[i])
        i += 1
      peak = b[b==b.max()]
      #since we loop until smetric==0 the last part 
      #of the bout is in the frame before this frame
      bEnd = i-(zeroLimit+1)

      if (np.size(peak) <= maxFramesAtPeak and np.size(b) >= minFramesPerBout):
        #we have a valid, singular bout
        peakframe = np.nonzero(b==np.max(b))
        peakframe = peakframe[0][0]
        #put peakframe into context
        peakframe = bStart + peakframe
        #a bout should not start on the peakframe
        if(bStart==peakframe):
            bStart = bStart-1
        # bt = np.r_[bStart,peakframe,bEnd,np.sum(b,dtype=float)/frameRate,np.max(b)]
        bt = np.r_[bStart, peakframe, bEnd, np.trapz(b, dx=1/frameRate), np.max(b)]
        bouts = np.vstack((bouts,bt))

  if(np.size(bouts)>1):
    bouts = bouts[1::,:]

  return bouts


# Boxcar filter of size 'window_len', forward and backwards (zero-phase)
def filtfilt(x, window_len):
  """
  Performs zero-phase digital filtering with a boxcar filter. Data is first
  passed through the filter in the forward and then backward direction. This
  approach preserves peak location.
  """
  if x.ndim != 1:
    raise ValueError("filtfilt only accepts 1 dimension arrays.")

  if x.size < window_len:
    raise ValueError("Input vector needs to be bigger than window size.")


  if window_len<3:
    return x

  #pad signals with reflected versions on both ends
  s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]

  #create our smoothing window
  w=np.ones(window_len,'d')

  #convolve forwards
  y=np.convolve(w/w.sum(),s,mode='valid')

  #convolve backwards
  yret=np.convolve(w/w.sum(),y[-1::-1],mode='valid')

  return yret[-1::-1]


# Loop over all frames in give .tif stack
def imgProcess(frameNumber, bg, im, framesWithinBoundary, camera):

  # Properties based off camera resolution
  if camera['res'] == 8:
    pxArea = (1,1)
    pxMult = 1
    pxThreshLo, pxThreshHi = (50, 170)
    acceptableArea = np.array([[[20, 20], [20, 60], [60, 60], [60, 20]]], dtype=np.int32) 
  elif camera['res'] == 24:
    pxArea = (2,2)
    pxMult = 5
    pxThreshLo, pxThreshHi = (90, 270)
    acceptableArea = np.array([[[60, 60], [60, 180], [180, 180], [180, 60]]], dtype=np.int32) 

  try:
    if frameNumber in framesWithinBoundary:
      # Background subtract to form image
      img = np.abs(im[frameNumber,:,:].astype(np.float32) - 
             bg[frameNumber,:,:].astype(np.float32)).astype(np.uint8)

      # Threshold image
      imgOT = cv2.threshold(img.copy(), 5, 255, 0)[1] # Threshold image
      imgOT = removeSpuriousPixels(imgOT)

      # There must be some nonzero pixels to continue / extract larval body information
      if np.sum(imgOT != 0) > pxThreshLo and np.sum(imgOT != 0) < pxThreshHi:
        # Find largest contours of thresheld image
        contours = cv2.findContours(imgOT.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        cnts = imutils.grab_contours(contours)
        c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

        # Exclude all pixels not contained within the largest contour
        mask = np.zeros(imgOT.shape, dtype=np.uint8) 
        cv2.drawContours(mask, [c], -1, 255, -1)
        imgM = cv2.bitwise_and(img, img, mask=mask)

        # Normalize histogram between (0,255)
        imgMN = np.zeros(img.shape, dtype=np.uint8)
        cv2.normalize(imgM, imgMN, 255.0, 0.0, cv2.NORM_INF)

        # Gamma correct image
        imgG = adjustGamma(imgMN.copy(), gamma=0.8)

        # Adaptive thresholding
        histMN = np.histogram(imgMN.flatten(), bins=50, range=(0,255))
        # Identify 3 contours (hopefully)
        cnorm = bodyPositions(pxMult, histMN, img.size, imgMN)
        # Doesn't always work so try gamma corrected image
        if not len(cnorm) == 3:
          _cnorm = bodyPositions(pxMult, histMN, img.size, imgG)
          if len(_cnorm) == 2 or len(_cnorm) == 3:
            cnorm = _cnorm

        # Find center of contour by taking average of largest contours (within acceptable area)
        centroids = []
        for cn in cnorm: 
          cnAvg = np.average(cn, axis=0)[0]

          # Check that all centroids are within main portion of image
          if cv2.pointPolygonTest(acceptableArea, tuple(cnAvg), False) != -1.0:
            centroids.append(tuple(np.round(cnAvg, decimals=3)))
          else:
            # Mistracked larval position
            return ((frameNumber, -4, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
                 np.full((20,), np.nan) , np.full((21,2), np.nan))

        if len(centroids) == 3:  ## Identify body parts (i.e., "eyes" and "swimbladder")
          # Identify the shortest pair
          pairs = centroids.copy()
          pairs.append(pairs[0])
          shortestPair = np.argmin(np.sqrt(np.sum(np.diff(pairs, axis=0)**2, axis=1)))

          # Point eyes and swimbladder ('sb')
          eyes = [pairs[shortestPair], pairs[shortestPair+1]]
          sb = list(set(centroids) - set(eyes))[0]
          thirdEye = midpoint(eyes[0] + eyes[1])
        elif len(centroids) == 2:  ## Identify body parts (i.e., "third eye" and "swimbladder")
          # Threshold gamma corrected image using Otsu method
          bodyMask = cv2.threshold(imgG, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
          M = cv2.moments(bodyMask)
          bodyCenter = (M["m10"]/M["m00"], M["m01"]/M["m00"])

          dists = np.linalg.norm(np.asarray(bodyCenter) - np.asarray(centroids), axis=1)
          thirdEye = centroids[np.argmax(dists)]
          sb = centroids[np.argmin(dists)]
        else:
          # Eyes and/or swim bladder not found :/
          return ((frameNumber, -2, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
               np.full((20,), np.nan) , np.full((21,2), np.nan))

        # Midpoint between eyes and swimbladder
        chakra = midpoint(thirdEye + sb)

        # Threshold once more (for extracting tail skeleton)
        imgMNT = cv2.threshold(imgMN, 5, 255, cv2.THRESH_BINARY)[1]
        imgMNT = removeSingleCornerPixels(imgMNT)
        if np.sum(imgMNT != 0) < pxThreshLo or np.sum(imgMNT != 0) > pxThreshHi:
          # Weird pixelated body (usually happens when larvae is over text in the dish)
          return ((frameNumber, -5, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
               np.full((20,), np.nan) , np.full((21,2), np.nan))

        # Tail position extraction
        tailMask = extractTailPosition(imgMNT, chakra, camera['res']/8, frameNumber, 'ZHANGSUEN', False) 
        tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
        decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
        points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
        tailLength, distance = tailMeasures(points)  ## Linear length along the line

        # Tail length considered too long, possibly branches
        # Scrub branches from tailMask (after ZHANGSUEN thinning)
        if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:
          if camera['res'] == 24:
            # Re-attempt after a branch removal process
            tailMask = removeBranches(tailMask)
            tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
            decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
            points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
            tailLength, distance = tailMeasures(points)  ## Linear length along the line

            # Tail length still considered too long, possibly due too abberation
            # in mask due to thinning, try different thinng (GUOHALL)
            if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:
              # Tail position extraction
              tailMask = extractTailPosition(imgOT, chakra, camera['res']/8, frameNumber, 'GUOHALL', False) 
              tailMask = removeBranches(tailMask)
              tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
              decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
              points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
              tailLength, distance = tailMeasures(points)  ## Linear length along the line

              # Difficulty in tail length persists
              if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:
                return ((frameNumber, -3, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
                     np.full((20,), np.nan) , np.full((21,2), np.nan))

          elif camera['res'] == 8:
              # Re-run thinning process using "Zhang Suen" method
              tailMask = extractTailPosition(imgMNT, chakra, camera['res']/8, frameNumber, 'GUOHALL', False) 
              tailPositions = np.nonzero(tailMask)  ## Convert to coordinates
              decimalTailPositions = decimalPrecision(img, tailPositions, pxArea[0], pxArea[1])
              points = pointSorting(decimalTailPositions, chakra)  ## Tail position sorting
              tailLength, distance = tailMeasures(points)  ## Linear length along the line

              # Difficulty in tail length persists
              if tailLength > camera['res']*3.5 or tailLength < camera['res']*1.5:
                return ((frameNumber, -3, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
                     np.full((20,), np.nan) , np.full((21,2), np.nan))

        # Get tail angles and associated information
        pointsFitted, tailAngles = computeTailAngles(points, distance)

        # Determine heading
        heading = determineHeading(imgM, thirdEye, sb, chakra, pointsFitted[-1], False)

        # Insert information into output variables, and increase counter
        return ((frameNumber, tailLength, heading, chakra[0], chakra[1], thirdEye[0], thirdEye[1],
             sb[0], sb[1]), tailAngles, pointsFitted)
      else:
        # No pixels found in image
        return ((frameNumber, 0, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
             np.full((20,), np.nan) , np.full((21,2), np.nan))
    else:
      # Larva position exceeds limits
      return ((frameNumber, -1, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
           np.full((20,), np.nan) , np.full((21,2), np.nan))
  except:
      # Fails for some unknown reason
      return ((frameNumber, -6, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan),
           np.full((20,), np.nan) , np.full((21,2), np.nan))


# Set up the function
def main(bdir, radialLimit):
  # Load 'frameDict.json'
  with open(os.path.join(bdir, 'frameDict.json')) as jsonFile:
    jsonFD = json.load(jsonFile)
  frameDict = json.loads(jsonFD)

  # Separete informatino of 'frameDict'
  files = list(frameDict.keys())  
  frames = list(frameDict.values())

  # Output file name creation ??
  textFileName = re.search('^(.*)(?=_(bg)|(cam)Image)', files[0]).groups()[0] + '_MPB.h5'

  # Sort frame number to order files
  bgIdx = [k for k, f in enumerate(files) if 'bgImage' in f]
  camIdx = [k for k, f in enumerate(files) if 'camImage' in f]
  assert len(bgIdx) == len(camIdx), 'The number of ''bg'' and ''cam'' images files are not equal.'

  # Index of associated 'bg' and 'cam' files, total file number (stacks), frames per stack
  frameIdx = np.array((bgIdx, camIdx)).T
  numStacks = frameIdx.shape[0]
  stackBins = np.insert(np.asarray(frames[0:numStacks])[:,1], 0, 0)
  frame2stackBins = np.digitize(np.arange(np.min(frames), np.max(frames)), stackBins) - 1

  ### Create and format 'rawData' variable
  # Aggregate '.track' data
  camera = ftf.camParams(bdir)
  rawData = ftf.moveData(bdir)
  rawData.insert(1, 'frameIdx', np.arange(rawData.shape[0]))
  # Add column for the "frame" number within the tiff stacks
  rawData.insert(1, 'frame', np.arange(np.min(frames), np.max(frames)) - stackBins[frame2stackBins])

  # Had to split "ftf.formatMoveData" into two parts...because code doesn't handle poor tracking that well, 
  # and the poorest of tracking occurs near the wall. Thus, we remove those prior to formatting.
  rawData = ftf.formatMoveData1(rawData, camera)
  exceedsRadialLimit = ftf.framesNearBoundary(rawData, camera, radialLimit, True)   
  # Show swim arena for debugging later (if need be)
  plt.savefig(os.path.join(bdir, 'swim-arena.png'), bbox_inches='tight', format='png')
  plt.close()

  rawData.iloc[exceedsRadialLimit, np.r_[3,4,5]] = np.nan  ## removing difficult indices
  # Second half of "ftf.formatMoveData"
  rawData = ftf.formatMoveData2(rawData, camera)
  # Update 'counter' in 'rawData' dataframe appropriately
  rawData['counter'] = np.arange(rawData.shape[0]) 

  ### Create and format 'FLHC' variable
  # F - frame number, L - length, H - heading angle, C - centroid
  tmpMat = np.zeros((rawData.shape[0], 2)); tmpMat.fill(np.nan)
  tmpMat[:,0] = rawData['counter'].values.astype(np.int)
  FLHC = pd.DataFrame(tmpMat, columns=['counter', 'stack'])

  # One more matrix to log skipped information
  # DF columnns = ['type', 'stack', 'frame', 'counter']
  skipinfo = pd.DataFrame(columns=['type', 'stack', 'frame', 'counter'])

  # Add "dropped frames" to their appropriate tiff stack
  droppedFrames = np.nonzero(np.isnan(rawData['frame'].values))[0] 

  counterBins = np.nonzero(np.diff(rawData['frame'].values)<0)[0] + 1
  counterBins = np.insert(np.append(counterBins, rawData.shape[0]), 0, 0)

  droppedStacks = np.digitize(droppedFrames, counterBins)-1

  # Insert stack numbers into 'FLHC'
  FLHC['stack'] = np.digitize(FLHC['counter'].values, counterBins)-1 
  # Add column for the tiff stacks
  rawData.insert(1, 'stack', FLHC['stack'].values)

  # Insert "dropped" information to 'skipinfo' (label = 1)
  droppedDat = list(zip(np.repeat('no frame', droppedFrames.shape), droppedStacks, 
          np.full(droppedFrames.shape, np.nan), droppedFrames))  
  skipinfo = skipinfo.append(pd.DataFrame(droppedDat, columns=skipinfo.columns), ignore_index=True) 

  ## Include frames exceeding radial boundary to 'skipinfo'
  # Then, determine which frames fall outside radial boundary (x mm from swim chamber wall)
  exceedStacks = np.digitize(exceedsRadialLimit, counterBins)-1
  exceedFrames = rawData['frame'].iloc[exceedsRadialLimit].values

   # Insert "exceeding" information to 'skipinfo'
  exceedDat = list(zip(np.repeat('exceeds limit', exceedFrames.shape), exceedStacks, 
             exceedFrames, exceedsRadialLimit)) 
  skipinfo = skipinfo.append(pd.DataFrame(exceedDat, columns=skipinfo.columns), ignore_index=True) 

  # Create temporary lists for concatenating multiprocess output
  fi, ta, tc = [], [], []
  numProcessors = multiprocessing.cpu_count()-2

  # Loop over all files behavioral session
  for stackOfInterest in tqdm(range(numStacks), desc='Processing .tif stacks', # range(numStacks)
                 ascii=False, ncols=75, position=0):

    # Load file corresponding to 'stackOfInterest'
    bg = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,0]]))
    im = imread(os.path.join(bdir, files[frameIdx[stackOfInterest,1]]))

    # Determine total frames in file
    assert im.shape[0] == bg.shape[0], 'Different number of frames between ''bg'' and ''cam'' files.'

    # Frames to loop ("multiprocess") over
    framesInStack = im.shape[0]
    framesWithinBoundary = np.delete(np.arange(framesInStack),
                                     exceedFrames[exceedStacks==stackOfInterest].astype(int))

    ##### Multiprocess!! #####
    pool = multiprocessing.Pool(processes=numProcessors)  ## , initializer=limitCpu)
    out = pool.map(functools.partial(imgProcess, bg=bg, im=im, framesWithinBoundary=framesWithinBoundary, camera=camera),
                     range(framesInStack), chunksize=int(framesInStack/numProcessors))
    pool.close()
    pool.join()
    #####----------------#####

    # Convert output to more easily manageable lists
    fiList, taList, tcList = map(list, zip(*out))
    fi.extend(fiList)
    ta.extend(taList)
    tc.extend(tcList)

    # Memory clean up
    del bg, im, framesInStack, framesWithinBoundary
    del pool, out, fiList, taList, tcList
    gc.collect()

  # Insert dropped frames into 'xxList'
  for drop in droppedFrames:
    fi.insert(drop, (np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan))
    ta.insert(drop, np.full((20,), np.nan))
    tc.insert(drop, np.full((21,2), np.nan))

  # Convert to numpy arrays
  frameinfo = np.asarray(fi)
  tailangles = np.asarray(ta)
  tailcoords = np.asarray(tc)

  del fi, ta, tc
  gc.collect()

  # Insert info into 'FLHC'
  FLHC['frame'] = frameinfo[:,0]
  FLHC['length'] = frameinfo[:,1]
  FLHC['heading'] = frameinfo[:,2]
  FLHC['mideye'] = frameinfo[:,5:7].tolist()
  FLHC['bladder'] = frameinfo[:,7:].tolist()
  FLHC['center'] = frameinfo[:,3:5].tolist()

  # For consistency add 'frameIdx' to FLHC
  FLHC.insert(3, 'frameIdx', rawData['frameIdx'].values)

  # Add skipped frame information to 'skipinfo'
  # DF columnns = ['type', 'stack', 'frame', 'counter']
  labels = ['no pixels', 'no body', 'weird tail', 'mistracked', 'weird body', 'unknown']
  for label, labelNum in zip(labels, [0, -2, -3, -4, -5, -6]):
    numSkipped = np.nonzero(frameinfo[:,1] == labelNum)[0].shape[0]
    addskipNP = list(zip(np.repeat(label, numSkipped), 
             np.digitize(np.nonzero(frameinfo[:,1] == labelNum)[0], counterBins)-1,
             frameinfo[np.nonzero(frameinfo[:,1] == labelNum)[0],0].astype(np.int),
             np.nonzero(frameinfo[:,1] == labelNum)[0]))
    skipinfo = skipinfo.append(pd.DataFrame(addskipNP, columns=skipinfo.columns), ignore_index=True) 

  # Extract bouts
  _, avgTailAngles, smoothedTA, smoothedHD = anglePrep(tailangles.copy(),
                                                       FLHC['heading'].values.copy(),
                                                       camera['rate'], (0,7), 5)
  bouts = detectBouts(avgTailAngles, 25, camera['rate'], loThresh=10, zeroLimit=2)
  boutData = pd.DataFrame(bouts, columns=['startCounter', 'peakCounter', 'endCounter', 'auc', 'max'])
  boutData = boutData.astype({'startCounter': np.int, 'peakCounter': np.int, 'endCounter': np.int})

  ## Save the sheeit to HDF5 file ##
  # DataFrames to HDF5, easy
  FLHC.to_hdf(os.path.join(bdir, textFileName), key='flhc', mode='w')
  skipinfo.to_hdf(os.path.join(bdir, textFileName), key='skipinfo', mode='r+')
  rawData.to_hdf(os.path.join(bdir, textFileName), key='rawdata', mode='r+')
  boutData.to_hdf(os.path.join(bdir, textFileName), key='boutdata', mode='r+')

  # Numpy arrays to HDF5, less easy (but still easy)
  with h5py.File(os.path.join(bdir, textFileName), 'r+') as h5f:
    h5f.create_dataset('tailangles', data=tailangles)
    h5f.create_dataset('tailcoords', data=tailcoords)
    h5f.create_dataset('exceedtrials', data=exceedsRadialLimit)
    h5f.create_dataset('tailtip', data=avgTailAngles)
    h5f.create_dataset('smoothedtailangles', data=smoothedTA)
    h5f.create_dataset('smoothedheading', data=smoothedHD)


if __name__ == "__main__":

  # Extract directory of interest ('DOI')
  opts = getOptions()
  DOI = os.path.realpath(opts.directory)
  radius = opts.radius

  # Prepare computer for multiprocessing...
  warnings.simplefilter(action="ignore", category=RuntimeWarning)  
  warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)  
  multiprocessing.freeze_support()

  # Locate '.info' files
  infoDirs = [os.path.dirname(d) for d in glob.glob(os.path.join(DOI, '**', '*.info'), recursive=True)]
  mpbDirs =  [os.path.dirname(d) for d in glob.glob(os.path.join(DOI, '**', '*MPB.h5'), recursive=True)]
  exDirs = [bdir for bdir in infoDirs if bdir not in mpbDirs]  ## directories to be processed
  inDirs = [bdir for bdir in infoDirs if bdir in mpbDirs]  ##  directories already processed

  # If no '.info' files are found, exit
  if infoDirs == []:
    print('No .info files found in directory {}. Ensure .info files exist somewhere '.format(DOI) +
          'within the directory, otherwise start coding a fix!')
    sys.exit()

  # List processed directories for user
  for bdir in inDirs:
    print('Directory processed {}...'.format(bdir))

  # Loop through '.info' files, processing each
  for bdir in tqdm(exDirs, desc='Looping over data directories', ascii=False, ncols=75, position=0):
    print('\nProcessing directory {}...'.format(bdir))

    # Create 'frameDict' variable if non-existent
    ftf.createFrameDict(bdir)

    # Process tail movement
    startTime = time.time()
    try:
      main(bdir, radius)
    except Exception as e:
      os.rename(bdir, bdir + 'F')
      print('Error: {}'.format(e))
      print('Processing failed, {} renamed...'.format(bdir + 'F'))
    finally:
      print('Runtime: {0}'.format(time.time() - startTime))
